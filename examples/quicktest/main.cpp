#include <cstdio>
#include <cstdio>
#include <cstdarg>
#include <iostream>
#include <cstddef>
#include <typeinfo>

#include <xen/xen-unity.cpp>

void printTypeDef(const xen::MetaType* type, void* data = nullptr, u32 indent_level = 0){
	printf("%s", type->name);
	if(type->fields.length == 0){
		printf(" = ");
		if (data != nullptr) {
			//then cur_fields is of an atomic type, print its value
			if     (type == xen::typeOf<u08>())   { printf("%i",   *((u08*   )data)); }
			else if(type == xen::typeOf<u16>())   { printf("%i",   *((u16*   )data)); }
			else if(type == xen::typeOf<u32>())   { printf("%i",   *((u32*   )data)); } 
			else if(type == xen::typeOf<u64>())   { printf("%lli", *((u64*   )data)); }
			else if(type == xen::typeOf<s08>())   { printf("%i",   *((s08*   )data)); }
			else if(type == xen::typeOf<s16>())   { printf("%i",   *((s16*   )data)); }
			else if(type == xen::typeOf<s32>())   { printf("%i",   *((s32*   )data)); }
			else if(type == xen::typeOf<s64>())   { printf("%lli", *((s64*   )data)); }
			else if(type == xen::typeOf<real32>()){ printf("%f",   *((real32*)data)); }
			else if(type == xen::typeOf<real64>()){ printf("%f",   *((real64*)data)); }
			else if(type == xen::typeOf<bool>())  { printf("%i",   *((bool*  )data) ? 0 : 1); }
			else { printf ("? ATOMIC ?"); }
		} else {
			printf("no value addr");
		}
		printf("\n");
	} else {
		printf("{\n");
		for(auto cur_field = xen::begin(type->fields); cur_field; ++cur_field){
			for(u32 i = 0; i < indent_level + 1; ++i){ printf("  "); }
			printf("%s : ", cur_field->name);
			printTypeDef(cur_field->type,
			             data != nullptr ? xen::accessField(data, (const xen::MetaTypeField*)cur_field) : nullptr,
			             indent_level + 1);
		}
		for(u32 i = 0; i < indent_level; ++i){ printf("  "); }
		printf("}\n");
	}

}

#include <iostream>

int main(int argc, const char** argv){
	char pathBuffer[256];
	xen::Path path = xen::makePath(pathBuffer, 256);
	xen::getCwd(path);
	printf("cwd: %s\n", path.buffer.values);

	printf("Iterating\n");
	for(auto it = xen::iterateDirectory(path); it; ++it){
		if(it.attributes & xen::FileIterator::TYPE_FILE){
			printf("FILE: ");
		} else if (it.attributes & xen::FileIterator::TYPE_DIRECTORY){
			printf(" DIR: ");
		} else {
			printf("????: ");
		}
		printf("%s\n", it.name);
	}
	printf("Iterating Done\n");
		
	xen::initializeSystemMemory();
	xen::initializeSystemLog(xen::getRootAllocator(), xen::megabytes(1));
	xen::initializeSystemMetaType(xen::getRootAllocator(), xen::megabytes(1));
	
	printf("%s, size: %i, id: %i\n", xen::typeOf<u08>()->name,    xen::typeOf<u08>()->static_size,    xen::typeOf<u08>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<u16>()->name,    xen::typeOf<u16>()->static_size,    xen::typeOf<u16>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<u32>()->name,    xen::typeOf<u32>()->static_size,    xen::typeOf<u32>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<u64>()->name,    xen::typeOf<u64>()->static_size,    xen::typeOf<u64>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<s08>()->name,    xen::typeOf<s08>()->static_size,    xen::typeOf<s08>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<s16>()->name,    xen::typeOf<s16>()->static_size,    xen::typeOf<s16>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<s32>()->name,    xen::typeOf<s32>()->static_size,    xen::typeOf<s32>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<s64>()->name,    xen::typeOf<s64>()->static_size,    xen::typeOf<s64>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<real>()->name,   xen::typeOf<real>()->static_size,   xen::typeOf<real>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<real32>()->name, xen::typeOf<real32>()->static_size, xen::typeOf<real32>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<real64>()->name, xen::typeOf<real64>()->static_size, xen::typeOf<real64>()->id);
	printf("%s, size: %i, id: %i\n", xen::typeOf<bool>()->name,   xen::typeOf<bool>()->static_size,   xen::typeOf<bool>()->id);
	printf("%s\n", xen::typeOf<Vec2>()->name);

	int arr[] = { 1,2,3,4 };
	xen::Array<int> xarr = xen::makeArray(arr);

	printTypeDef(xen::typeOf<xen::Array>());
	printTypeDef(xen::typeOf<xen::Array<int>>(), &xarr);


	/*printf("*\n*\n*\nRUN TIME TYPES\n", xen::type_name<Vec2>());
	printf("long, %s, size: %i, id: %i\n", xen::getType("long")->name, xen::getType("long")->static_size,    xen::getType("long")->id);
	printf("b32, %s, size: %i, id: %i\n", xen::getType("b32")->name, xen::getType("b32")->static_size,    xen::getType("b32")->id);
	printf("u08, %s, size: %i, id: %i\n", xen::getType("u08")->name, xen::getType("u08")->static_size,    xen::getType("u08")->id);
	printf("real32, %s, size: %i, id: %i\n", xen::getType("real32")->name, xen::getType("real32")->static_size,    xen::getType("real32")->id);
	printf("long, %s, size: %i, id: %i\n", xen::getType("long long")->name, xen::getType("long long")->static_size, xen::getType("long long")->id);*/

	xen::shutdownSystemMetaType();
	xen::shutdownSystemLog();
	xen::shutdownSystemMemory();

	//std::cout << "Press enter to terminate the program..." << std::endl;
	//std::cin.get();
	return 0;
}
