# /examples/quicktest

Quicktest is the primary application used to test the engine as features are being developed.

Since you cannot 'run' the engine, this is also the application that is ran by the run commands in the \shell_win and \shell_unix directories.

Since features are tested using this application as they are developed, going back to a commit where a feature is being added will likely have example usage code in quicktest's main.cpp