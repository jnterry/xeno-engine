////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \author Jamie Terry
/// \date 2016/07/25
/// \brief Contains main function of the build-time utility.
///
/// Overview
/// --------
///
/// The build-time tool is a command line utility which can measure the elapsed time
/// between two invokations of the the tool. Its intended use is for measuring how
/// build times for a project change over time, however it could be used in any
/// comand line script to measure other processes.
///
/// Each usage is split into "phases", for example in building a project you may need
/// to run a preprocessor to generate some source files, then compile some library, and
/// finaly the application that uses the library.
///
/// These would be the phases "preprocess", "library-build", "executable-build".
/// Each phase can have its duration recorded. (Note that it is not possible for
/// phases to be overlapped - each must end before another can begin).
///
/// \todo :TODO: if want to support overlapped phases could add support in report
/// generation to group phases, eg library/compile and library/link are sub phases
/// of 'library' 
///
/// The tool saves this data to files and can then analyse the data at some later point.
///
/// Usage
/// -----
///
/// File Formats
/// ------------
/// The build-time tool stores data in two format, a single build
/// (build time single -> .bts) and a bundle format (build time bundle -> .btb)
///
/// This is to enable adding the build-time files to version control if desired.
/// If every build was just appended to a single file then (ie, the bundle format)
/// then merge conflicts would occur everytime, and they could not be sorted since
/// the files are binary. Instead the .bts files should not conflict, since they
/// are named based on the second at which the (build-time start) command was issued.
/// The build-time single (.bts) files can then be bundled up into a bundle (.btb)
/// format, eg the day or week after the bts files are generated. This bundle would
/// never need to be updated since the date range it represents would have passed
/// once it is created. This bundle format is also more efficent for build-time to
/// load in order to generate historical reports concerning how the build times have
/// changed over time.
///
/// \todo :TODO: implment bundling
///
/// Build Time Single Data Layout
/// -----------------------------
/// u16 FILE_MAGIC_NUMBER
/// u16 FILE_TYPE_SINGLE
/// u16 version           = 1
///
/// properties, strings of the form "compile=msvc", "os=windows", etc
/// Properties seperated with CHAR_PROPERTY_SEPERATOR (NOT NULL TERMINATED),
/// last one terminated with CHAR_PHASE_SEPERATOR, after which Phases are stored
///
/// phases (any number of following block until EOF):
/// char[]        phase name (null terminator indicates the end)
/// xen::Duration duration   (this immediately follows the null terminator)
/// u8            state      (INCOMPLETE, SUCCESS, FAILED)
/// CHAR_PHASE_SEPERATOR     (may be omitted if last phase, since the EOF)   
////////////////////////////////////////////////////////////////////////////////

//:TODO: this is littered with fread and fwrite which will mean files written
//with one endianess wont be readable on a system with the other, fix it!

#include <cstdio>

#include <xen/xen-unity.cpp>

/// \brief global var indicating the time at which the program
const xen::DateTime program_start_time = xen::getLocalTime();

/// \brief First 2 bytes of any build-time file to identify it
constexpr const static u16 FILE_MAGIC_NUMBER = 0xB4B5;

/// \brief 3rd and 4th bytes of any build-time file to identify
/// if its a single or bundle file
constexpr const static u16 FILE_TYPE_SINGLE  = 0x4953;
constexpr const static u16 FILE_TYPE_BUNDLE  = 0x5542;

/// \breif byte used to seperate properties in a single file
constexpr const static s8 CHAR_PROPERTY_SEPERATOR = ',';

/// \brief byte used at the very start of a phase block
constexpr const static s8 CHAR_PHASE_SEPERATOR = 2;

struct FileHeader{
	u16 magic_number;
	u16 type;
	u16 version;
};

/// \breif writes a file header to the file at its current positions
void writeFileHeader(FILE* file, FileHeader header){
	fwrite(&header.magic_number, sizeof(u16), 1, file);
	fwrite(&header.type,         sizeof(u16), 1, file);
	fwrite(&header.version,      sizeof(u16), 1, file);
}

/// \brief determines if a file is valid by loading a FileHeader, returns 0
/// if not, else returns a FILE_TYPE_***
u16 isValidFile(FILE* file, const char* file_name){
	u16 values[3];
	
	size_t count = fread(&values, sizeof(values[0]), 3, file);
	if(count < 2){
		fprintf(stderr, "Failed to validate file '%s' as a build-time file since an "
		        "error occured while reading magic number and file type\n", file_name);
		return 0;
	}

	if(values[0] != FILE_MAGIC_NUMBER){
		fprintf(stderr, "Failed to validate file '%s' as a build-time file since the "
		        "magic number was not as expected\n", file_name);
		return 0;
	}

	if(values[2] != 1){
		fprintf(stderr, "Failed to validate file '%s' as a build-time file since the "
		        "version number was not recognised, file's version: %i", file_name, values[2]);
		return 0;
	}

	if(values[1] == FILE_TYPE_SINGLE || values[1] == FILE_TYPE_BUNDLE){
		return values[1];
	}

	fprintf(stderr, "Failed to validate file '%s' as a build-time file since the "
		        "file type identifier was not valid\n", file_name);
	return 0;
}

struct PhaseStates{
	enum Value : u8{
		INCOMPLETE = 0,
		FAILED     = 1,
		SUCCESS    = 2,
	};
};
	
struct FileSinglePhaseFooter{
	union{
		xen::Duration duration;
		xen::DateTime date_time;
	};

	u8 state;
};
//when state is INCOMPLETE its a date_time of when we started, otherwise its a duration of how long it lasted
static_assert(sizeof(xen::DateTime) == sizeof(xen::Duration), "This assumes that duration and date time are same size");

/// \brief outputs a xen::DateTime to stdout as a hex string that
/// can later be parsed by inputDateTime
void outputDateTime(const xen::DateTime& dt){
	const char* data = (const char*)&dt;
	for(u32 i = 0; i < sizeof(xen::DateTime); ++i){
		printf("%02x", data[i] & 0xFF);
	}
	printf("\n");
}
xen::DateTime inputDateTime(const char* str){
	xen::DateTime result;
	char* data = (char*)&result;
	for(u32 i = 0; i < sizeof(xen::DateTime); ++i){ data[i] = 0; }
	for(u32 i = 0; i < sizeof(xen::DateTime) * 2; ++i){
		u32 data_index = i / 2;
		u32 data_shift = i % 2 ? 0u : 4u;
		if(str[i] == '\0'){
			printf("Failed to read datetime string - ended too early\n");
			return {0};
		}
		u8 hex_val = xen::hexCharToNibble(str[i]);
		if(hex_val & 0xF0){
			printf("Failed to read datetime string '%s' - invalid character %c\n", str, str[i]);
			return {0};
		}
		data[data_index] |= hex_val << data_shift;
	}
	return result;
}

void displayUsage(){
	printf("Usage\n");
	printf("-----\n");
	printf(" build-time command [directory] [params]\n");
	printf(" Where:\n");
	printf("   - 'command' is one of:\n");
	printf("     - h/help/-h -> display this help\n");
	printf("     - start     -> create a new timing file and start a phase\n");
	printf("     - end       -> end a previously created timing file\n");
	printf("     - phase     -> stop timing previous phase, start timing new one\n");
	printf("     - phase     -> stop timing previous phase, start timing new one\n");
	printf("   - directory is the path containg the build-time output files\n");
	printf("   - params is the parameters to the specified command\n\n");

	printf("You can run \"build-time command\" (ie: missing the command paramters) in order to see help for a particular command\n");
}

void displayUsageStart(){
	printf("Command: Start\n");
	printf("--------------\n\n");

	printf("The start command creates a new build-time single (.bts) file and starts a new phase in the file\n\n");

	printf("build-time start build_time_directory [phase_name] [properties...]\n");
	printf("  - build_time_directory -> Root directory containg the build time files (.bts amd .btb) for the project\n");
	printf("  - phase_name -> Name of the first phase to create, optional\n");
	printf("  - properties -> Any extra properties to associated with this build, any number allowed\n");
	printf("    - Each must be of the form property_name=value\n");
	printf("    - Name and value may contain spaces, used quotes to keep as one argument, eg:\n");
	printf("      \"Operating System=Windows 10\"\n\n");

	printf("This command will output a hex string to stdout that must be captured in order to refer ");
	printf("to the created file in subsequent phase and end commands\n");
	printf("Any errors will be written to stderr, stderr will not be used if the process is successful\n");
}

void displayUsageEnd(){
	printf("Command: End\n");
	printf("------------\n\n");

	printf("The end command ends the most recently started phase in a build time single (.bts) file\n\n");

	printf("build-time end build_time_directory file_id [last_phase_state]\n");
	printf("  - build_time_directory -> Root directory containg the build time files (.bts amd .btb) for the project\n");
	printf("  - file_id -> The hex string identifing the .bts file as output by the start command\n");
	printf("  - last_phase_state -> The return value for the last phase.\n");
	printf("    - 0 indicates success, anything else indcates an error occured\n");
	printf("    - if not specified build-time assumes phase was successful\n\n");

	printf("This command will output to stdout the name of the ended phase and the amount of time it took\n\n");

	printf("Note that this command can be called multiple times for a single .bts file, provided that the phase ");
	printf("command is used to start a new phase between calls. This can be helpful if there is a section of your ");
	printf("build process you do not wish to time, for example:\n");

	printf("build-time start [dir] preprocess\n");
	printf("  ... run preprocess steps ...\n");
	printf("build-time end [dir] [file_id]\n");
	printf("  ... do something you dont want to time ...\n");
	printf("build-time phase [dir] [file_id] build\n");
	printf("  ... do project build ...\n");
	printf("build-time end [dir] [file_id]\n");
}

void displayUsagePhase(){
	printf("Command: Phase\n");
	printf("--------------\n\n");

	printf("The phase command stops timing the last phase and then immediately starts a new phase\n\n");

	printf("build-time phase build_time_directory file_id new_phase_name [last_phase_state]\n");
	printf("  - build_time_directory -> Root directory containg the build time files (.bts amd .btb) for the project\n");
	printf("  - file_id -> The hex string identifing the .bts file as output by the start command\n");
	printf("  - new_phase_name -> The name of the new phase to start\n");
   printf("  - last_phase_state -> The return value for the last phase.\n");
	printf("    - 0 indicates success, anything else indcates an error occured\n");
	printf("    - if not specified build-time assumes phase was successful\n\n");

	printf("This command will output to stdout the name of the ended phase and the amount of time it took\n\n");

	printf("If all previous phases have already been ended with the command 'end' then this command just starts a new phase\n\n");
}

void displayUsageShow(){
	printf("Command: Show\n");
	printf("-------------\n\n");

	printf("The show command will display the data in some build-time file\n\n");

	printf("build-time show build_time_directory file_id\n");
	printf("  - build_time_directory -> Root directory containg the build time files (.bts amd .btb) for the project\n");
	printf("  - file_id -> The hex string identifing the .bts file as output by the start command\n\n");


	printf("build-time show file_path\n");
	printf("  - file_path -> The file you wish to examine\n");
}



/// \breif Pushes the directory for a particular date time onto a base path
void pushFileDir(xen::Path& path, const xen::DateTime& dt){
	char tmp_buffer[48];
	xen::formatDateTime(dt, tmp_buffer, 48, "%Y/%m/%d/");
	xen::push(path, tmp_buffer);
}

/// \brief pushes a filename for a particular date time onto a base path
void pushFileName(xen::Path& path, const xen::DateTime& dt){
	char tmp_buffer[48];
	xen::formatDateTime(dt, tmp_buffer, 48, "%H-%M-%S.bts"); //build time single
	xen::push(path, tmp_buffer);
}

/// \breif Helper function that opens a result file for the specified date and time,
/// creates any missing directories and files
FILE* openResultFile(xen::Path& base, const xen::DateTime& dt, const char* open_mode){
   pushFileDir(base, dt);
	bool dir_made = xen::createDirectory(base.buffer.values);
	if(!dir_made){
		fprintf(stderr, "Error creating directory to put result file in, directory: '%s'\n", base.buffer.values);
		return nullptr;
	}
	
	pushFileName(base, dt);
	FILE* file = fopen(base.buffer.values, open_mode);

	if(!file){
		fprintf(stderr, "Error opening result file: '%s'\n", base.buffer.values);
		return nullptr;
	} else {
		return file;
	}
}

/// \brief Writes the specified phase to the file in its current position (ie: doesn't fseek to end)
bool writePhase(FILE* fout, const char* phase_name){
	fwrite(&CHAR_PHASE_SEPERATOR, sizeof(CHAR_PHASE_SEPERATOR), 1, fout);

	// write the phase name
	fwrite(phase_name, 1, strlen(phase_name) + 1, fout);

	// write FileSinglePhaseFooter
	auto now = xen::getLocalTime();
	fwrite(&now, sizeof(xen::DateTime), 1, fout);
	u8 status = PhaseStates::INCOMPLETE;
	fwrite(&status, 1, 1, fout);

	return true;
}

FileSinglePhaseFooter readFileSinglePhaseFooter(FILE* file, bool verbose){
	FileSinglePhaseFooter result;

	if(!fread(&result.date_time, sizeof(result.date_time), 1, file)){
		if(verbose){
			fprintf(stderr, "Failed to read start DateTime of latest phase\n");
		}
		return {0};
	}
	
	if(!fread(&result.state, sizeof(result.state), 1, file)){
		fprintf(stderr, "Failed to read state of latest phase\n");
		return {0};
	}

	return result;
}

/// \brief Completes the last phase in the file, returns false if error occurs (eg, bad
/// format, phase already completed, etc). This modifies the read/write position of file
/// \param successful whether to set the phase state to PhaseStates::SUCCESS or PhaseStates::FAILED
bool completeLastPhase(FILE* file, bool successful, bool verbose){
	//find the first byte of the phase footer, -1 to get null terminator of preceeding phase name string
	fseek(file, -(s64)(sizeof(FileSinglePhaseFooter::duration) + sizeof(FileSinglePhaseFooter::state) + 1), SEEK_END);

	long null_position = ftell(file);
	if(null_position < 0){
		fprintf(stderr, "Failed to get position of phase string null terminator\n");
		return false;
	}
	char current_char = (char)fgetc(file);
	if(current_char != '\0'){
		if(verbose){
			fprintf(stderr, "Null terminator for last phase name not at expected location in file\n");
		}
		return false;
	}

	FileSinglePhaseFooter last_phase_data = readFileSinglePhaseFooter(file, verbose);
	if(last_phase_data.duration.nanoseconds == 0){
		fprintf(stderr, "Failed to read last phase data\n");
		return false;
	}

	switch(last_phase_data.state){
	case PhaseStates::INCOMPLETE: break;
	case PhaseStates::SUCCESS:
		if(verbose){
			fprintf(stderr, "Latest phase already completed with status 'success'\n");
		}
		return false;
	case PhaseStates::FAILED:
		if(verbose){
			fprintf(stderr, "Latest phase already completed with status 'failed'\n");
		}
		return false;
	default:
		if(verbose){
			fprintf(stderr, "Latest phase had unknown state code: %i, aborting\n", last_phase_data.state);
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////
	//work backwards from null_position to get the name of the phase
	//:TODO: we have no guarenty phase name fits withing 128 chars -> add check when writting phases
	char last_phase_name_buffer[128];
	char* last_phase_name = &last_phase_name_buffer[XenArrayLength(last_phase_name_buffer) - 1];
	*last_phase_name = '\0';
	for(long i = 1; i < null_position && current_char != CHAR_PHASE_SEPERATOR; ++i){
		fseek(file, null_position - i, SEEK_SET);
		current_char = (char)fgetc(file);
		--last_phase_name;
		*last_phase_name = current_char;
	}
	++last_phase_name; //skip past the CHAR_PHASE_SEPERATOR

	xen::Duration last_phase_elapsed = program_start_time - last_phase_data.date_time;
	printf("Completed ");
	if(*last_phase_name != '\0'){
		printf("phase '%s' ", last_phase_name);
	}
	printf("%s in %f seconds\n", successful ? "successfully" : "unsuccessfully", xen::asSeconds<float>(last_phase_elapsed));

	//update the phase footer with the elapsed time and set the state
	fseek(file, null_position + 1, SEEK_SET);
	fwrite(&last_phase_elapsed, sizeof(xen::Duration), 1, file);
	u8 status = successful ? PhaseStates::SUCCESS : PhaseStates::FAILED;
	fwrite(&status, 1, 1, file);

	return true;
}

int cmdStart(int argc, const char* argv[]){
	if(argc < 1) {
		printf("Not enough paramters to command 'start'\n");
		displayUsageStart();
		return 1;
	} 
	char path_buffer[256];
	xen::Path path = xen::makePath(path_buffer, 256, argv[0]);
	
	FILE* fout = openResultFile(path, program_start_time, "wb");
	if(!fout){ return 101; }

	// write the datetime to stdout so that caller can refer to the created file
	//in future invokations of this program
	outputDateTime(program_start_time);

	/////////////////////////////////////////////////////////////
	// write file header
	fwrite(&FILE_MAGIC_NUMBER, 1, sizeof(FILE_MAGIC_NUMBER), fout);
	fwrite(&FILE_TYPE_SINGLE,  1, sizeof(FILE_TYPE_SINGLE), fout);
	u16 version = 1;
	fwrite(&version, 1, sizeof(version), fout);

	/////////////////////////////////////////////////////////////
	// write the properties, eg, compiler=msvc, etc
	if(argc > 2){
		for(int i = 2; i < argc; ++i){
			//:TODO: check properties are valid, ie, of the form [str]=[str]
			fwrite(argv[i], 1, strlen(argv[i]), fout);
			if(i != argc - 1){ fwrite(&CHAR_PROPERTY_SEPERATOR, 1, sizeof(CHAR_PROPERTY_SEPERATOR), fout); }
		}
	}

	//write the phase
	if(!writePhase(fout, argc > 1 ? argv[1] : "\0")){
		fprintf(stderr, "Error occured writting phase");
		fclose(fout);
		return 1;
	}
	
	fclose(fout);
	return 0;
}

int cmdPhase(int argc, const char* argv[]){
	if(argc < 3) {
		printf("Not enough paramters to command 'phase'\n");
		displayUsagePhase();
		return 1;
	}

	char path_buffer[256];
	xen::Path path = xen::makePath(path_buffer, 256, argv[0]);

	xen::DateTime file_dt = inputDateTime(argv[1]);

	const char* new_phase_name = argv[2];

	bool phase_successful = true;
	if(argc > 3){
		if(argv[3][0] != '0' || argv[3][1] != '\0'){ phase_successful = false; }
	}

   FILE* file = openResultFile(path, file_dt, "r+b");
	if(!file){ return 201; }

	u16 type = isValidFile(file, path.buffer.values);
	if(!type){ return 202; }
	if(type != FILE_TYPE_SINGLE){
		fprintf(stderr, "Expected file '%s' to be a single file, but it is not\n", path.buffer.values);
		return 203;
	}

	completeLastPhase(file, phase_successful, false);

	fseek(file, 0, SEEK_END);
	writePhase(file, new_phase_name);
	printf("Started phase %s\n", new_phase_name);
	
	return 0;
}

int cmdEnd(int argc, const char* argv[]){
   if(argc < 2) {
	   printf("Not enough paramters to command 'end'\n");
	   displayUsageEnd();
	   return 1;
   }
	
	char path_buffer[256];
	xen::Path path = xen::makePath(path_buffer, 256, argv[0]);

	xen::DateTime file_dt = inputDateTime(argv[1]);
	if (file_dt == xen::DateTime{0}) { return 1; }
	
	bool phase_successful = true;
	if(argc > 2){
		if(argv[2][0] != '0' || argv[2][1] != '\0'){ phase_successful = false; }
	}

   FILE* file = openResultFile(path, file_dt, "r+b");
	if(!file){ return 301; }

	u16 type = isValidFile(file, path.buffer.values);
	if(!type){ return 302; }
	if(type != FILE_TYPE_SINGLE){
		fprintf(stderr, "Expected file '%s' to be a single file, but it is not\n", path.buffer.values);
		return 303;
	}

	completeLastPhase(file, phase_successful, true);
	
	return 0;
}

int showSingle(FILE* fin, const char* file_name, xen::DateTime dt){
	///////////////////////////////////////////////////////////////
	//print heading -> Creation_Time - propertyA=value, ...
	char dt_buffer[64];
	xen::formatDateTime(dt, dt_buffer, XenArrayLength(dt_buffer), "%c");
	printf("%s", dt_buffer);
	char cur_char = (char)fgetc(fin);
	if(cur_char != CHAR_PHASE_SEPERATOR){
		printf(" - ");
		while(cur_char != CHAR_PHASE_SEPERATOR && feof(fin) == 0){
			printf("%c", cur_char);
			cur_char = (char)fgetc(fin);
		}
	}
	printf("\n");

	///////////////////////////////////////////////////////////////
	//print each phase
	while(!feof(fin)){
		u32 name_length = 24;
		printf("  ");
		while(cur_char = (char)fgetc(fin), cur_char != '\0'){
			--name_length;
			printf("%c", cur_char);
		}
		printf("%*s", name_length, "");
		FileSinglePhaseFooter phase_data = readFileSinglePhaseFooter(fin, false);
		printf(" - ");

		if(phase_data.duration.nanoseconds == 0){
			printf("ERROR LOADING PHASE DATA\n");
		} else {
			if(phase_data.state == PhaseStates::INCOMPLETE){
				xen::formatDateTime(dt, dt_buffer, XenArrayLength(dt_buffer), "%c");
				printf("Incomplete, started at: %s\n", dt_buffer);
			} else if (phase_data.state == PhaseStates::SUCCESS || phase_data.state == PhaseStates::FAILED){
				printf("Completed, successful: %i, time taken: %f seconds\n",
				       phase_data.state == PhaseStates::SUCCESS,
				       xen::asSeconds<float>(phase_data.duration));
			} else {
				printf("PHASE IN UNKNOWN STATE\n");
			}
		}

		cur_char = (char)fgetc(fin);
		if(cur_char != CHAR_PHASE_SEPERATOR && !feof(fin)){
			fprintf(stderr, "  Failed to find phase seperator in expected location\n");
			break;
		}
	}


	
	return 1;
}

int cmdShow(int argc, const char* argv[]){
	char path_buffer[256];
	xen::Path path;
	xen::DateTime file_single_dt = {0};
	switch(argc){
	case 0:
		printf("Not enough parameters to command 'show'\n");
		displayUsageShow();
		return 1;
	case 1: //then show [full_path] format
		path = xen::makePath(path_buffer, XenArrayLength(path_buffer), argv[0]);
		break;
	case 2: {//then show [dir] [file_id] format
		path = xen::makePath(path_buffer, XenArrayLength(path_buffer), argv[0]);
		file_single_dt = inputDateTime(argv[1]);
		pushFileDir (path, file_single_dt);
		pushFileName(path, file_single_dt);
	}  break;
	} //end switch

	FILE* fin = fopen(path.buffer.values, "r");
	if(!fin){
		printf("Failed to open file: %s\n", path.buffer.values);
		return 401;
	}

	u16 type = isValidFile(fin, path.buffer.values);
	if(!type){ return 402; }
	if(type == FILE_TYPE_BUNDLE){
		fprintf(stderr, "Show command doesn't support bundle files yet");
		return 498;
	} else if (type == FILE_TYPE_SINGLE){
		return showSingle(fin, path.buffer.values, file_single_dt);
	} else {
		fprintf(stderr, "Unknown file type");
		return 499;
	}
}

/// \breif Some function which corresponds to a particular command that can be run
typedef int(*CommandFunction)(int, const char*[]);
const char* CMD_NAMES[] = {
	"start",
	"end",
	"phase",
	"show",
	//"bundle",
	//"csv",
	//"stats"
};
const CommandFunction CMD_FUNCTIONS[] = {
	&cmdStart,
	&cmdEnd,
	&cmdPhase,
	&cmdShow,
};
static_assert(XenArrayLength(CMD_NAMES) == XenArrayLength(CMD_FUNCTIONS), "Must be a 1:1 mapping between mode names and functions");

int main(int argc, const char* argv[]){
	if(argc < 2 || strcmp(argv[1], "h") == 0 || strcmp(argv[1], "help") == 0 ||
	   strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "/?") == 0){
		displayUsage();
		return 1;
	}

	if(argc < 2){
		printf("Not enough arguments\n");
		displayUsage();
		return 0;
	}
	
	for(int i = 0; i < XenArrayLength(CMD_NAMES); ++i){
		if(strcmp(argv[1], CMD_NAMES[i]) == 0){
			return CMD_FUNCTIONS[i](argc-2, &argv[2]);
			break;
		}
	}

	printf("Invalid command name '%s'\n", argv[1]);
	return displayUsage();
}


