@echo off

set CommonCompilerFlags=-Od -MTd -nologo -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -FC -Z7 -EHsc
set CommonCompilerFlags=%CommonCompilerFlags% -W4

:: enable some warnings that are disabled by default, even at /W4
:: /Wall has too many, so just enable ones that make sense
:: see https://msdn.microsoft.com/en-us/library/23k5d385.aspx
set CommonCompilerFlags=%CommonCompilerFlags% -wd4268         & :: ignore C4268 - const static/global initied with compiler default constructors
set CommonCompilerFlags=%CommonCompilerFlags% -wd4100         & :: ignore C4100 - unused parameter to function
set CommonCompilerFlags=%CommonCompilerFlags% -wd4201         & :: ignore C4100 - using nameless struct inside union
set CommonCompilerFlags=%CommonCompilerFlags% -we4146         & :: make '-(unsigned_type)' an error, use -((int)(unsigned_type)) instead
set CommonCompilerFlags=%CommonCompilerFlags% -w44061 -w44062 & :: missing case in enum switch
set CommonCompilerFlags=%CommonCompilerFlags% -w44365 -w44388 & :: implicit conversion between unsigned and signed
set CommonCompilerFlags=%CommonCompilerFlags% -w34738         & :: ran out of float registers, putting in memory
set CommonCompilerFlags=%CommonCompilerFlags% -w14311         & :: pointer truncation (assigned pointer to int type thats too small)
set CommonCompilerFlags=%CommonCompilerFlags% -w44296         & :: if comparision always evaluates to false
set CommonCompilerFlags=%CommonCompilerFlags% -w44302         & :: conversion from larger to smaller type
set CommonLinkerFlags= -incremental:no -opt:ref


del *.exe > NUL 2> NUL
del *.pdb > NUL 2> NUL

cl main.cpp %CommonCompilerFlags% -I..\..\..\engine -Feloc-analyser -link %CommonLinkerFlags%
