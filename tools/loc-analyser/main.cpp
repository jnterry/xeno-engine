////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \author Jamie Terry
/// \date 2016/07/23
/// \brief Command line utility for counting lines of code in some directory and
/// its sub directories
///
/// \todo :TODO: Bug occurs when file nam exceeds DIR_OUT_WIDTH, we try to pad
/// by a negative number of characters, unsigned overflow -> pad by 4billion characters
///
/// \todo :TODO: add cmd options for including only certain file extensions
////////////////////////////////////////////////////////////////////////////////

#include <xen/xen-unity.cpp>
#include <cstdio>

constexpr static const u32 DIR_OUT_WIDTH = 70;

// if the output is garbeled it will be due to using extended ascii characters,
// ideally we'd be printing unicode
//constexpr static const char CHAR_COL_SEPERATOR       = 186; // ||
//constexpr static const char CHAR_ROW_SEPERATOR       = 196; // -
//constexpr static const char CHAR_TREE_VERT           = 179; // |
//constexpr static const char CHAR_TREE_BRANCH_LEFT    = 195;
//constexpr static const char CHAR_TREE_BRANCH_DOWN    = 194;
//constexpr static const char CHAR_TREE_HORIZONTAL     = 196;
//constexpr static const char CHAR_TREE_TO_HORIZONTAL  = 192; // L

constexpr static const char CHAR_COL_SEPERATOR       = '|'; // ||
constexpr static const char CHAR_ROW_SEPERATOR       = '-'; // -
constexpr static const char CHAR_TREE_VERT           = '|'; // |
constexpr static const char CHAR_TREE_BRANCH_LEFT    = '+';
constexpr static const char CHAR_TREE_BRANCH_DOWN    = '+';
constexpr static const char CHAR_TREE_HORIZONTAL     = '-';
constexpr static const char CHAR_TREE_TO_HORIZONTAL  = '+';

struct LineStats{
	u32 code;    ///< pure code lines
	u32 mixed;   ///< mixed comment + code lines
	u32 comment; ///< pure comment lines
	u32 empty;   ///< lines containing only whitespace

	LineStats& operator+=(const LineStats& other){
		this->code    += other.code;
		this->mixed   += other.mixed;
		this->comment += other.comment;
		this->empty   += other.empty;
		return *this;
	}
	u32 total() const { return this->code + this->mixed + this->comment + this->empty; }
};

bool isNewLine(const char c){ return c == '\n' || c == '\r'; }

const char* eatLine(const char* c){
	while(!isNewLine(*c) && c){ ++c; } //eat to end of line
	return c;
}

const char* eatWhitespaceExceptNewLine(const char* c){
	while((*c == ' ' || *c == '\t' || *c == '\v') && *c != '\0'){ ++c; }
	return c;
}

void updateLineStats(LineStats* stats, bool line_contains_code, bool line_contains_comment){
	if(line_contains_code){
		if(line_contains_comment){
			++stats->mixed;
		} else {
			++stats->code;
		}
	} else {
		if(line_contains_comment){
			++stats->comment;
		} else {
			++stats->empty;
		}
	}
}

LineStats analyseFile(const char* file_content){
	const char* cur = file_content;
	LineStats stats = {0};

	bool in_multiline_comment = false;
	bool line_contains_comment = false;
	bool line_contains_code = false;
	while(cur){
		if(isNewLine(*cur)){
			if(in_multiline_comment){ line_contains_comment = true; }
			updateLineStats(&stats, line_contains_code, line_contains_comment);
			if(*cur == '\r'){ ++cur; }
			if(*cur == '\n'){ ++cur; }
			line_contains_code    = false;
			line_contains_comment = false;
			continue;
		}

		cur = eatWhitespaceExceptNewLine(cur);
		if(*cur == '\0'){ break; }

		if(in_multiline_comment){
			line_contains_comment = true;
			if(cur[0] == '*' && cur[1] == '/'){
				++cur;
				in_multiline_comment = false;
			}
			++cur;
			continue;
		} //otherwise not in multiline comment

		if(cur[0] == '/' && cur[1] == '/'){
			line_contains_comment = true;
			cur = eatLine(cur);
		} else if(cur[0] == '/' && cur[1] == '*'){
			line_contains_comment = true;
			in_multiline_comment  = true;
			++cur; ++cur;
		} else if (cur[0] == '\n') {
			//this is needed to ensure that handling of lines containing only tabs is correct
			//(ie: counted as a blank line) - this is because \n\t\n will trigger the if whitespace,
			//then get to eatWhitespaceExceptNewLine(), then come through and assume its a code line
		} else {
			line_contains_code = true;
			++cur;
		}
	}

	return stats;
}

void printTableRowSeperator(u32 number){
	for(u32 i = 0; i < number; ++i){ printf("%c", CHAR_ROW_SEPERATOR); }
}

void printTableHorizontalDivider(){
	printf("%c", CHAR_COL_SEPERATOR);
	printTableRowSeperator(6);
	printf("%c", CHAR_COL_SEPERATOR);
	printTableRowSeperator(9);
	printf("%c", CHAR_COL_SEPERATOR);
	printTableRowSeperator(7);
	printf("%c", CHAR_COL_SEPERATOR);
	printTableRowSeperator(7);
	printf("%c", CHAR_COL_SEPERATOR);
	printTableRowSeperator(9);
	printf("%c\n", CHAR_COL_SEPERATOR);
}

void printTableRow(const LineStats& stats){
	printf("%c %4i %c %7i %c %5i %c %5i %c %7i %c\n",
	       CHAR_COL_SEPERATOR,
	       stats.code,    CHAR_COL_SEPERATOR,
	       stats.comment, CHAR_COL_SEPERATOR,
	       stats.empty,   CHAR_COL_SEPERATOR,
	       stats.mixed,   CHAR_COL_SEPERATOR,
	       stats.total(), CHAR_COL_SEPERATOR);
}

void analyseDirectory(xen::ArenaLinear* arena, xen::Path path, LineStats& stats, s32 depth = 0){
	LineStats dir_stats = {0};
	{
		for(s32 i = 0; i < depth-1; ++i){ printf("%c ",CHAR_TREE_VERT); }
		if(depth){ printf("%c%c", CHAR_TREE_BRANCH_LEFT, CHAR_TREE_HORIZONTAL); }
		printf("%c%c %s ", CHAR_TREE_BRANCH_DOWN, CHAR_TREE_HORIZONTAL, path.buffer.values);
		s32 spacer_count = DIR_OUT_WIDTH - 2*depth - 3 - strlen(path.buffer.values);
		printTableRowSeperator(spacer_count);
		printTableHorizontalDivider();
	}

	for(xen::FileIterator it = xen::iterateDirectory(path.buffer.values); it; ++it){
		xen::push(path, it.name);
		if(it.attributes & xen::FileIterator::TYPE_FILE){
			if(xen::findFirst('\0', it.name)[-1] != '~' && !xen::findFirst('#', it.name)){
				char* file_contents = xen::loadFile(path.buffer.values, *arena);
				LineStats file_stats = analyseFile(file_contents);
				xen::rollback(*arena, file_contents);

				for(s32 i = 0; i < depth; ++i){ printf("%c ", CHAR_TREE_VERT); }
				printf("%c%c", CHAR_TREE_BRANCH_LEFT, CHAR_TREE_HORIZONTAL);
				printf(" %-*s ", DIR_OUT_WIDTH-(depth+1)*2 - 1, &xen::findLast('/', path.buffer.values)[1]);
				printTableRow(file_stats);

				dir_stats += file_stats;
			}
		} else if(it.attributes & xen::FileIterator::TYPE_DIRECTORY){
			analyseDirectory(arena, path, dir_stats, depth + 1);
		}
		xen::pop(path);
	}

	{
		for(s32 i = 0; i < depth; ++i){ printf("%c ", CHAR_TREE_VERT); }
		printf("%c", CHAR_TREE_VERT); // "|"
		s32 spacer_count = DIR_OUT_WIDTH - 2*depth - 8;
		for(s32 i = 0; i < spacer_count; ++i){ printf(" "); }
		printf(" TOTALS ");
		printTableRow(dir_stats);
		for(s32 i = 0; i < depth; ++i){ printf("%c ", CHAR_TREE_VERT); }
	   printf("%c", CHAR_TREE_TO_HORIZONTAL); // "L"
		spacer_count = DIR_OUT_WIDTH - 2*depth;
		printTableRowSeperator(spacer_count);
		printTableHorizontalDivider();
	}
	stats += dir_stats;
}

int main(int argc, char* argv[]){
	xen::Stopwatch timer;

	xen::initializeSystemMemory();
	xen::ArenaLinear* arena = xen::allocateArenaLinear(xen::getRootAllocator(), xen::megabytes(64));

	char pathBuffer[256];
	xen::Path path = xen::makePath(pathBuffer, 256);
	xen::getCwd(path);

	printf("ANALYSING PATH: %s\n\n", path.buffer.values);

	LineStats stats = {0};
	printf("%-*s %c %s %c %s %c %s %c %s %c  %s  %c\n", DIR_OUT_WIDTH,
	       "   File/Directory", CHAR_COL_SEPERATOR,
	       "Code", CHAR_COL_SEPERATOR,
	       "Comment", CHAR_COL_SEPERATOR,
	       "Blank", CHAR_COL_SEPERATOR,
	       "Mixed", CHAR_COL_SEPERATOR,
	       "Total", CHAR_COL_SEPERATOR);
	analyseDirectory(arena, path, stats);

	xen::deallocate(xen::getRootAllocator(), arena);
	xen::shutdownSystemMemory();

	xen::Duration elapsed = timer.getElapsedTime();
	printf("Analysis done in %f seconds\n", xen::asSeconds<double>(elapsed));
}
