# /tools/loc-analyser

The LOC Analyser can be run to count the lines of code in some directory and its sub directories


# TODOs

:TODO: save results to file so we can generate history graphs

:TODO: rather than outputting by directory output by arbitrary groups, eg, by extension, even better if we can just do ending, eg
FILES
 .hpp
   .win.hpp
	.unix.hpp
 .cpp
   .win.cpp
	.unix.hpp

:TODO: add command line options so can specifiy directory to search rather than just using cwd. Also could specify extensions of files that should be counted/excluded