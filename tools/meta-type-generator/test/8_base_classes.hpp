namespace BasesTest{
	struct Base{
		int x;
	};

	struct Derived : public Base{
		int y;
	};

	struct TemplateDerived : public xen::Array<xen::Vector3<float>>{
		bool flag;
	};

}
