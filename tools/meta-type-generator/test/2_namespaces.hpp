namespace xen{
	struct Thing;

	struct Stuff{
		std::string name;     //type of this should be std::string

		int stuff;            //type of this should be int

		Thing object;         //type of this should be xen::Thing

		::Thing other_object; //type of this should be ::Thing
	};
}
