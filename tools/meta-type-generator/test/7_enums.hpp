enum TestEnumA{
	NUMBER_ONE,
	NUMBER_TWO,
	NUMBER_THREE,
};

namespace EnumTest{
	enum ThingA : u32 {
		ENTRY_A,
		ENTRY_B,
	};	
}
