/* comments are
ignored...

...right?*/

struct BasicTest{ //..they should be
	const u32 thing;
	static int stuff[2];
	Lump* other_lump;
	Thing& ref_to_thing;
	bool* bool_list[16];
};

struct BasicTest2{
	std::vector<int> my_ints; //template args should be merged into type
};
