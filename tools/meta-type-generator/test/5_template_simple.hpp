
template<typename T>
struct TemplateA{
	T value; //should have type xen::MetaTParam[0]
};

template<class T_TYPE>
struct TemplateB{
	T_TYPE thing; //should have type xen::MetaTParam[0]
};
