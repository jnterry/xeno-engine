template<size_t T_SIZE>
struct TemplateComplexA{
	int stuff[T_SIZE]; //type int, static array
};

template<typename T1, class T2>
struct TemplateComplexB{
	T1 valueA; // type xen::MetaTParam[0]
	T2 valueB; // type xen::MetaTParam[1]
	T  stuff;  // type T
};

template<u32 T_SIZE, typename T>
struct TemplateComplexC{
	T value; // type xen::MetaTParam[1]
	T* data[T_SIZE]; // type dynamic array of pointer to xen::MetaTParam[1]
};

template<template<typename> T_CONTAINER, typename T_VALUE>
struct TemplateComplexD{
	T_CONTAINER<T_VALUE> container;
};


template<template<typename T1, class T2, size_t T3> T_OUTER, typename T_INNER>
struct TemplateComplexE{
	T_OUTER<T_INNER> container;
};
