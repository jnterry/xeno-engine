////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file tokenizer.cpp
/// \author Jamie Terry
/// \date 2016/07/27
/// \brief Contains utilities for tokenizing a cpp source file
////////////////////////////////////////////////////////////////////////////////

#include "tokenizer.hpp"
#include <xen/core/intrinsics.hpp>

const char* const Token::CONTENT[] = {
	"? bad token ?",
	
	"literal_int", "literal_float", "literal_string", "literal_char",

	"(", ")", "[", "]", "{", "}",
	";", ",", "?", "+", "-",
	"*", "/", "%",
	"*=", "/=", "+=", "-=",
	"==", "=", ">", "<", ">=", "<=",
	"&&", "||", "!",
	"&", "|", "~", "^",
	"&=", "|=", "^=", "!=",
	":", "...",

	"namespace", "class", "struct", "union", "enum",
	"template", "typename",
	"private", "protected", "public", "friend",
	"static", "const", "extern", "constexpr", "unsigned",
	"typedef", "nullptr", "operator", "inline",
	"unknown_identifier",

	"? token_type_count ?",
};
static_assert(XenArrayLength(Token::CONTENT)-1 == Token::Count, "Each token must have corrosponding content string");

static const char* const PREPROCESSOR_DIRECTIVES[] = {
	"include ", "include\"", "include<",
	"define ", "undef ",
	"if ", "ifndef ", "ifdef ",
	"else\n", "else\r", "else\t", "else ", "elif",
	"endif\n", "endif\r", "endif\t", "endif ",
	"line ", "error ", "warning ", "pragma ",
};

//:TODO:COMP: common with loc analyser + probably helpful elsewhere, move into xeno engine?
bool isNewLine   (char c){ return c == '\r' || c == '\n'; }
bool isWhitespace(char c){ return c == ' ' || c == '\t' || c == '\v' || isNewLine(c); }
bool isDigit     (char c){ return '0' <= c && c <= '9'; }
bool isLetter    (char c){ return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z'); }
bool isHexChar   (char c){ return isDigit(c) || ('A' <= c && c <= 'F') || ('a' <= c && c <= 'f'); }

char* eatLine(char* cur){
	bool escaped = false;
	while(*cur && !isNewLine(*cur)){
		escaped = (*cur == '\\');
		++cur;
	}
	while(*cur && isNewLine(*cur)){ ++cur; }
	if(escaped){
		//then the new line char didn't count
		return eatLine(cur);
	} else {
		return cur;
	}
}

void eatAllWhiteSpace(Tokenizer& t){
	while(true){
		while(isWhitespace(*t.position)){ ++t.position; }
		
		if(t.position[0] == '/'){
			if(t.position[1] == '/'){         // then // comment, go to next line
				t.position = eatLine(t.position);
			} else if(t.position[1] == '*'){  // then block comment, keep going until * /
				t.position += 2; //skip opening /*
				while(t.position[0] != '*' || t.position[1] != '/'){
					if(t.position[0] == '\0'){
						xen::log::write(XenError("Unterminated-multi line comment detected", "mtgen.tokenizer"));
						return;
					}
					++t.position;
				}
				t.position += 2; //skip closing */ too
			} else {
				return; //saw /, but not comment, just a divide symbol
			}
		} else {
			return; //found non-whitespace non comment
		}
	}
}

bool isSymbolChar(char c){
	for(int i = Token::FirstSymbol; i < Token::LastAutoDetectSymbol; ++i){
		if(c == Token::CONTENT[i][0]) { return true; }
	}
	return false;
}
	
/// \brief Advances the tokenizer such that tokenizer.current represents the next token
/// Returns false if \0 is reached
/// Sets tokenizeer.current.content to token_buffer and uses the buffer to store the token's content,
/// unless it is a constant value token, eg token Symbol, Eq always has the content "==",
/// in which case tokenizer.current.content is set to some value other than token_buffer
bool advance(Tokenizer& tokenizer, char* token_buffer){
	//////////////////////////////////////////////////////////////////
	// Skip whitespace, comments and preprocessor directives
	eatAllWhiteSpace(tokenizer);
	if(*tokenizer.position == '\0'){ return false; }
	
	if(tokenizer.position[0] == '\\' && isNewLine(tokenizer.position[1])){
		//then this is an escaped new line, skip it
		++tokenizer.position;
		while(isNewLine(tokenizer.position[0])){ ++tokenizer.position; }
		return advance(tokenizer, token_buffer);
	}
	
	if(tokenizer.position[0] == '#'){
		for(u32 i = 0; i < XenArrayLength(PREPROCESSOR_DIRECTIVES); ++i){
			if(xen::stringStartsEqual(&tokenizer.position[1], PREPROCESSOR_DIRECTIVES[i])){
				//then we've found a preprocessor line, skip it
				tokenizer.position = eatLine(tokenizer.position);
				return advance(tokenizer, token_buffer);
			}
		}
	}

   //////////////////////////////////////////////////////////////////
	// Everything we dont care about has been skipped, now work out what
	// the token we've arived at is...
	tokenizer.current.type = Token::BadToken;
	
	//////////////////////////////////////////////////////////////////
	// Handle symbols
	for(int i = Token::FirstSymbol; i <= Token::LastAutoDetectSymbol; ++i){
		//check for double character maches, eg, ==, <=, etc
		if(tokenizer.position[0] == Token::CONTENT[i][0] &&
		   tokenizer.position[1] == Token::CONTENT[i][1]){
			tokenizer.current.type = (Token::Type)i;
			tokenizer.position += 2;
			tokenizer.current.content = Token::CONTENT[i];
			return true;
		}
	}
	for(int i = Token::FirstSymbol; i <= Token::LastAutoDetectSymbol; ++i){
		//check for single character maches, eg, =, <, etc
		if(tokenizer.position[0] == Token::CONTENT[i][0] &&
		   Token::CONTENT[i][1] == '\0'){
			tokenizer.current.type = (Token::Type)i;
			tokenizer.position++;
			tokenizer.current.content = Token::CONTENT[i];
			return true;
		}
	}
	if (tokenizer.position[0] == ':' && tokenizer.position[1] != ':') {
		//then this cant be part of an identifier like xen::Array, emit it as a standalone
		//Symbol::Colon token
		tokenizer.current.type = Token::Colon;
		tokenizer.current.content = Token::CONTENT[Token::Colon];
		++tokenizer.position;
		return true;
	} else if (tokenizer.position[0] == '.' && tokenizer.position[1] == '.' && tokenizer.position[2] == '.'){
		tokenizer.position += 3;
		tokenizer.current.type = Token::Ellipsis;
		tokenizer.current.content = Token::CONTENT[Token::Ellipsis];
		return true;
	}
	
	
	// if we haven't returned yet then the token is not one with fixed content 
	// (eg, Symbol::EqEq content is always "=="), therefore we need to copy the
	// content into a buffer, do setup for that
	char* cur_dest  = &token_buffer[0];
	tokenizer.current.content = token_buffer;

	//////////////////////////////////////////////////////////////////
	// Handle strings and character literals
	if(*tokenizer.position == '"' || *tokenizer.position == '\''){
		char closing_char      = *tokenizer.position;
		tokenizer.current.type = *tokenizer.position == '"' ? Token::String : Token::Char;
		++tokenizer.position; //skip copying the opening character (and ensure loop doesnt terminate immediately)
		while(*tokenizer.position && *tokenizer.position != closing_char){
			*(cur_dest++) = *(tokenizer.position++);
			if(*tokenizer.position == '\\'){
				//then next char is still in string but is escaped, skip it (don't want to terminate early on "\"")
			   *(cur_dest++) = *(tokenizer.position++);
			}
		}
		if (*tokenizer.position) {
			++tokenizer.position; //skip the closing quote
		}
		//:TODO: merge string literals? eg, "test" "stuff" is merged into single string by compiler
		*cur_dest = '\0';
		return true;
	}

	//////////////////////////////////////////////////////////////////
	// Handle number literals + identifiers

	//first copy into tokenizer.current.type up to first whitespace or breaking char, eg a symbol like =
	while(!isWhitespace(*tokenizer.position) && !isSymbolChar(*tokenizer.position)){
		if(tokenizer.position[0] == ':'){
		   if(tokenizer.position[1] != ':'){
			   //don't split on '::' in, eg, xen::Array, but do split on a single colon
			   //(eg, in constructor init list, after case label)
			   break;
		   } else {
			   //copy both :: chars in single iteration of loop so we don't get confused
			   //next iteration (ie: thinking the second : doesn't have a : after it)
			   *(cur_dest++) = *(tokenizer.position++);
		   }
		}
		if(tokenizer.position[0] == '.' && tokenizer.position[1] == '.'){
			break;
		}
	   *(cur_dest++) = *(tokenizer.position++);
	}
	*cur_dest = '\0';

	//now work out what the string is
	char* cur = token_buffer;
	bool valid_identifier = cur[0] == '_' || isLetter(cur[0]);
   valid_identifier |= (cur[0] == ':' && cur[1] == ':'); // global namespace identifier, eg, ::xen::thing
   
   bool seen_point     = cur[0] == '.';
   bool valid_number   = isDigit(cur[0]) || cur[0] == '+' || cur[1] == '-' || seen_point;
   bool valid_float    = valid_number;
   bool valid_int      = valid_number && !seen_point;
   bool first_is_0     = cur[0] == '0';
   bool valid_hex      = first_is_0 && cur[1] == 'x';
   bool valid_octal    = first_is_0 && !valid_hex;
   ++cur; //we've used value of cur[0] to fill in the above bools, now skip it
   while(*cur != '\0'){
	   if(*cur == '.'){
		   if(seen_point) { valid_number = false; }
		   seen_point  = true;
		   valid_hex   = false;
		   valid_octal = false;
		   valid_int   = false;
		   if(cur[-1] == '.' || cur[1] == '.'){ valid_identifier = false; }
	   }

	   if(cur[0] < '0' || cur[0] > '7'){ valid_octal = false; }
	   if(!isHexChar(cur[0]) ){
		   if(cur[0] == 'x' && &tokenizer.current.content[1] == cur){} else { valid_hex = false; }
	   }
	   
	   if(!isDigit(*cur) && *cur != '.'){
		   if(isLetter(*cur) || *cur == '_' || (*cur == ':' && ((cur[-1] == ':') != (cur[1] == ':')))){
			   //then still valid identifer, might no longer be valid number, let's check...
			   if((cur[0] == 'f' || cur[0] == 'd') && cur[1] == '\0'){ 
				   //could be double or float still
			   } else {
				   valid_float = false;
			   }
			   
			   if(cur[0] == 'l' && (cur[1] == '\0' || cur[1] == 'u') ||
			      cur[0] == 'u' && (cur[1] == '\0' || cur[1] == 'l')){
				   //could be unsigned long
			   } else {
				   valid_int = false;
			   }

			   //:TODO: float in scientific notation?
		   } else {
			   valid_int        = false;
			   valid_float      = false;
			   valid_identifier = false;
		   }
	   }
	   ++cur;
   }

   if(valid_number && valid_float && !valid_int){
	   tokenizer.current.type = Token::Float;
   } else if((valid_number && valid_int) || valid_hex || valid_octal){
	   tokenizer.current.type = Token::Int;
   } else if(valid_identifier){
	   //assume unknown identifier...
	   tokenizer.current.type = Token::UnknownIdentifier; 
	   
	   //...check if its actually an known identifier
	   for(int i = Token::FirstIdentifier; i <= Token::LastIdentifier; ++i){
		   if(xen::stringEquals(token_buffer, Token::CONTENT[i])){
			   tokenizer.current.type    = (Token::Type)i;
			   tokenizer.current.content = Token::CONTENT[i];
			   break;
		   }
	   }
   } else {
	   //hopefully this doesn't happen in syntactically correct program
	   xen::log::write(XenWarn("Unable to identify type of token '%s'", tokenizer.current.content, "mtgen.tokenizer"));
   }
	
	return true;
}

xen::Array<Token> tokenize(char* content, xen::ArenaLinear& token_arena, xen::ArenaLinear& token_content_arena){
	XenAssert(&token_arena != &token_content_arena, "Require two seperate arenas since building up result array element by element");
	xen::Array<Token> result;
	xen::ptrAlignForward(&token_arena._next_byte, alignof(Token));
	result.values = (Token*)token_arena._next_byte;
	result.length = 0;

	Tokenizer tokenizer;
	tokenizer.position = content;
	char content_buffer[2048];;
	while(advance(tokenizer, content_buffer)){
	   ++result.length;
	   Token* tok = (Token*)xen::reserve<Token>(token_arena);
	   *tok = tokenizer.current;
	   if(tok->content == content_buffer){
		   //then copy the content out
		   tok->content = xen::pushString(token_content_arena, content_buffer);
	   }
	}

	return result;
}
