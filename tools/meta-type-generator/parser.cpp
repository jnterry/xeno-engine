////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file parser.cpp
/// \author Jamie Terry
/// \date 2016/07/30
/// \brief Contains implementation of functios declared in parser.hpp
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_CPP
#define PARSER_CPP

#include <xen/core/debug.hpp>
#include "parser.hpp"
#include "parser_helpers.cpp"

const char* Entry::TYPE_NAMES[] = {
	"?NoType?",
	"Variable",
	"Struct",
	"Namespace",
	"Union",
	"Enum",
	"Function",
	"Typedef",
};
static_assert(XenArrayLength(Entry::TYPE_NAMES) == Entry::Count, "Must be name string for each entry type");

const char* const ACCESS_MODIFIER_CONTENT[AccessModifier::Count] = {
	"public",
	"protected",
	"private",
};

/// \brief Parses a typename including templates, eg, xen::Array<Token>, if required
/// builds new string in specified arena, returns pointer the the resulting string
const char* parseTypeName(xen::Array<Token>::Iterator& token, xen::ArenaLinear& string_arena){
	const char* result;
	if(!token[1] || token[1]->type != Token::Lt){
		//then no template parameters
		result = token->content;
		++token;
	} else {
		u32 depth = 0;
		result = xen::pushStringNoTerminate(string_arena, token->content);
		++token; //skip first identifier
		do {
			switch(token->type){
			case Token::Lt: ++depth; break;
			case Token::Gt: --depth; break;
			}
			xen::pushStringNoTerminate(string_arena, token->content);
			xen::pushStringNoTerminate(string_arena, " ");
		} while(++token && depth);
		xen::pushString(string_arena, ""); //terminate the string
		if(depth > 0){
			xen::log::write(XenError("Unexpected end of file encountered while parsing templated type name '%s'\n", result));
		}
		
	}
	return result;
}


/// \brief Parses a list of template parameters
/// \param result This is the output, number of params and pointer to first stored in here
/// expects token to initially refer to the first token after the opening <, will terminate with
/// token refering to the token after the closing >
/// Note that result may be cleared to 0 even if successful if a template specialization, eg
/// template<> doStuff<int>(...) ->here the first \c template<> parameter list contains 0 parameters
/// \result True if successful, else false in which case the scope_arena will have been cleared to 0
bool parseTemplateParameters(xen::Array<Token>::Iterator& token, xen::Array<TParam>& result,
                             xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena){
	//:TODO: this doesn't handle vardic template arguments, or default template arguments
	XenAssert(&scope_arena != &string_arena, "Need 2 distinct arenas as building up continous array element by element");

	xen::log::write(XenTrace("Parsing template parameter list", "mtgen.parser.templates"));
	
	u32 depth = 1;
	bool success = true;
	TParam* current = nullptr;
	result = {0};
	result.values = (TParam*)scope_arena._next_byte;
	
	while(token && depth && success){
		switch(token->type){
		case Token::Gt:    --depth;           break; //getting closer to closing tparam list
		case Token::Comma: current = nullptr; break; //start a new tparam
			
		case Token::Typename:          // |
		case Token::Class:             // |
		case Token::Template:          // |
		case Token::UnknownIdentifier: // v Then parse part of current TParam
			if(current == nullptr){
				//then we need to start a new TParam
				current = xen::reserve<TParam>(scope_arena);
				xen::clearToZero(current);
				++result.length;
				current->type_name_type = token->type;
				current->type_name      = token->content;
				if(current->type_name_type == Token::Template){
					//then parse a template template parameter
					//could do recursivly, but we dont need as much infomation about the template template
					//parameter arguments. Also doing recursibly would require smarter way to build up array
					//since atm if we called this function recursivly the resulting TParam array would be embedded
					//inside the one for this call. C++ dictates can only nest template template parameters
					//one level so not much advantage to doing it recursivly anyway.
					++token;
					if(token->type != Token::Lt){
						xen::log::write(XenError("Found template template parameter but following token was not '<', got: '%s'",
						                         token->content, "mtgen.parser.templates"));
						success = false;
					} else {
						++depth;
						++token; // skip the <
						current->template_template_content = (char*)string_arena._next_byte;
						pushTokenContentUntil(token, Token::Gt, string_arena);
						xen::pushString(string_arena, ""); //terminate the string
						// > will be skipped by the last line of this loop
						--depth;
					}
				}
			} else if (current->template_template_content &&
			           (token->type == Token::Typename || token->type == Token::Class)) {
				//then this is:
				// template<template<...> typename T>
				//                        ^^^^^^^^
				// just skip it
			} else if (current->type_name && !current->name) {
				//then this the the tparam's name
				//template<typename T_TYPE>
				//                  ^^^^^^
				current->name = token->content;
			} else {
				xen::log::write(XenError("Template parameter contained too many identifiers, latest: '%s'",
				                         token->content, "mtgen.parser.templates"));
				success = false;
			}
			break;
		default:
			xen::log::write(XenError("Unexpected token encountered in template parameter list, token content: '%s'",
			                         token->content, "mtgen.parser.templates"));
			success = false;
			break;
		} //end switch token->type
		++token;
	}

	if(!success){
		//:TODO: should really rollback string_arena -> look for first non null .template_template_content
		//->may not be any
		xen::log::write(XenError("Error occured parsing template parameter list, skipping to closing >",
		                         "mtgen.parser.templates"));
		xen::rollback(scope_arena, result.values);
		result.values = {0};
		skipNestedBlock(token, Token::Lt, Token::Gt, depth);
	} else {
		for(auto it = xen::begin(result); it; ++it){
			xen::log::write(XenTrace("  PARSED: %s : %s<%s>",
			                         it->name, it->type_name, it->template_template_content,
			                         "mtgen.parser.templates"));
		}
	}

	return success;
}

bool _addField(bool success, Entry* scope, const char* name, TypeData& type_data,
               xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena){
	xen::log::write(XenTrace("GOT FIELD '%s': %s%s%s%s%s, ptr_level: %i",
	                         name,
	                         ((type_data.flags & TypeData::Const)       ? "const " : ""),
	                         ((type_data.flags & TypeData::Static)      ? "static ": ""),
	                         type_data.type_name,
	                         ((type_data.flags & TypeData::Reference)   ? "& "     : ""),
	                         ((type_data.flags & TypeData::StaticArray) ? "[]"     : ""),
	                         type_data.ptr_level,
	                         "mtgen.parser.field"));
	if (success && name && type_data.type_name) {
		Entry* new_entry = getChildEntry(scope, name, scope_arena, string_arena);
		new_entry->type = Entry::Variable;
		new_entry->variable_data.type = type_data;
		return true;
	} else if (success){
		xen::log::write(XenError("Parsed field suposedly successfully but atleast one of member->name and member->type was null",
		                         "mtgen.parser.field"));
		return false;
	} else { //then !success
		return false;
	}
}


bool parseField(xen::Array<Token>::Iterator& token, Entry* scope, xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena){

	TypeData type_data = {0};
	bool parsing       = true;
	bool is_function   = false;
	bool success       = true;
	const char* name   = nullptr;
	
	while(parsing && token && success){
		switch(token->type){
		case Token::Static:
			type_data.flags |= TypeData::Static;
			break;
		case Token::Const:
			if(type_data.ptr_level > 0){
				//then const after *, eg int* const, therefore make the pointer const
				type_data.flags |= TypeData::PtrConst;
			} else {
				//then const before *, eg, const int*, therefore make the type const
				type_data.flags |= TypeData::Const;
			}
			break;
		case Token::Constexpr:
			type_data.flags |= TypeData::Constexpr;
			break;
		case Token::Asterisk:
			if(type_data.type_name != nullptr){
				++type_data.ptr_level;
			} else {
				xen::log::write(XenError("Saw pointer * before type name", "mtgen.parser.field"));
				success = false;
			}
			break;
		case Token::Extern: //just skip it
			break;
		case Token::Ampersand:
			if(type_data.type_name != nullptr){
				type_data.flags |= TypeData::Reference;
			} else {
				xen::log::write(XenError("Saw & before type name", "mtgen.parser.field"));
				success = false;
			}
			break;
		case Token::Eq:
			//then this is probably a const static being assigned, or const in cpp, just skip to end of line
			success |= skipTo(token, Token::Semicolon);
			parsing = false;
			break;
		case Token::OpenBracket:
			type_data.flags |= TypeData::StaticArray;
			success |= skipNestedBlock(token, Token::OpenBracket, Token::CloseBracket, 0);
			--token; //un-skip the closing ], about to skip it as last line of this loop
			break;
		case Token::Semicolon:
			parsing = false;
			break;
		case Token::OpenParen:
			parsing = false;
			is_function = true;
			break;
		case Token::Comma:
			//then composite decleration, eg: int x, y, z;
			if(!type_data.type_name){
				xen::log::write(XenError("Found ',' in field, usually something like \"int x, y;\","
				                         "but field's type has not yet been found",
				                         type_data.type_name, "mtgen.parser.field"));
				success = false;
			} else if(!name){
				xen::log::write(XenError("Found ',' in field, usually something like \"int x, y;\","
				                         "but field name has not yet been found, field type: '%s'",
				                         type_data.type_name, "mtgen.parser.field"));
				success = false;
			} else {
				//then we've got type_name and field name, add the field
				success |= _addField(success, scope, name, type_data, scope_arena, string_arena);
				name = nullptr; //clear name in prep for next field
			}
			break;
		case Token::UnknownIdentifier:
			if(type_data.type_name == nullptr){
				type_data.type_name = parseTypeName(token, string_arena);
				--token; //unskip last token, about to skip it at end of loop
				break;
			} else if(name == nullptr){
				//parse as type name since we want to get template specializations with <...>
				name = parseTypeName(token, string_arena);
				--token; //unskip last token, about to skip it at end of loop
				if(!xen::endsWith(name, "operator")){
					break;
				} //else dont break, fall though to operator function handler below  ************* VVVVVV
				
			} else {
				xen::log::write(XenError("Too many unknown identifiers while parsing field, type: '%s', name: '%s', next: '%s'",
				                         type_data.type_name, name, token->content, "mtgen.parser.field"));
				success = false;
				break;
			}
			/////////////////////////////////////////////////////////////////////////////////////////
			//DONT REARANGE -> UNKNOWNIDENTIFER FALLS THORUGH TO THIS CASE BUNDLE SOMETIMES ****** ^^^^^
		case Token::Operator: //operator overload
		case Token::Inline:   //inline func decleration
		case Token::Tilde:    //~ -> destructor decleration
			is_function = true;
			success |= skipTo(token, Token::OpenParen);
			--token; //unskip the token before the OpenParen as about to skip at end of this loop
			break;
			/////////////////////////////////////////////////////////////////////////////////////////
		default:
			xen::log::write(XenError("Unexpected token encountered while parsing field: %s %s, token: '%s'",
			                         type_data.type_name, name, token->content, "mtgen.parser.field"));
			success = false;
			break;
		} //end of switch token->type
		++token;
	} // end of while parsing && successful loop

	if(is_function){
		xen::log::write(XenTrace("Skipping function: %s %s", type_data.type_name, name, "mtgen.parser.field"));

		skipNestedBlock(token, Token::OpenParen, Token::CloseParen, 1); //skip to end of parameter list

		if(token->type == Token::Const){
			//then function is delcared as const, skip the const token and carry on as normal
			++token;
		}

		if(token->type == Token::Colon){
			//then we've found a constructor init list, skip to the {
			success |= skipTo(token, Token::OpenBrace);
		}

		if(token){
			switch(token->type){
			case Token::OpenBrace:
				//then function has associated body
				++token;
				success |= skipNestedBlock(token, Token::OpenBrace, Token::CloseBrace, 1);
				break;
			case Token::Semicolon:
				//function decleration only, no body, skip the ;
				++token;
				break;
			default:
				xen::log::write(XenWarn("While skipping function '%s %s' did not find { or ; at end of decleration. "
				                        "Could be legal macro call with no ;, or function parsing is broken.",
				                        type_data.type_name, name, "mtgen.parser.field"));
				break;
			}
		}
	} else { //not a function so must be a field
		success |= _addField(success, scope, name, type_data, scope_arena, string_arena);
	}
	
	return success;
}

/// \brief Parses a typedef and adds it to the parent_scope :TODO: add typedefs
/// \param token Token iterator, should initially refer a token of type Token::Typedef,
/// when function returns will refer to the token after the ; ending the typedef
bool parseTypedef(xen::Array<Token>::Iterator& token, Entry* parent_scope, xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena){
	++token; //skip the token 'typedef'
	while(token && token[1]->type != Token::Semicolon){
		++token;
	}
	if(!requireNoEndOfFile(token, "typedef")){ return false; }
	xen::log::write(XenTrace("Skipped typedef for '%s'", token->content, "mtgen.parser.typedef"));
	++token; //skip typedef's name
	requireToken(token, Token::Semicolon, "typedef decleration");
	return true;
}

/// \brief Parses a scope opener, eg:
/// \code
/// struct Test;
/// namespace thing;
/// enum Lump;
/// \endcode
/// If opener is followed by scope body (ie: { } rather than ;) then calls
/// appropriate function to parse that too
/// \param token When calling should refer to the scope opener token, ie
/// token->type == Token::Namespace or Token::Struct, etc
/// When the function returns it will refer to the token after the scope closing
/// token (ie: ; after forward decerlation, } after namespace body, }; after struct body, etc)
/// \param tparams The result of the template decleration just before the scope, {0} if there
/// isnt one
bool parseScopeOpener(xen::Array<Token>::Iterator& token, xen::Array<TParam> tparams, Entry* parent_scope,
                      xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena){

	Entry::Type entry_type        = Entry::NoType;
	AccessModifier default_access = AccessModifier::Public;
	switch(token->type){
	case Token::Namespace:
		entry_type     = Entry::Namespace;
		break;
	case Token::Class:
		default_access = AccessModifier::Private;
	case Token::Struct: // ^ not breaking on purpose!
		entry_type = Entry::Struct;
		break;
	case Token::Union:
		entry_type = Entry::Union;
		break;
	case Token::Enum:
		entry_type = Entry::Enum;
		break;
	default: XenInvalidCodePath;
	}
	++token; //skip scope opener ("enum", "namespace", "struct", etc) now we've captured the type
	if(!requireNoEndOfFile(token, "scope opener")){ return false; }

	if(!isIdentifier(*token)){ //:TODO: unknown identifier only?
		//:TODO: handle anonomous scopes (only unions and structs in unions right?)
		xen::log::write(XenError("Scope opener '%s' was not followed by an identifier. Following token: '%s'",
		                         Entry::TYPE_NAMES[entry_type], token->content)), "mtgen.parser.scope_opener";
		return false;
	}

	const char* scope_name = parseTypeName(token, string_arena);
	Entry* new_scope = getChildEntry(parent_scope, scope_name, scope_arena, string_arena);
	if(new_scope->type != Entry::NoType && new_scope->type != entry_type){
		xen::log::write(XenError("Found scope: '%s' declared as two different types, original: '%s', new: '%s'",
		                         new_scope->full_name, Entry::TYPE_NAMES[new_scope->type],
		                         Entry::TYPE_NAMES[entry_type], "mtgen.parser.scope_opener"));
		return false;
	}
	new_scope->type = entry_type;
	new_scope->tparams = tparams;

	if(!requireNoEndOfFile(token, "scope opener")){ return false; }

	if(token->type == Token::Colon){
		++token; //skip the :
		if(new_scope->type == Entry::Enum){
			//then this is storage specifier for the enum
			if(!token) { return false; } //:TODO: expectAdvance func which system exits if err so dont need checks everywhere

			if (token->type == Token::Unsigned){
				++token;
				if(!token){ return false; }
				//:TODO: capture fact its unsigned
			}
			
			if(token->type != Token::UnknownIdentifier){
				xen::log::write(XenError("Found enum decleration for '%s', name was followed by ':' but then got unexpected token '%s'",
				                         new_scope->name, token->content, "mtgen.parser.scope_opener"));
				return false;
			}
			
			++token; //skip storage specifier :TODO: capture it
			if(!token){ return false; }
			//carry on to the switch parse of body
		} else if(new_scope->type == Entry::Struct){
			AccessModifier amod = default_access;
			new_scope->struct_data.bases.values = (BaseClass*)scope_arena._next_byte;
			while(token && token->type != Token::OpenBrace){
				switch(token->type){
				case Token::Public:    amod = AccessModifier::Public;    break;
				case Token::Protected: amod = AccessModifier::Protected; break;
				case Token::Private:   amod = AccessModifier::Private;   break;
				case Token::Comma:     amod = default_access;            break;
				case Token::UnknownIdentifier:{
					BaseClass* bc = xen::reserve<BaseClass>(scope_arena);
					++new_scope->struct_data.bases.length;
					bc->access_modifier = default_access;
					bc->type_name = parseTypeName(token, string_arena);
					--token; //unskip last token, about to skip at end of this loop
					break;
				}
				default:
					xen::log::write(XenError("ERROR: Unexpected token in struct '%s' base class list, token: '%s'",
					                         new_scope->name, token->content, "mtgen.parser.scope_opener"));
					return false;
				}//end switch
				++token;
			}
			if(!requireNoEndOfFile(token, "struct decleration base class list")){ return false; }
		} else {
			xen::log::write(XenError("Unexpected ':' after scope decleration %s %s",
			                         Entry::TYPE_NAMES[new_scope->type], new_scope->name, "mtgen.parser.scope_opener"));
			return false;
		}
	}	
	
	switch(token->type){
	case Token::Semicolon:
		// then this is just a forward decleration, skip it
		// (getChildScope has now registred it in hierachy, but thats fine since we can
		// use the forward decleration in name resolution)
		++token;
		return true;
	case Token::OpenBrace:
		//exit switch and fall through to body parsing
		break;
	default:
		xen::log::write(XenError("Found decleration for '%s %s' but it was followed by unexpected token: '%s'",
		        Entry::TYPE_NAMES[entry_type], new_scope->name, token->content, "mtgen.parser.scope_opener"));
		return false;
	}

	//if got to here then need to parse the scope's body
	++token; //skip the {

	if(new_scope->type == Entry::Enum){
		xen::log::write(XenDebug("Found enum '%s', parsing not implemented yet, skiping",
		                        new_scope->full_name, "mtgen.parser.scope_opener"));
		//:TODO: parse enum
		return skipNestedBlock(token, Token::OpenBrace, Token::CloseBrace, 1);
	} else {
		return parseScopeBody(token, new_scope, default_access, scope_arena, string_arena, false);
	}
}

bool parseScopeBody(xen::Array<Token>::Iterator& token, Entry* scope, AccessModifier default_access_modifier,
                    xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena, bool global_scope){
	xen::log::write(XenDebug("Begining parse of scope type: '%s', name: '%s'",
	                         Entry::TYPE_NAMES[scope->type], scope->full_name, "mtgen.parser.scope"));

	bool success = true;
	u32 brace_depth = 1;
	xen::Array<TParam> tparams = {0};
	AccessModifier access_modifier = default_access_modifier;

	while(token && brace_depth && success){
		//printToken(tokens[i]);
		switch(token->type){
		case Token::OpenBrace:  ++brace_depth; ++token; break;
		case Token::CloseBrace: --brace_depth; ++token; break;
		case Token::Semicolon:
			// random extra ; is valid C/C++, just skip them
			++token;
			break;
		case Token::Public:
		case Token::Protected:
		case Token::Private:
			access_modifier = getAccessModifierFromKeyword(token->type);AccessModifier::Public;
			++token;
			success |= requireToken(token, Token::Colon, "After access modifier");
			break; 
		case Token::Friend: //we dont care about friend declerations, skip
			success |= skipTo(token, Token::Semicolon);
			break;
		case Token::Typedef:
			success |= parseTypedef(token, scope, scope_arena, string_arena);
			break;
		case Token::Template: //then parse a template parameter list to attach to next thing we encounter
			++token;
			success |= requireToken(token, Token::Lt, "keyword 'template'");
			if(!success) { break; }
			success |= parseTemplateParameters(token, tparams, scope_arena, string_arena);
			break;
		case Token::Namespace:
		case Token::Struct:
		case Token::Class:
		case Token::Union:
		case Token::Enum:
			parseScopeOpener(token, tparams, scope, scope_arena, string_arena);
			tparams = {0}; //reset tparams now we've parsed the scope they are for
			break;
		case Token::Extern:
		case Token::Const:
		case Token::Static:
		case Token::Constexpr:
		case Token::Inline:
		case Token::Tilde: //destructor
		case Token::Operator: //operator overload
		case Token::UnknownIdentifier:
			// then this is a type qualifier or some unknown identifier rather than a keyword,
			// presumably a typename, either way try and parse a field
			success |= parseField(token, scope, scope_arena, string_arena);
			tparams = {0}; //reset tparams now we've parsed the field they are for
			break;
		default:
			xen::log::write(XenError("Unexpected token '%s' in scope: '%s'",
			                         token->content, scope->full_name, "mtgen.parser.scope"));
			success = false;
			break;
		} //end of switch token->type
	} //end of while parsing scope loop

	if(success && !global_scope && brace_depth > 0){
		xen::log::write(XenError("End of file reached before all scopes were closed (not enough } )", "mtgen.parser.scope"));
		success = false;
	}

	xen::log::write(XenMsg(success ? xen::log::DEBUG : xen::log::ERROR,
	                       "Ending parse of scope '%s %s', successful: %i", Entry::TYPE_NAMES[scope->type],
	                       scope->full_name, success,  "mtgen.parser.scope"));
	return success;
}


#endif
