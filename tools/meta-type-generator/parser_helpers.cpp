////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file parser_helpers.cpp
/// \author Jamie Terry
/// \date 2016/07/30
/// \brief Contains various helper functions for the parser
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_HELPERS_CPP
#define PARSER_HELPERS_CPP

/// \brief Skips a token iterator forward such that it refers to the token after a a series
/// of nested blocks
/// eg, { { } }, ( ( ) ), < < > >, etc.
/// \param token The iterator to advance
/// \param opener The token type which opens a block
/// \param closer The token type which closes a block
/// \param starting_depth The depth of block the iterator is starting in, opener
/// increases it by 1, closer decreases it by 1, functions stops when depth = 0
/// \return True if depth reached 0 before end of token stream was reached, else false
bool skipNestedBlock(xen::Array<Token>::Iterator& token, Token::Type opener, Token::Type closer, u32 starting_depth){
	u32 depth = starting_depth;
	
	do{
		if(token->type == opener){
			++depth;
		} else if(token->type == closer){
			--depth;
		}
		++token;
	} while (token && depth);
	if(depth > 0){
		xen::log::write(XenError("Attempted to skip nested block delimeted by %s, %s "
		                         "but end of file reached before scope was closed (not enough %s )",
		                         Token::CONTENT[opener], Token::CONTENT[closer], Token::CONTENT[closer],
		                         "xen.parser"));
		return false;
	} else {
		return true;
	}
}

bool skipTo(xen::Array<Token>::Iterator& token, Token::Type target){
	while(token && token->type != target){
		++token;
	}
	if(!token){
		xen::log::write(XenError("Attempted to skip until a token of type %s, but end of file was "
		                         "reached before the target was encountered", Token::CONTENT[target],
		                         "xen.parser"));
		return false;
	} else {
		return true;
	}
}


/// \brief Pushes the content of tokens onto end of string arena until a token of the specified
/// type is encountered. The built up string is not null terminated. After the function returns
/// \c token will refer to a token of the specified type.
/// \return True if the specified symbol was encountered before end of file, else false
bool pushTokenContentUntil(xen::Array<Token>::Iterator& token, Token::Type target, xen::ArenaLinear& string_arena){
	while(token && token->type != target){
		xen::pushStringNoTerminate(string_arena, token->content);
		xen::pushStringNoTerminate(string_arena, " ");
		++token;
	}
	return (bool)token;
}

Entry* getChildEntry(Entry* parent, const char* name, xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena){
	for(Entry* cur = parent->first_child; cur; cur = cur->next_sibling){
		if(xen::stringEquals(cur->name, name)){
			return cur;
		}
	}
	
	//if not returned then specified entry doesn't exist as child of parent
	Entry* result = xen::reserve<Entry>(scope_arena);
	xen::clearToZero(result);
	
	result->full_name = xen::pushStringNoTerminate(string_arena, parent->full_name);
	if(parent->full_name[0]){
		xen::pushStringNoTerminate(string_arena, "::");
	}
	result->name = xen::pushString(string_arena, name);
	result->next_sibling = parent->first_child;
	parent->first_child = result;
	return result;
}

AccessModifier getAccessModifierFromKeyword(Token::Type keyword){
	switch(keyword){
	case Token::Public:    return AccessModifier::Public;
	case Token::Protected: return AccessModifier::Protected;
	case Token::Private:   return AccessModifier::Private;
	default:
		XenInvalidCodePath;
		return AccessModifier::Public;
	}
}

/// \brief Checks that the token is of the specified type, if skips it and returns true
/// otherwise prints error message saying required \c token after \c after_what
/// with details about what was got instead, the returns false -> in this case does not advance
/// token
bool requireToken(xen::Array<Token>::Iterator& token, Token::Type type, const char* after_what){
	if(token->type == type){
		++token;
		return true;
	} else {
		xen::log::write(XenError("Required token '%s' after '%s' but got '%s'\n",
		                         Token::CONTENT[type], after_what, token->content, "mtgen.parser"));
		return false;
	}
}

/// \breif Checks the token iterator is not at EOF, if so logs error and returns false
/// \param during String to include in error message of form 'Unexpected end of file during %s'
bool requireNoEndOfFile(xen::Array<Token>::Iterator& token, const char* during){
	if(!token){
		xen::log::write(XenError("Unexpected end of file encountered during %s", during, "mtgen.parser"));
		return false;
	}
	return true;
}

#endif
