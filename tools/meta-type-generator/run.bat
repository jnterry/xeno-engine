@echo off

echo.
echo Building
echo --------
echo.

call build.bat

echo.
echo Running
echo -------
echo.

mtgen.exe %*
