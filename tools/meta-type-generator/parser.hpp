////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file parser.hpp
/// \author Jamie Terry
/// \date 2016/07/30
/// \brief Contains code for parsing a stream of tokens produced by the Tokenizer
/// and converting into a tree of types we're intressted in that can be used to
/// generate the code which builds MetaType's descirbing the types found
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_HPP
#define PARSER_HPP

#include "tokenizer.hpp"

enum class AccessModifier{
	Public,
	Protected,
	Private,
	Count,
};
extern const char* const ACCESS_MODIFIER_CONTENT[AccessModifier::Count];

struct TParam{	
	// name of the parameter
	// template<typename T_TYPE> -> "T_TYPE"
	const char* name; 

	///< the type of type_name, eg, Typename, Class, Unknown (if some other type,
	///< eg in template<size_t T>
	Token::Type type_name_type;

	// the type the tparam is declared with
	// template<typename T>                 -> typename
	// template<template<typename> class T> -> template
	// template<size_t T>                   -> size_t
	const char* type_name;

	// the content of the template template parameter, or nullptr
	// if this isnt a template template
	// eg, in template<template<typename, size_t> T> this is "typename, size_t"
	// in template<size_t T_SIZE> this is nullptr
	const char* template_template_content;
};

struct BaseClass{
	const char* type_name;
	AccessModifier access_modifier;
};

struct TypeData{
	enum Flags{
		Const        = 0x01, //if type is const                       (ie: const int*)
		PtrConst     = 0x02, //if is pointer whether the ptr is const (ie: int* const)
		Static       = 0x04,
		Reference    = 0x08,
		StaticArray  = 0x10,
		Volatile     = 0x20,
		Mutable      = 0x40,
		Constexpr    = 0x80,
   };

	const char* type_name; ///< name of the type, eg int, u32, xen::Array<int*>, etc
	u8 ptr_level;          ///< number of pointers, int -> 0, int** -> 2
	u8 flags;              ///< extra flags qualifing the type
};

struct Entry{
	enum Type{
		NoType = 0,   // value given to a new entry when it is first created
		Variable,     // member variable or global var in namespace
		Struct,
		Namespace,
		Union,
		Enum,
		Function, // a function (in a namespace or in a struct)
		Typedef,  //either typedef or using directive
		Count,
	};
	/// \brief The type of this entry -> depending on the value of this different
	/// data is stored in the union
	Type type;

	/// \brief Fully qualified name of this entry, eg, xen::log::Message::time
	const char* full_name;

	/// \brief Name of this entry relative to its scope, eg: time for above
	/// may point into full_name string to save space
	const char* name;
	
	//tparams declared just before this entry, should only apply to structs
	//or functions, but we're lazy and just look for them in front of any
	//decleration to simplify the code - no point us doing too much error
	//checking, our messages are never going to be better than the compilers
	xen::Array<TParam> tparams; 

	/// \brief Pointer to next Entry with the same parent as this 
	Entry* next_sibling;

	/// \brief Pointer to the first child of this
	Entry* first_child;

	// data specific to the type
	union{
		struct {
			TypeData       type;   //the type of the variable
			AccessModifier access; //public/protected/private
		} variable_data;

		struct {
			xen::Array<BaseClass> bases; //array of pointers to names of base classes
		} struct_data;

		struct {
			u32          count;  //number of posibilities
			s64*         values; //array of the values of each entry, length = count
			const char** names;  //array of the names of each entry,  length = count
		} enum_data;
		
		struct {
			TypeData       return_type;
			u32            parameter_count;
			TypeData*      parameter_types;
			const char**   parameter_names;
			AccessModifier access;
		} function_data;

		struct {
			//the type we're creating an alias for (the actual alias is given by the .name field
			TypeData* type; 
		} typedef_data;

	};

	static const char* TYPE_NAMES[];
};

bool parseScopeBody(xen::Array<Token>::Iterator& token, Entry* root_scope, AccessModifier default_access,
                    xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena,
                    bool global_scope);


#endif
