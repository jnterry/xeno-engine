////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \author Jamie Terry
/// \date 2016/07/27
/// \brief Contains main functions for the Meta Type Info Generator. This parses
/// the engine code in order to extract infomation about the types
////////////////////////////////////////////////////////////////////////////////

#define XEN_LOG_NO_TRACE 1


#include <cstdio>

#include <xen/xen-unity.cpp>
#include "tokenizer.cpp"
#include "parser.cpp"

bool processFile(xen::Path& path, xen::ArenaLinear& tmp_arena, xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena, Entry* root_scope){
	printf("\n\n------------------------------------\n");
	printf("Processing file: %s\n", (char*)path);
	printf("------------------------------------\n");
	char* file_content = xen::loadFile(path, tmp_arena);

	xen::log::write(XenInfo("Tokenizing file '%s'", (char*)path, "mtgen.main"));
	xen::Array<Token> tokens = tokenize(file_content, tmp_arena, string_arena);

	xen::log::write(XenInfo("Parsing file '%s'", (char*)path, "mtgen.main"));
	auto token = xen::begin(tokens);
	bool success = parseScopeBody(token, root_scope, AccessModifier::Public, scope_arena, string_arena, true);
	/*bool success = true;
	for(auto it = xen::begin(tokens); it; ++it){
		printToken(*it);
		}*/
	
	xen::rollback(tmp_arena, file_content);
	return success;
}

bool processDirectory(xen::Path& path, xen::ArenaLinear& tmp_arena, xen::ArenaLinear& scope_arena, xen::ArenaLinear& string_arena, Entry* root_scope){
	printf("\n\n------------------------------------\n");
	printf("Processing directory %s\n", (char*)path);
	printf("------------------------------------\n");
	for(auto it = xen::iterateDirectory(path); it; ++it){
		if(it.attributes & xen::FileIterator::TYPE_FILE){
			char* extension = xen::findLast('.', it.name);
			if(extension[0] == '\0'){
				printf("Found extensionless file: %s/%s\n", (char*)path, it.name);
			} else if (xen::stringEquals(extension, ".cpp") || xen::stringEquals(extension, ".c") ||
			           xen::stringEquals(extension, ".hpp") || xen::stringEquals(extension, ".h")){
				xen::push(path, it.name);
				processFile(path, tmp_arena, scope_arena, string_arena, root_scope);
				xen::pop(path);
			} else {
				printf("\n\n------------------------------------\n");
				printf("Found unknown file type: %s/%s\n", (char*)path, it.name);
				printf("------------------------------------\n");
			}
		} else if (it.attributes & xen::FileIterator::TYPE_DIRECTORY){
			xen::push(path, it.name);
			processDirectory(path, tmp_arena, scope_arena, string_arena, root_scope);
			xen::pop(path);
		}
	}

	return true;
}

void printEntry(Entry* entry, s32 depth = 0){
	for(s32 i = 0; i < depth; ++i){
		printf("| ");
	}
	printf(":: %s - %s", Entry::TYPE_NAMES[entry->type], entry->name);
	if(entry->tparams.length != 0){
		printf("<");
		for(auto it = xen::begin(entry->tparams); it; ++it){
			printf("%s ", it->type_name);
			if(it->template_template_content){
				printf("<%s> ", it->template_template_content);
			}
			printf("%s", it->name);
			if(it[1]){ printf(", "); }
		}
		printf(">");
	}
	if (entry->type == Entry::Variable) {
		printf(": ");
		if (entry->variable_data.type.flags & TypeData::Static) {
			printf("static ");
		}
		if (entry->variable_data.type.flags & TypeData::Const) {
			printf("const ");
		}
		printf("%s", entry->variable_data.type.type_name);
		if (entry->variable_data.type.flags & TypeData::Reference) {
			printf("& ");
		}
		for (u8 i = 0; i < entry->variable_data.type.ptr_level; ++i) {
			printf("*");
		}
		if (entry->variable_data.type.flags & TypeData::StaticArray) {
			printf("[]");
		}
	} else if (entry->type == Entry::Struct){
		if(entry->struct_data.bases.length){
			printf(": ");
			for(auto it = xen::begin(entry->struct_data.bases); it; ++it){
				printf("%s %s, ", ACCESS_MODIFIER_CONTENT[(u32)it->access_modifier], it->type_name);
			}
		}
	}
	
	printf("\n");
	for(Entry* cur = entry->first_child; cur; cur = cur->next_sibling){
		printEntry(cur, depth + 1);
	}
}

int main(int argc, const char* argv[]){
	if(argc < 3){
		printf("Not enough arguments! Usage: 'mtgen output_file input_directory...'\n");
		return 1;
	}

	xen::initializeSystemMemory();
	xen::initializeSystemLog(xen::getRootAllocator(), xen::megabytes(1));
	printf("\n\n\n\n\n");
	xen::ArenaLinear* tmp_arena    = xen::allocateArenaLinear(xen::getRootAllocator(), xen::megabytes(64));
	xen::ArenaLinear* scope_arena  = xen::allocateArenaLinear(xen::getRootAllocator(), xen::megabytes(64));
	xen::ArenaLinear* string_arena = xen::allocateArenaLinear(xen::getRootAllocator(), xen::megabytes(64));

	Entry* root_scope = xen::reserve<Entry>(*tmp_arena);
	xen::clearToZero(root_scope); //:TODO: allocation flags that will clear to 0 (want to do this more often than change alignment, put before)
	root_scope->full_name    = "";
	root_scope->name         = "";
	root_scope->type         = Entry::Namespace;	
	xen::Path path = xen::makePath((char*)xen::reserveBytes(*tmp_arena, 512), 512, argv[2]);
	
	processDirectory(path, *tmp_arena, *scope_arena, *string_arena, root_scope);

	printEntry(root_scope);
	
	xen::deallocate(xen::getRootAllocator(), string_arena);
	xen::deallocate(xen::getRootAllocator(), scope_arena);
	xen::deallocate(xen::getRootAllocator(), tmp_arena);
	printf("\n\n\n\n\n\n\n");
	xen::shutdownSystemLog();
	xen::shutdownSystemMemory();
}
