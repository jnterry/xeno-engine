////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file tokenizer.hpp
/// \author Jamie Terry
/// \date 2016/07/26
/// \brief Contains Tokenizer type and associated functions
////////////////////////////////////////////////////////////////////////////////

#ifndef TOKENIZER_HPP
#define TOKENIZER_HPP

#include <xen/core/debug.hpp>

struct Token{
	enum Type{
		BadToken, //invalid token/we cant work out what it is

		////////////////////////////////////////////
		//Literals
		Int, Float,
		String, // "text"
		Char,   // 'a'

		FirstLiteral = Int,
		LastLiteral = Char,

		////////////////////////////////////////////
		//Symbols
		OpenParen, CloseParen,   // ( )
		OpenBracket,  CloseBracket,// [ ]
		OpenBrace,  CloseBrace,   // { }
   
		Semicolon, Comma, QuestionMark, Plus, Minus, 
		Asterisk, Multiply = Asterisk,
		Slash, Divide = Slash, // '/'
		Percent, Mod = Percent,
	   MultiplyEq, DivideEq, PlusEq, MinusEq,   // *= etc

		//==  =   >   <    >=       <=
		EqEq, Eq, Gt, Lt, GtOrEq, LtOrEq,

		And, Or, Not,      // &&   ||  !
		
		BitAnd, Ampersand = BitAnd,  // &
		BitOr,    // |
		BitNot, Tilde = BitNot, // ~
		BitXor,   // ^
		BitAndEq, BitOrEq, BitXorEq, NotEq, // &=   |=   ^=   !=

		LastAutoDetectSymbol = NotEq,
		//things below here are handled in a special way rather than just checked
		//for with exact string matches

		// we don't want namespace::struct to be split by colon, but we
		// do want to detect : (eg, after a case label), this is handled specially
		Colon,

		//auto detect only works with 2 or 1 character symbols (also dont want to
		//split on single '.', eg vec.x)
		Ellipsis,

		FirstSymbol = OpenParen,
		LastSymbol  = Ellipsis,

		////////////////////////////////////////////
		//Identifiers
		Namespace, Class, Struct, Union, Enum,
		FirstScope = Namespace, LastScope = Enum,
		
		Template, Typename,
		Private, Protected, Public, Friend,
		Static, Const, Extern, Constexpr, Unsigned,
		Typedef, Nullptr, Operator, Inline,
			
		UnknownIdentifier, //some random identifier, user defined or a keyword we don't care about

		FirstIdentifier = Namespace, LastIdentifier = UnknownIdentifier,
	   ////////////////////////////////////////////
		
		Count, //number of token types
	};
	// array of strings containing the content of fixed tokens
	// eg, And is always "", but UnknownIdentifier varies, hence content
	// field may not point into this array
	static const char* const CONTENT[];

	const char* content;
	Type type;
};

struct Tokenizer {
	char* position;   // current location in file
	Token current;
};

/// \brief Advances the Tokenizer to the next token, updates \c tokenizer.current
/// \return True if new token found, false if end of file reached
bool advance(Tokenizer& tokenizer);

xen::Array<Token> tokenize(char* content, xen::ArenaLinear& token_arena, xen::ArenaLinear& token_content_arena);

bool isIdentifier(const Token& token){
	return token.type >= Token::FirstIdentifier && token.type <= Token::LastIdentifier;
}

bool isSymbol(const Token& token){
	return token.type >= Token::FirstSymbol && token.type <= Token::LastSymbol;
}

void printToken(Token& token){
	printf("%18s - %s\n", Token::CONTENT[token.type], token.content);
}

#endif
