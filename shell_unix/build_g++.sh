#!/bin/bash

cd $(dirname "${0}")
SCRIPTDIR=$(pwd -L)
cd -

OUTPUTDIR=$SCRIPTDIR/../bin/quicktest/g++_unix

mkdir -p $OUTPUTDIR

cd $OUTPUTDIR

rm -f ./quicktest

g++ ../../../examples/quicktest/main.cpp -o $OUTPUTDIR/quicktest -I../../../engine -std=c++11 -g -Wall

