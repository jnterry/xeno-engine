@echo off

set build_script=%1

IF [%1] == [] GOTO BAD_USAGE

echo.
echo Building quicktest
echo ------------------
echo.

IF NOT EXIST %~dp0build_%build_script%.bat GOTO BAD_USAGE

set bt_id=old
for /F "delims=" %%A in ('%~dp0build-time.exe start %dp0%..\build_time "Build Engine" "compiler=%build_script%" "type=Debug" "os=Windows" "unity=1" "computer_name=%COMPUTERNAME%"') do (
	set bt_id=%%A
)
call %~dp0\build_%build_script%.bat
%~dp0build-time end %dp0%..\build_time %bt_id% %ERRORLEVEL%
%~dp0build-time show %dp0%..\build_time %bt_id%

echo.
echo Running  quicktest
echo ------------------
echo.

%~dp0..\bin\quicktest\%build_script%\quicktest.exe

EXIT /B 0

:BAD_USAGE
echo Usage: -run [compiler_name]
echo Must be a corrosponding build_[compiler_name].bat in: %~dp0

EXIT /B 1
