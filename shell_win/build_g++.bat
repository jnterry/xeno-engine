@echo off

set executable_output_dir=%~dp0\..\bin\quicktest\g++

IF NOT EXIST %executable_output_dir% mkdir %executable_output_dir%

pushd %executable_output_dir%

del *.exe > NUL 2> NUL

g++ ..\..\..\examples\quicktest\main.cpp -o %executable_output_dir%\quicktest.exe -I../../../engine -std=c++11 -g -Wall

popd
