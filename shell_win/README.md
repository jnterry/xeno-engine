# /shell_win

This directory contains a number of batch files and executables which are designed to be run from the command line while developing Xeno Engine on windows.


This folder is automattically (temporarly) added to PATH when running shell.bat in the root directory. This then enables you to run any of the these utilities from that command line session, from any directory.

## run

The run command will automattically build and then run the example "quicktest", which is the primary method of testing the engine as features are being developed.

This bat calls the build bats to actually build the engine, these may be called independenly if you just wish to build without running

## loc

This is the executable produced by \tools\loc-analyser, it can be called to count the lines of code in the current working directory, and its sub directories.