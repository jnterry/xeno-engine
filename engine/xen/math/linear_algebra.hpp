////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file linear_algebra.hpp
/// \author Jamie Terry
/// \date 2016/07/19
/// \brief Contains definitions of various types for representing mathmatical
/// objects involved in linear algebra (vectors, matricies, etc)
///
/// \ingroup math
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_MATH_LINEAR_ALGEBRA_HPP
#define XEN_MATH_LINEAR_ALGEBRA_HPP

#include "../core/intrinsics.hpp"

#define XEN_IMPL_MATH_VEC_BODY(element_count) \
   constexpr static const u32 COUNT = element_count; \
	Vec(){} \
	const T_TYPE& operator[](const u32 index) const { return elements[index]; } \
	T_TYPE&       operator[](const u32 index)       { return elements[index]; }

namespace xen{
	/// \brief Represents a compile time fixed sized vector with elements of T_TYPE
	template<u32 T_COUNT, typename T_TYPE>
	struct Vec{
		T_TYPE elements[T_COUNT];

		XEN_IMPL_MATH_VEC_BODY(T_COUNT)
	};

	template<typename T_TYPE>
	struct Vec<2, T_TYPE>{
		union{
			T_TYPE elements[2];
			struct{ T_TYPE x, y; };
			struct{ T_TYPE u, v; };
		};
		
		XEN_IMPL_MATH_VEC_BODY(2)

		static const Vec<2, T_TYPE> UnitX, UnitY, Origin;
		static const Vec<2, T_TYPE> Unit[2];
	};
	template<typename T_TYPE> const Vec<2, T_TYPE> Vec<2, T_TYPE>::UnitX(1,0,0);
	template<typename T_TYPE> const Vec<2, T_TYPE> Vec<2, T_TYPE>::UnitY(0,1,0);
	template<typename T_TYPE> const Vec<2, T_TYPE> Vec<2, T_TYPE>::Origin(0,0,0);
	template<typename T_TYPE> const Vec<2, T_TYPE> Vec<2, T_TYPE>::Unit[2] = {{1,0}, {0,1}};

	template<typename T_TYPE>
	struct Vec<3, T_TYPE>{
		Vec(T_TYPE nx, T_TYPE ny, T_TYPE nz) : x(nx), y(ny), z(nz) {}
		
		union{
			T_TYPE elements[3];
			struct{ T_TYPE x, y, z;                     };
			struct{ T_TYPE u, v;                        };
			struct{ Vec<2, T_TYPE> uv;                  };
			struct{ Vec<2, T_TYPE> xy;                  };
			struct{ T_TYPE _unused1; Vec<2, T_TYPE> yz; };
		};
		
		XEN_IMPL_MATH_VEC_BODY(3)

		static const Vec<3, T_TYPE> UnitX, UnitY, UnitZ, Origin;
		static const Vec<3, T_TYPE> Unit[3];
		//static const Vec<3, Vec<3,T_TYPE>> Unit; //:TODO: kills gcc, infinite template recursion
	};
	template<typename T_TYPE> const Vec<3, T_TYPE> Vec<3, T_TYPE>::UnitX(1,0,0);
	template<typename T_TYPE> const Vec<3, T_TYPE> Vec<3, T_TYPE>::UnitY(0,1,0);
	template<typename T_TYPE> const Vec<3, T_TYPE> Vec<3, T_TYPE>::UnitZ(0,0,1);
	template<typename T_TYPE> const Vec<3, T_TYPE> Vec<3, T_TYPE>::Origin(0,0,0);
	template<typename T_TYPE> const Vec<3, T_TYPE> Vec<3, T_TYPE>::Unit[3] = {{1,0,0}, {0,1,0}, {0,0,1}};
	//template<typename T_TYPE> 
	//const Vec<3, Vec<3, T_TYPE>> Vec<3, T_TYPE>::Unit = {{1,0,0}, {0,1,0}, {0,0,1}}; //:TODO: kills gcc, infinite template recurison
	
	template<typename T_TYPE>
	struct Vec<4, T_TYPE>{
		union{
			T_TYPE elements[4];
			struct{ T_TYPE x, y, z, w;                   };
			struct{ Vec<2, T_TYPE> xy, zw;               };
			struct{ Vec<2, T_TYPE> uv;                   };
			struct{ Vec<3, T_TYPE> xyz;                  };
			struct{ T_TYPE u, v;                         };
			struct{ T_TYPE _unused1; Vec<3, T_TYPE> yzw; };
			struct{ T_TYPE _unused2; Vec<2, T_TYPE> yz;  };
		};
		
		XEN_IMPL_MATH_VEC_BODY(4)

		static const Vec<4, T_TYPE> UnitX, UnitY, UnitZ, UnitW, Origin;
		static const Vec<4, T_TYPE> Unit[4];
	};
	template<typename T_TYPE> const Vec<4, T_TYPE> Vec<4, T_TYPE>::UnitX(1,0,0,0);
	template<typename T_TYPE> const Vec<4, T_TYPE> Vec<4, T_TYPE>::UnitY(0,1,0,0);
	template<typename T_TYPE> const Vec<4, T_TYPE> Vec<4, T_TYPE>::UnitZ(0,0,1,0);
	template<typename T_TYPE> const Vec<4, T_TYPE> Vec<4, T_TYPE>::UnitW(0,0,1,1);
	template<typename T_TYPE> const Vec<4, T_TYPE> Vec<4, T_TYPE>::Origin(0,0,0,0);
	template<typename T_TYPE> const Vec<4, T_TYPE> Vec<4, T_TYPE>::Unit[4] = {{1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {1,0,0,0}};
}

typedef xen::Vec<2, real>   Vec2 ;
typedef xen::Vec<2, u32>    Vec2u;
typedef xen::Vec<2, s32>    Vec2s;
typedef xen::Vec<2, double> Vec2d;
typedef xen::Vec<2, float>  Vec2f;

typedef xen::Vec<3, real>   Vec3 ;
typedef xen::Vec<3, u32>    Vec3u;
typedef xen::Vec<3, s32>    Vec3s;
typedef xen::Vec<3, double> Vec3d;
typedef xen::Vec<3, float>  Vec3f;

typedef xen::Vec<4, real>   Vec4 ;
typedef xen::Vec<4, u32>    Vec4u;
typedef xen::Vec<4, s32>    Vec4s;
typedef xen::Vec<4, double> Vec4d;
typedef xen::Vec<4, float>  Vec4f;

#undef XEN_IMPL_MATH_VEC_OPERATORS

//users should be able to compare if types are visiable, even if can't do most operations
//would lead to hard to find bug if used compiler default == in some places where the main
//function header isn't included
//:TODO: is compiler default okay? just need to do byte-comparison of the whole struct
template<typename T_TYPE_A, typename T_TYPE_B, u32 T_COUNT>
bool operator==(const xen::Vec<T_COUNT, T_TYPE_A>& lhs, const xen::Vec<T_COUNT, T_TYPE_B>& rhs){
	bool result = true;
	for(u32 i = 0; i < T_COUNT; ++i){ result &= (lhs[i] == rhs[i]); }
	return result;
}

template<typename T_TYPE_A, typename T_TYPE_B, u32 T_COUNT>
inline bool operator!=(const xen::Vec<T_COUNT, T_TYPE_A>& lhs, const xen::Vec<T_COUNT, T_TYPE_B>& rhs){
	return !(operator!=(lhs, rhs));
}

#endif
