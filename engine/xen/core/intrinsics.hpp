////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file intrinsics.hpp
/// \author Jamie Terry
/// \date 2016/07/16
/// \brief Contains core types and functions in xeno engine. Note that unlike everything
/// else some of these types are not inside the xen namespace, in order to decrease typeing
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_INTRINSICS_HPP
#define XEN_CORE_INTRINSICS_HPP

#include <limits>
#include <stdint.h>
#include <cstddef>

#define Minimum(A,B) ((A < B) ? (A) : (B))
#define Maximum(A,B) ((A > B) ? (A) : (B))
#define XenArrayLength(array) (sizeof(array) / sizeof((array)[0]))

/// \brief Triggers break in debugger (or crash if no debugger)
/// \todo :TODO: something better
#define XenBreak (*(char*)nullptr) = 'a'; \

#if 0

// :TODO: renable struct primative types?
// original use case solved in another way now, but these could be useful elsewhere
// could help with reading/writing same binary files on platforms of different endianness
// need to see if extracting parts of types is useful elsewhere
// if using these need to check they don't produce less efficent asm, compile should inline
// everything and have no overhead, might have significant performance hit in debug mode though
// also might need different defintions for different compilers if packing order varies and or
// endianess of the platform

#define XEN_IMPL_PRIMATIVES_BYTES  \
	struct{ \
		T_BYTE& operator[](size_t index)             { return (&byte0)[index]; } \
		const T_BYTE& operator[](size_t index) const { return (&byte0)[index]; } \
	} bytes
#define XEN_IMPL_PRIMATIVES_WORDS \
	struct{ \
		T_WORD& operator[](size_t index)             { return (&word0)[index]; } \
		const T_WORD& operator[](size_t index) const { return (&word0)[index]; } \
	} words
#define XEN_IMPL_PRIMATIVES_INTS \
	struct{ \
		T_BYTE& operator[](size_t index)             { return (&byte0)[index]; } \
		const T_BYTE& operator[](size_t index) const { return (&byte0)[index]; } \
	} ints
#define XEN_IMPL_PRIMATIVES_INTEGER_BODY(TypeName) \
	typedef T_FULL TYPE; \
	inline TypeName(){} \
	inline TypeName(T_FULL val) : value(val) {} \
	inline TypeName& operator=(T_FULL val) { this->value = val; return *this; } \
	inline operator T_FULL()               { return this->value; } \


namespace xen{
	template<typename T_DERIVED>
	struct _integer_base{
		static const T_DERIVED MAX;
		static const T_DERIVED MIN;
	};
	template<typename T_DERIVED>
	const T_DERIVED _integer_base<T_DERIVED>::MAX = std::numeric_limits<typename T_DERIVED::TYPE>::max();
	template<typename T_DERIVED>
	const T_DERIVED _integer_base<T_DERIVED>::MIN = std::numeric_limits<typename T_DERIVED::TYPE>::min();

	template<typename T_FULL>
	struct _int8 : public _integer_base<_int8<T_FULL>>{
		T_FULL value;
		XEN_IMPL_PRIMATIVES_INTEGER_BODY(_int8);
	};

	template<typename T_FULL, typename T_BYTE>
	struct _int16 : public _integer_base<_int16<T_FULL, T_BYTE>>{
		union{
			T_FULL value;
			struct{ T_BYTE low_byte, high_byte; };
			struct{ T_BYTE byte0, byte1; };
			XEN_IMPL_PRIMATIVES_BYTES;
		};
		XEN_IMPL_PRIMATIVES_INTEGER_BODY(_int16);
	};

	template<typename T_FULL, typename T_WORD, typename T_BYTE>
	struct _int32 : public _integer_base<_int32<T_FULL, T_WORD, T_BYTE>>{
		union{
			T_FULL value;
			struct{ T_WORD low_word, high_word; };
			struct{ T_WORD word0, word1; };
			struct{ T_BYTE byte0, byte1, byte2, byte3; };
			XEN_IMPL_PRIMATIVES_BYTES; XEN_IMPL_PRIMATIVES_WORDS;
		};
		XEN_IMPL_PRIMATIVES_INTEGER_BODY(_int32);
	};

	template<typename T_FULL, typename T_INT, typename T_WORD, typename T_BYTE>
	struct _int64 : public _integer_base<_int64<T_FULL, T_INT, T_WORD, T_BYTE>>{
		union{
			T_FULL value;
			struct{ T_INT  low_int, high_int; };
			struct{ T_INT  int0, int1; };
			struct{ T_WORD word0, word1, word2, word3; };
			struct{ T_BYTE byte0, byte1, byte2, byte3, byte4, byte5, byte6, byte7; };
			XEN_IMPL_PRIMATIVES_BYTES; XEN_IMPL_PRIMATIVES_WORDS; XEN_IMPL_PRIMATIVES_INTS;
		};
		XEN_IMPL_PRIMATIVES_INTEGER_BODY(_int64);
	};
}

#undef XEN_IMPL_PRIMATIVES_BYTES
#undef XEN_IMPL_PRIMATIVES_WORDS
#undef XEN_IMPL_PRIMATIVES_INTS
#undef XEN_IMPL_PRIMATIVES_INTEGER_BODY

typedef xen::_int8<int8_t>                             int8;
typedef int8                                           int08;
typedef xen::_int16<int16_t, int8_t>                   int16;
typedef xen::_int32<int32_t, int16, int8_t>            int32;
typedef xen::_int64<int64_t, int32, int16, int8_t>     int64;

typedef xen::_int8<uint8_t>                            uint8;
typedef uint8                                          uint08;
typedef xen::_int16<uint16_t, uint8_t>                 uint16;
typedef xen::_int32<uint32_t, uint16, int8_t>          uint32;
typedef xen::_int64<uint64_t, uint32, uint16, uint8_t> uint64;

#endif
typedef  int8_t   int8;
typedef  int8_t   int08;
typedef  int16_t  int16;
typedef  int32_t  int32;
typedef  int64_t  int64;

typedef uint8_t   uint8;
typedef uint8_t   uint08;
typedef uint16_t  uint16;
typedef uint32_t  uint32;
typedef uint64_t  uint64;

typedef int8_t    bool8;
typedef int32_t   bool32;

typedef float     real32;
typedef double    real64;

#ifdef XEN_USE_DOUBLE_PRECISION
typedef real64   real;
#else
typedef real32   real;
#endif

typedef real32   r32;
typedef real64   r64;
typedef int8     s8;
typedef int8     s08;
typedef int16    s16;
typedef int32    s32;
typedef int64    s64;
typedef uint8    u8;
typedef uint8    u08;
typedef uint16   u16;
typedef uint32   u32;
typedef uint64   u64;
typedef bool8    b8;
typedef bool32   b32;

namespace xen{
	struct NonCopyable{
		NonCopyable(){}
	private:
		NonCopyable(const NonCopyable& other)            = delete;
		NonCopyable& operator=(const NonCopyable& other) = delete;
	};

	template<typename T>
	struct Array{
		struct Iterator{
			xen::Array<T>* array;
			size_t index;

			inline Iterator  operator[](size_t offset)         const { return Iterator {array, index + offset}; }
			inline bool      operator==(const Iterator& other) const { return index == other.index && array == other.array; }
			inline bool      operator!=(const Iterator& other) const { return !operator==(other); }
			inline Iterator& operator++()                            { ++index; return *this; }
			inline Iterator& operator--()                            { --index; return *this; }
			inline T*        operator->()                      const { return &(*array)[index]; }
			inline T&        operator*()                       const { return (*array)[index]; }
			inline operator  bool()                            const { return index >= 0 && index < array->length; }
			inline explicit operator T*()                      const { return &(*array)[index]; }
		};

		struct ConstIterator{
			const xen::Array<T>* array;
			size_t index;

		   inline ConstIterator  operator[](size_t offset)    const { return Iterator {array, index + offset}; }
			inline bool      operator==(const Iterator& other) const { return index == other.index && array == other.array; }
			inline bool      operator!=(const Iterator& other) const { return !operator==(other); }
			inline ConstIterator& operator++()                       { ++index; return *this; }
			inline ConstIterator& operator--()                       { --index; return *this; }
			inline const T*  operator->()                      const { return &(*array)[index]; }
			inline const T&  operator*()                       const { return (*array)[index]; }
			inline operator  bool()                            const { return index >= 0 && index < array->length; }
			inline explicit operator const T*()                const { return &(*array)[index]; }
		};

		size_t length;
		T* values;

		const T& operator[](size_t index) const { return values[index]; }
		T&       operator[](size_t index)       { return values[index]; }
	};

	template<typename T_TYPE, size_t T_SIZE>
	Array<T_TYPE> makeArray(T_TYPE (&array)[T_SIZE]) { return xen::Array<T_TYPE>{T_SIZE, array}; }

	template<typename T_TYPE, size_t T_SIZE>
	constexpr const Array<T_TYPE> makeArray(const T_TYPE (&array)[T_SIZE]) { return xen::Array<T_TYPE>{T_SIZE, array}; }

	template<typename T_TYPE>
	Array<T_TYPE> makeArray(T_TYPE* array, size_t length){ return xen::Array<T_TYPE>{length, array}; }

	/// \brief Returns an Array<T>::Iterator to the first element of the specified Array<T>
	/// \public \memberof Array
	template<typename T>
	typename Array<T>::ConstIterator begin(const Array<T>& arr){
		return { &arr, 0 };
	}

	/// \brief Returns an Array<T>::Iterator to the first element of the specified Array<T>
	/// \public \memberof Array
	template<typename T>
	typename Array<T>::Iterator begin(Array<T>& arr){ return { &arr, 0}; }

	/// \brief Copies source string into destination, will never write more than destination_length
	/// characters, resulting string is always null terminated (unless destination_length == 0),
	/// but will be truncated if strlen(source) > destination_length
	/// \return True if entire string was copied, else false. Regardless of return value
	/// destination will point to the first char of a null terminated string if \c destination_length > 0
	inline bool copyString(const char* source, char* destination, size_t destination_length){
		if(destination_length < 1){ return false; }
		size_t cur;
		for(cur = 0; cur < destination_length; ++cur){
			destination[cur] = source[cur];
			if(source[cur] == '\0'){
				return true; //there is room for \0, success
			}
		}
		//ensure null terminated and return false
		destination[cur-1] = '\0';
		return false;
	}

	/// \brief Determines the length of a string, not counting the null terminator
	inline u32 strlen(const char* str){
		u32 result = 0;
		while(*str++){ ++result; };
		return result;
	}

	/// \brief determines if two null terminated strings are equal
	inline bool stringEquals(const char* a, const char* b){
		if(a == b){ return true; }
		const char* cur_a = a;
		const char* cur_b = b;
		while(*cur_a && *cur_b){ if(*cur_a++ != *cur_b++) { return false; } }
		return !(*cur_a) && !(*cur_b);
	}

	/// \brief determines if two null terminates strings are equal up to a certain point
	/// Comapares only the first 'length' characters, or less if either string terminates early with \0
	inline bool stringEquals(const char* a, const char* b, u32 length){
		if(a == b) { return true; }
		const char* cur_a = a;
		const char* cur_b = b;
		while(length-- && *cur_a && *cur_b){ if(*cur_a++ != *cur_b++) {return false; } }
		return true;
	}

	/// \brief Determines if two null terminated strings are equal up to the point that the shorter
	/// of the two terminates
	inline bool stringStartsEqual(const char* a, const char* b){
		if(a == b){ return true; }
		if(a == b){ return true; }
		const char* cur_a = a;
		const char* cur_b = b;
		while(*cur_a && *cur_b){ if(*cur_a++ != *cur_b++) { return false; } }
		return true;
	}

	/// \brief Determines the index of the first element of the array which == target
	/// \return Index of element or -1 if not found
	template<typename T>
	inline s32 indexOfFirst(const T& target, const T* array, u32 num_elements){
		for(s32 i = 0; i < num_elements; ++i){ if(array[i] == target) return i; }
		return -1;
	}

	/// \brief Determines the index of the first element of the array which == target
	/// \return Index of element of -1 if not found
	/// \public \memberof Array
	template<typename T>
	inline s32 indexOfFirst(const T& target, const Array<T>& arr){
		return indexOfFirst(target, arr.values, arr.length);
	}

	/// \brief Determines the index of the first char in the string equal to target
	/// \return Index of character or -1 if '\0' is encountered before target
	/// \note Works correctly when searching for '\0'
	inline s32 indexOfFirst(const char target, const char* string){
		//len + 1 so we also examine the null terminator incase we're searching for it
		// :TODO:OPT: inefficent as searching over string twice, once to find length, once to find first
		return indexOfFirst(target, string, strlen(string) + 1);
	}

	/// \brief Finds first instance of some character in the target string, note
	/// this also works for finding the null terminator
	inline const char* findFirst(char target, const char* string){
		for(const char* cur = string; true; ++cur){
			if(*cur == target){ return cur; } //have to check this before '\0' incase we're looking for \0
			if(*cur == '\0') { return nullptr; }
		}
	}

	inline char* findFirst(char target, char* string){
		for(char* cur = string; true; ++cur){
			if(*cur == target){ return cur; } //have to check this before '\0' incase we're looking for \0
			if(*cur == '\0') { return nullptr; }
		}
	}

	inline char* findLast(char target, char* string){
		char* result = nullptr;
		char* cur    = string;
		for(; *cur; ++cur){
			if(*cur == target){ result = cur; }
		}
		if(result == nullptr){ result = cur; }
		return result;
	}

	inline const char* findNullTerminator(const char* string){
		while(*string){ ++string; }
		return string;
	}

	inline bool endsWith(const char* string, const char* suffix){
		const char* suffix_cur = findNullTerminator(suffix) - 1;
		const char* string_cur = findNullTerminator(string) - 1;
		while(suffix_cur >= suffix && string_cur >= string){
			if(*suffix_cur != *string_cur){ return false; }
			--suffix_cur;
			--string_cur;
		}
		return (suffix - 1) == suffix_cur;
		//:TODO: does this work if one char off?
		//eg "hello world" ends with "?orld"
	}


	/// \brief Determines number of elements in array which == target
	template<typename T>
	inline u32 countXIn(const T& target, const T* array, u32 num_elements){
		u32 result = 0;
		for(u32 i = 0; i < num_elements; ++i){ if(array[i] == target) ++result; }
		return result;
	}

	/// \brief Determines number of elements in array which == target
	/// \public \memberof Array
	template<typename T>
	inline u32 countXIn(const T& target, const Array<T>& arr){
		return countXIn(target, arr.values, arr.length);
	}

	/// \brief Determines number of characters in string which == target
	inline u32 countXIn(const char target, const char* string){
		//len + 1 so we also examing the null terminator incase we're counting that
		//:TODO:OPT: inefficent as searching over string twice, once to find length, once to first count
		return countXIn(target, string, strlen(string) + 1);
	}

	///< Converts the low four bits of \c nibble into a lowercase hex char
	inline u8 nibbleToHexChar(u8 nibble){
		switch(nibble & 0xF){
		case 0x0: return '0';
		case 0x1: return '1';
		case 0x2: return '2';
		case 0x3: return '3';
		case 0x4: return '4';
		case 0x5: return '5';
		case 0x6: return '6';
		case 0x7: return '7';
		case 0x8: return '8';
		case 0x9: return '9';
		case 0xA: return 'a';
		case 0xB: return 'b';
		case 0xC: return 'c';
		case 0xD: return 'd';
		case 0xE: return 'e';
		case 0xF: return 'f';
		default: XenBreak;
		}
	}

	/// \brief Converts a hex character (upper or lower case accepted) into a
	/// nibble
	/// \note Returns u8 with top four bits set to non 0 if char \c c is not a valid
	/// hex character, test with:
	/// \code
	/// if(result & 0xF0){ /*bad hex char*/ } else { /* good hex char*/ }
	/// \encode
	inline u8 hexCharToNibble(char c){
		switch(c){
		case '0':           return 0x0;
		case '1':           return 0x1;
		case '2':           return 0x2;
		case '3':           return 0x3;
		case '4':           return 0x4;
		case '5':           return 0x5;
		case '6':           return 0x6;
		case '7':           return 0x7;
		case '8':           return 0x8;
		case '9':           return 0x9;
		case 'A': case 'a': return 0xA;
		case 'B': case 'b': return 0xB;
		case 'C': case 'c': return 0xC;
		case 'D': case 'd': return 0xD;
		case 'E': case 'e': return 0xE;
		case 'F': case 'f': return 0xF;
		default: return 0xFF;
		}
	}
}

#endif
