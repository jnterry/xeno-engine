////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file meta_types.hpp
/// \author Jamie Terry
/// \date 2016/07/19
/// \brief Contains types and functions for interacting with Xeno Engine's
/// run time reflection system
///
/// Usage
/// -----
/// MetaType's can be obtained for a type using:
/// \code
/// const xen::MetaType* t = xen::type<u32>();
/// \endcode
/// Note also that templates have a meta type, eg:
/// \code
/// //xen::Array is template class, eg, xen::Array<u32>
/// const xen::MetaType* t = xen::type<xen::Array>();
/// \endcode
///
/// Design Overview
/// ---------------
/// Ensuring xen::typeOf<T> works requires quite a bit of template trickery
/// We want to support:
/// - xen::typeOf<u32>              //tparam is a type
/// - xen::typeOf<xen::Array>       //tparam is a template
/// - xen::typeOf<xen::Array<int>>  //tparam is a template instatiation, this is equivlant to a type
///
/// We can only accept a un-instatiated template (eg: \c xen::Array) if we take a template as a template
/// parameter, eg:
/// \code
/// template<template<typename> class T>
/// \endcode
///
/// However this means we can't take actual types, like \c u32 or \c xen::Array<u32>, hence we
/// need to also take types, eg:
/// \code
/// template<typename T>
/// \endcode
///
/// This requirement forces us to use a function, since we cant define a struct with both
/// of these, since technically we're overloading the function rather than providing
/// a template specialization
///
/// However, in order to extract a type's template paramter, eg, \c u32 from \c xen::Array<u32>
/// we need to do partial specialization such as:
/// \code
/// template<typename T>
/// const MetaType* xen::MetaType<xen::Array<T>>(){...}
/// \endcode
///
/// However functions cannot be partially specialized, only structs can. Hence this forces
/// us to use a struct.
///
/// The conflicting requirements (need function overloading, and template PARTIAL specialization)
/// force us to use both a function and a struct, with the function defering the call to a member
/// of the struct.
///
/// This helper struct is \c xen::impl::TypeOf
/// It has the member "MetaType type", which may be speciailized for each type in order to associated
/// a MetaType instance with a C++ type
///
/// When a type (eg: \c u32) or template instantiation (eg: \c xen::Array<u32>) is passed to
/// xen::typeOf<> the type is directly passed to the struct, ie, we return
/// \code xen::impl::TypeOf<T>::type \endcode
///
/// When an un-instantiated template (eg: \c xen::Array) is passed to xen::typeOf<> the type
/// must first be instantiated, so it can be passed to TypeOf. This is done by introducting
/// a dummy type: xen::_DummyInstantiator
///
/// A full specialization must then be provided for xen::TypeOf<T> with:
/// \code T = xen::Array<_DummyInstantiator> \endcode
///
/// A partial specialization handles the instantiated template case.
///
/// \todo :TODO: run time getting of types? xen::type("xen::Array");
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_META_TYPE_HPP
#define XEN_CORE_META_TYPE_HPP

#include <typeinfo>
#include <type_traits> //:TODO: for std::is_same<U, V> make own? (template helper header?)
#include <cstddef> //:TODO: for offsetof, cant implement it by self in constexpr way
#include <limits> //:TODO:COMP: needed for std::numeric_limits, use intrinsics.hpp struct primatves with ::MAX? 

#include "intrinsics.hpp"
#include "memory.hpp"

//:TODO: type alisis, eg, u8, unsigned char, u08, uint8_t all go to same meta type

namespace xen{
	struct Allocator;
	struct MetaType;

	/// \struct SystemMetaType
	/// \brief Opqaue type representing all the memory that the meta type
	/// system requires
	/// \todo :TODO: cant be opaque since templates at bottom of this file
	/// for primatives need to see the MetaType*
   struct SystemMetaType{
		Allocator* allocator;
		ArenaLinear* meta_type_store;
	   ArenaLinear* alias_store;
		u32 meta_type_next_id;

	   struct Alias{
		   const char*     name; ///< the type name, eg u32
		   const MetaType* type; ///< the type

		   //:TODO:COMP: linked list helper functions
		   Alias* next; ///< next in the linked list
	   };

	   ///< array of alias link lists
	   /// names must start with a-z, A-Z, _
	   /// to speed up searching we have a linked list per starting character, this array is continous
	   /// from A (ascii 65) -> z (ascii 122), do first char of name - 'A' to get index in this
	   Alias* aliases['z' - 'A'];
	};

	namespace _sys{
		/// \brief The global SystemMetaType which will be used by default by all releavant functions
		extern SystemMetaType* meta_type;
	};

	/// \brief Creates a new Meta Type system
	/// \param alloc The Allocator with which to create the arenas the system will use
	/// \param bytes The number of bytes the system is allowed to use
	/// \public \memberof SystemMetaType
	SystemMetaType* createSystemMetaType(Allocator& alloc, size_t bytes);

	/// \brief Creates a new MetaType system and installs it as the global default
	/// meta system
	/// \see createSystemMetaType
	/// \public \memberof SystemMetaType
	inline bool initializeSystemMetaType(Allocator& alloc, size_t bytes){
		XenAssert(_sys::meta_type == nullptr, "Attempted to initialize the global SystemMetaType multiple times");
		_sys::meta_type = createSystemMetaType(alloc, bytes);
		return _sys::meta_type != nullptr;
	}

	/// \brief Destroys the specified SystemMetaType, releasing all resources it has
	/// aquired
	/// \public \memberof SystemMetaType
	void destroySystemMetaType(SystemMetaType* system);

	/// \brief Destroys the global default SystemMetaType
	inline void shutdownSystemMetaType(){
		XenAssert(_sys::meta_type != nullptr, "Attempted to shutdown the global SystemMetaType before it was initialized");
		destroySystemMetaType(_sys::meta_type);
		_sys::meta_type = nullptr;
	}

	/// \brief Represents a single field of some MetaType
	struct MetaTypeField{
		const char* name; ///< name of field, nullptr for anoynomous struct/union

		/// \brief The type of the field
		///
		/// If this field part of a MetaType representing a template then this will be set to one
		/// of the elements of the array xen::MetaTParam
		const MetaType* type;

		union{
			size_t static_array_length;          ///< number of elements in a KIND_STATIC_ARRAY
			const MetaTypeField* dynamic_array_length; ///< integer field storing length of this KIND_DYNAMIC_ARRAY
			const MetaTypeField* union_type;          ///< integer field storing index of currently active union member
		};
		
		u32 offset; ///< Number of bytes between the start of the type and this field

		/// \brief Value used for the member \c offset to indicate that the offset is unknown
		/// since it depends on a template parameter, and the MetaType containing this field
		/// is still a template, ie, xen::Array is a template, xen::Array<int> is an actual type
		constexpr const static u32 OFFSET_UNKNOWN = std::numeric_limits<u32>::max();


		/// \brief Enumeration of various qualifiers a field may be declared with
		enum Qualifiers{
			/// \brief mask of the bits used to store the pointer level
			/// eg: int has level 0, int** has level 2
			/// \note Guarrentied to be the low n-bits, hence can just biwise-and
			/// this->qualifiers with the mask and use result as an int with no further manipulation
			POINTER_LEVEL_MASK = 0x0F,
			
			KIND_VALUE         = 0x00, ///< Single value, eg: `int i;`
			KIND_UNION         = 0x10, ///< Union where another field in the type stores which union member should be used
			KIND_STATIC_ARRAY  = 0x20, ///< Compile time fixed size array, eg: `int arr[4];`
			KIND_DYNAMIC_ARRAY = 0x30, ///< Flexible array, eg: `int* arr; int arr_length;`
			KIND_MASK          = 0x30, ///< Mask of the bits used to store the kind
			KIND_IS_ARRAY      = 0x20, ///< Mask of a bit which is set if the kind is KIND_STATIC_ARRAY or KIND_DYNAMIC_ARRAY

			CONST              = 0x40, ///< Whether the field is declared as const
			REFERENCE          = 0x80, ///< Whether the field is a reference
		};
		
		u8 qualifiers; ///< bitfield storing various misc properties, see Qualifiers enum

		/*MetaTypeField(){}
		MetaTypeField(const char* name_, const MetaType* type_, u16 offset_, u8 qualifiers_ = KIND_VALUE)
		:name(name_), type(type_), offset(offset_), qualifiers(qualifiers_){}*/

		/// \brief Named constructor for creating MetaTypeField with KIND_STATIC_ARRAY
		/// \param length                The number of elements in the static array
		/// \param additional_qualifiers An extra qualifiers other than KIND_STATIC_ARRAY, if any other
		///                              KIND_*** qualifier is set it will be ignored
		/*static MetaTypeField staticArray(const char* name_, const MetaType* type_, u16 offset_,
		                                 size_t length, u8 additional_qualifiers = 0);

		/// \brief Nammed constructor for creating MetaTypeField with KIND_DYNAMIC_ARRAY
		///
		/// Should be used for types like:
		/// \code
		/// struct{
		///     int* array;         // this is the field we're representing
		///     u32  array_length;  // the MetaTypeField for this should be passed as length_field
		/// };
		/// \endcode
		///
		/// \param length_field The field which stores the run time length of the dynamic array
		/// May be nullptr, otherwise xen::isInteger(length_field->type) MUST be true.
		/// \param additonal_qualifiers Any extra qualifiers other than KIND_DYNAMIC_ARRAY, if any other
		/// KIND_*** qualifier is set it will be ignored 1 will automattically be added to the pointer count
		/// since dynamic arrays are represented by a pointer to the first element
		static MetaTypeField dynamicArray(const char* name_, const MetaType* type_, u16 offset_,
		                                  const MetaTypeField* length_field, u8 additional_qualifiers = 0,
		                                  SystemMetaType* system = _sys::meta_type);*/
	};

	///< Checks if a MetaType is for one of the primative integer types (u8..u64, s8..s64)
	bool isInteger(const MetaType* type, SystemMetaType* system = _sys::meta_type);

	/// \brief Determines if a field is dependent on a template parameter of its type (note: returns
	/// false if the type has been instantiated and hence the template fields have had their
	/// actual types filled in
	bool isTemplate(const MetaTypeField* field);

	/// \brief Stores data about the byte layout of the fields within some type
	struct MetaType{
		u32 id; ///< Top bit indicates if MetaTypeTemplate or MetaType

		const char* name;
		
		///< array of fields this type has
		const Array<MetaTypeField> fields;

		///< The size in bytes this type takes, static size only, ie, what sizeof(T)
		///< would return for the corrosponding type. Dynamically allocated data owned
		///< by the type is not included
		u32 static_size;

		/// \brief Value used for the \c static_size member when the size is not
		/// known as it depends on a template parameter
		constexpr static const decltype(static_size) STATIC_SIZE_UNKNOWN = std::numeric_limits<decltype(static_size)>::max();
	};

	/// \brief Returns a pointer to the value of some field in a type
	///
	/// This chases as many pointers are nessacery and does any nessacary offset
	/// within the type
	///
	/// \param address Pointer to the memory of the type
	/// \param MetaTypeField the field to access
	/// \return void* to the field's value, or nullptr if can't access the field
	void* accessField(void* address, const MetaTypeField* field){
		if(field->offset == MetaTypeField::OFFSET_UNKNOWN) { return nullptr; }
		void* result = ptrAdvance(address, field->offset);

		if(field->qualifiers & MetaTypeField::KIND_IS_ARRAY){ result = *((void**)result); }
		
		for(u8 i = (u8)(field->qualifiers & MetaTypeField::POINTER_LEVEL_MASK); i > 0; --i){
			result = *((void**)result);
		}
		
		return result;
	}

	/// \brief Placeholder instances of MetaType used MetaTypeField::type in order to
	/// indicate the type of the field is dependent on a template parameter
	extern MetaType MetaTParam[8];

	namespace impl{
		/// \brief Implementation detail dummy type used to instantiate templates
		/// in order to distinguish a typeOf template vs a typeOf instantiated
		/// template, eg, typeOf<xen::Array> returns a templated MetaType
		/// but typeOf<xen::Array<int>> returns an actual MetaType
		struct _DummyInstantiator{};

		template<typename T>
		struct TypeOf{
			const static MetaType type;
		};
		template<typename T>
		const MetaType TypeOf<T>::type = {0, typeid(T).name(), {0}, sizeof(T)};
	}


	template<typename T> const MetaType* typeOf(){
		return &impl::TypeOf<T>::type;
	}
	
	template<template<typename> class T> const MetaType* typeOf(){
		return &impl::TypeOf<T<impl::_DummyInstantiator>>::type;
	}

	/// \brief Allows for run time retrieval of types by name, return nullptr if no alias registered with specified name
	const MetaType* getType(const char* name, const SystemMetaType* system = _sys::meta_type);

	/// Creates a new xen::MetaType, registers it with the meta type system and returns
	/// a pointer to the new xen::MetaType instance
	///
	/// \note Copies name and fields into internal buffer, caller remains owner of the parameters
	const MetaType* _emplaceMetaType(const char* name, const xen::Array<MetaTypeField>& fields,
	                                 u32 static_size, SystemMetaType* system = _sys::meta_type);

	/// \brief Registers an alias that can later be used to retrieve a MetaType, copies
	/// name into internal buffer, caller remains owner of it. MetaType is assumed to already
	/// be in the SystemMetaType, if not caller is responsible for keeping it alive until the death
	/// of the SystemMetaType
	/// \note First character of name must begin with A-Z, a-z, _
	void registerTypeAlias(const char* name, const MetaType* type, SystemMetaType* system = _sys::meta_type);
}

#include "meta_type_primatives.hpp"

#endif


