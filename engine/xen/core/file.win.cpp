////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file file.win.cpp
/// \author Jamie Terry
/// \date 2016/07/23
/// \brief Contains windows specific implementations of types and methods defined
/// in file.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#include "../windows_header.hpp"
#include <utility> //:TODO: for std::move, what is it actually doing?

#include "file.hpp"
#include "intrinsics.hpp"
#include "debug.hpp"

static_assert(sizeof(HANDLE) <= sizeof(xen::FileIterator::_data1), "Platform type needs to fit in xen::FileIterator::_data1");

//this code uses FileIterator::_data1 to store the HANDLE that is being used with FindFirstFile, FindNextFile, etc

namespace xen{
	namespace impl{
		void _findDataToFileIterator(WIN32_FIND_DATA* find_data, FileIterator* it){
			it->attributes = FileIterator::VALID;
			
			u32 i = 0;
			//:TODO: < 63 should use length of array it->name
			while(i < 63 && find_data->cFileName[i] != '\0'){
				it->name[i] = find_data->cFileName[i];
				++i;
			}
			it->name[i] = '\0';

			if(find_data->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
				it->attributes |= FileIterator::TYPE_DIRECTORY;
			} else if(find_data->dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT){
				it->attributes |= FileIterator::TYPE_LINK;
			} else {
				it->attributes |= FileIterator::TYPE_FILE;
			}
		}
	} //end of namespace impl

	bool getCwd(Path& path, bool allow_fail){
		//:TODO: do we need unicode support? (GetCurrentDirectoryW)
		DWORD status = GetCurrentDirectoryA(128, path.buffer.values);
		if(status == 0){
			xen::log::write(XenFatalMaybe("Failed to get current working directory due to windows error: %i",
			                              GetLastError(), "xen.windows", "xen.file"));
			return false;
		}

		if(status > path.buffer.length){
			xen::log::write(XenFatalMaybe("Failed to get current working directory as buffer too short, "
			                              "buffer length: %i, required length: %i", path.buffer.length, status + 1,
			                              "xen.windows", "xen.file"));
			return false;
		}

		path.path_length = status;
		for(u32 i = 0; i < path.path_length; ++i){
			if(path.buffer[i] == '\\') { path.buffer[i] = '/'; }
		}

		return true;
	}
	
	FileIterator iterateDirectory(const char* directory){
		// windows requires a * on the end, eg, C:/dir/*, copiny into tmp buffer, bit nasty
		// but we dont want to modify the parameter, and probably iteration is slower than strcpy
		char search_string[256];
		XenAssert(xen::copyString(directory, search_string, XenArrayLength(search_string)), "search_string buffer too short");

		char* str_end = findFirst('\0', search_string);
		if(str_end > search_string){
			if(str_end[-1] != '/'){
				//append '/' if not present
			   str_end[0] = '/';
				++str_end;
			}
		   str_end[0] = '*'; //append '*', ensure still null terminated
		   str_end[1] = '\0';

		}

		FileIterator it;
		it.attributes = FileIterator::VALID;
		WIN32_FIND_DATA find_data;
		HANDLE find_result = FindFirstFile(search_string, &find_data);

		if(find_result == INVALID_HANDLE_VALUE){
			xen::log::write(XenWarn("Attemped to iterate over files in directory %s, but windows error occured: %i", directory, GetLastError()));
			it.attributes = 0;
			it._data1 = 0;
		} else {
			it._data1 = (u64)find_result;
			if(impl::isThisDirOrUpDirName(find_data.cFileName)){
				it.operator++();
			} else {
				impl::_findDataToFileIterator(&find_data, &it);
			}
		}
		
		return std::move(it);
	}

	FileIterator& FileIterator::operator++(){
		WIN32_FIND_DATA find_data;
		BOOL find_result = FindNextFile((HANDLE)this->_data1, &find_data);
		if(find_result){
			//skip the entries for . and ..
			if(impl::isThisDirOrUpDirName(find_data.cFileName)){
				return this->operator++();
			}
			
			impl::_findDataToFileIterator(&find_data, this);
		} else {
			this->attributes = 0;
		}
		return *this;
	}
	
	FileIterator::~FileIterator(){
		HANDLE handle = (HANDLE)this->_data1;
		if(handle && handle != INVALID_HANDLE_VALUE){
			if(!FindClose(handle)){
				xen::log::write(XenWarn("Failed to close Win32 FindFirstFile handle upon "
				                        "destruction of FileIterator, Win32 Error: %i",
				                        GetLastError(), "xen.windows"));
			}
		}
	}

	bool createDirectory(char* path, bool create_ancestors){
		//:TODO: this does a double to CreateDirectory call for each folder
		//that doesn't exist, as it only recurses up to parent directory when it fails,
		//might be better to say if first one fails immediately go up to the top level
		//and start creating from there -> which is actually will depend on usage
		// (ie: few missing directory levels vs many) -> ideally would do binary search
		//to find first directory level that doesnt exist
		BOOL result = CreateDirectoryA(path, NULL);
		if(result){ return true; }

		auto err = GetLastError();
		if(err == ERROR_PATH_NOT_FOUND){
			if(!create_ancestors){ return false; }
			
			char* slash = findLast('/', path);
			if(slash == nullptr){ return false; }
			*slash = '\0';
			bool successful = createDirectory(path, create_ancestors);
			*slash = '/';
			if(slash[1] == '\0'){
				//then path param had a trailing slash, dont try to create test/a/b and test/a/b/
				return successful;
			} else {
				if(!successful){ return false; }
				result = CreateDirectoryA(path, NULL); //try again now ancestors have been created
				return result == TRUE;
			}
		} else if (err == ERROR_ALREADY_EXISTS){
			return true;
		} else {
			return false;
		}	
	}
}

