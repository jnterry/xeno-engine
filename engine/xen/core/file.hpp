////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file file.hpp
/// \author Jamie Terry
/// \date 2016/07/23
/// \brief Contains functions for interacting with the file system, eg listing
/// files in directory, etc
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_FILE_HPP
#define XEN_CORE_FILE_HPP

#include "intrinsics.hpp"

// setup defaults for max path length and number of components,
// can manually #define XEN_PATH_MAX_XXX when including Xeno Engine
// to change the defaults
#ifndef XEN_PATH_MAX_LENGTH
    #define XEN_PATH_MAX_LENGTH 256;
#endif

namespace xen{
	struct ArenaLinear;

	/// \breif Type representing a file system Path.
	///
	/// Paths are stored in the unix style, ie, with / seperating components
	/// This is done as unix requires / be used, but windows allows \ or / to be used
	///
	/// Path is really just a char array, but there exist extra functions to push and pop
	/// components from the path
	///
	/// This allows indivdual components to easily be treated as c strings (since they
	/// are)
	struct Path{
		/// \breif Array of characters containg the whole path's data
		xen::Array<char> buffer;

		/// \breif Length of the path including the null terminator
		u32 path_length;

		operator const char*() const{ return buffer.values; }
		operator       char*()      { return buffer.values; }
	};

	/*template<typename T_SIZE>
	Path makePath(char (&array)[T_SIZE]){
		Path result = {0};
		result.buffer.count  = T_SIZE;
		result.buffer.values = array;
		return result;
		}*/

	/// \brief Creates a new Path object setup to use the specified buffer
	/// \public \memberof Path
	Path makePath(char* buffer, size_t buffer_length, const char* value = "", bool allow_fail = false);

	/// \brief Pushes a string onto the end of a path, if the path does not have
	/// a trailing slash adds it before pushing
	/// \param allow_fail if true and push fails as the path's buffer does not
	/// have room then false will be returned, else fatal log message will be written
	/// \note If failed to push and allow_fail is true then the path will not have been modifed (although
	/// buffer.values may have been after the strings null terminator)
	/// \public \memberof Path
	bool push(Path& base, const char* relative, bool allow_fail = false);

	/// \brief Removes the last component of a path, does not change whether path has
	/// trailing slash, but always removes a component, ie: /test/thing/ -> /test/
	/// /test/thing -> /test
	/// \return True if Path had at least one component and hence pop succeeded, else false
	/// \public \memberof Path
	bool pop(Path& p);

	/// \brief Clears a path so that it has no components
	/// \public \memberof Path
	void clearPath(Path& path);

	/// \brief Fills in a Path object with the current working directory of the running
	/// executable
	/// \return True if the parameter \c path had a large enough buffer and the operation
	/// was successful
	/// \public \memberof Path
	bool getCwd(Path& path, bool allow_fail = false);

	/// \brief Loads a file into memory and null terminates
	char* loadFile(const char* path, ArenaLinear& arena, bool allow_fail = false);

	/// \brief Creates a directory with the specified path
	/// \param path The path of the directory to create
	/// \param create_ancestors If set then any parents of the directory will also be created if needed
	/// \return True if the directory was created, or already existed, else false
	/// \note The contents of the string \c path may be changed during this call, but will always
	/// be restored to its original value by the time this call returns
	bool createDirectory(char* path, bool create_ancestors = true);
	bool createDirectory(const char* path, bool create_ancestors = true){
		char tmp[384];
		xen::copyString(path, tmp, XenArrayLength(tmp));
		return createDirectory(path, create_ancestors);
	}
	
	/// \brief Type used to iterate over the files and subdirectories of a directory
	///
	/// Example Usage
	/// -------------
	/// \code
	/// for(auto it = xen::iterateDirectory("path/to/iterate"); it; ++it){
	///     if(it.attributes & xen::FileIterator::TYPE_FILE){
	///         //process
	//      } //etc...
	/// }
	/// \endcode
	struct FileIterator : public NonCopyable{
		enum Attributes{
			TYPE_UNKNOWN   = 0x00,
			TYPE_FILE      = 0x01,
			TYPE_DIRECTORY = 0x02,
			TYPE_LINK      = 0x03,
			TYPE_MASK      = 0x07,
			
			//			READABLE       = 0x08,
			//	WRITEABLE      = 0x10,
			//HIDDEN         = 0x20,
			VALID          = 0x80, /// \brief Whether this file iterator is valid
		};

		/// \brief Bitset containg the file's attirubtes
		u8 attributes;

		/// \breif The name of this file/directory relative to the path being iterated
		char name[64];

		//DateTime created_at;
		//DateTime modified_at;
		//DateTime accessed_at;

		/// \brief Reserved data used by the platform specific functions to keep
		/// track of what and where is being iterated
		u64 _data1;

		/// \brief Reserved data used by the platform specific functions to keep
		/// track of what and where is being iterated
		u64 _data2;

		/// \brief Determines if this FileIterator is valid
		inline operator b32(){ return this->attributes & VALID; }

		FileIterator(){}

		/// \breif Advances this FileIterator to the next file in the directory being iterated
		FileIterator& operator++();

		FileIterator(FileIterator&& other);
		FileIterator& operator=(FileIterator&& other);
		~FileIterator();
	};

	/// \breif Returns a FileIterator that is setup to iterate over the specified
	/// directory.
	/// \param directory The path of the directory to iterate over
	///
	/// \note The paremeter directory is expected to be valid for the lifetime
	/// of the returned FileIterator. The caller retains ownership of it's memory
	///
	/// \note The entries for . and .. (this-dir and up-a-dir) are not included
	FileIterator iterateDirectory(const char* directory);
}

#endif

