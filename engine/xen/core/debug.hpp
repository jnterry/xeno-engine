////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file log.hpp
/// \author Jamie Terry
/// \date 2016/07/18
/// \brief Contains functions and types for interacting with Xeno Engine's logging
/// system as well as other debugging features
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_DEBUG_HPP
#define XEN_CORE_DEBUG_HPP

#include "intrinsics.hpp"
#include "time.hpp"

//:TODO: stack trace generation
//:TODO: display message prompt message using OS

// :TODO: better #define-s to disable these -> define min level int?
#ifdef XEN_LOG_NO_TRACE
	#define XenTrace(...)
#else
#define XenTrace(...)     ::xen::_sys::log, __FILE__, __LINE__, xen::log::TRACE,     __VA_ARGS__, nullptr
#endif
#define XenDebug(...)     ::xen::_sys::log, __FILE__, __LINE__, xen::log::DEBUG,     __VA_ARGS__, nullptr
#define XenInfo(...)      ::xen::_sys::log, __FILE__, __LINE__, xen::log::INFO,      __VA_ARGS__, nullptr
#define XenWarn(...)      ::xen::_sys::log, __FILE__, __LINE__, xen::log::WARN,      __VA_ARGS__, nullptr
#define XenError(...)     ::xen::_sys::log, __FILE__, __LINE__, xen::log::ERROR,     __VA_ARGS__, nullptr
#define XenFatalDev(...)  ::xen::_sys::log, __FILE__, __LINE__, xen::log::FATAL_DEV, __VA_ARGS__, nullptr
#define XenFatal(...)     ::xen::_sys::log, __FILE__, __LINE__, xen::log::FATAL,     __VA_ARGS__, nullptr

///< \breif The XenFatalMaybe supports the common idiom in Xeno Engine of having an allow_fail parameter
/// which if true means errors arn't fatal, this macro will log ERROR if allow_fail is true, otherwise
/// it will log fatal
#define XenFatalMaybe(...) ::xen::_sys::log, __FILE__, __LINE__, (allow_fail ? xen::log::ERROR : xen::log::FATAL), __VA_ARGS__, nullptr

/// \brief Logging macro that allows you to specify the level to log, particularly helpful for messages
/// which should be logged at different levels depeding on some condition, you can use
/// ternay operator to select the level
#define XenMsg(level, ...) ::xen::_sys::log, __FILE__, __LINE__, level, __VA_ARGS__, nullptr


#define XenAssert(expr, msg) \
	if(!(expr)) { \
		::xen::log::write(XenError("Runtime Assertion Triggered: %s", msg)); \
		XenBreak; \
	}
#define InvalidCodePath XenAssert(0, "InvalidCodePath") //:TODO: remove this
#define XenInvalidCodePath XenAssert(0, "InvalidCodePath")

namespace xen{
	struct Allocator;
	struct SystemLog;
	struct ArenaLinear;

	namespace _sys{
		/// \brief The global default logging system
		/// \see xen::initializeSystemLog
		extern SystemLog* log;
	}
	
	namespace log{
		typedef u8 Level;
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Enumeration of various predefined log levels, note that the log level
		/// is just a unsigned 8 bit integer and hence the user of Xeno Engine may define
		/// their own log levels if desired, however all messages logged by xeno engine
		/// will use one of these levels.
		//////////////////////////////////////////////////////////////////////////
		enum LevelValues : Level{
			///< Very detailed infomation that could be helpful in fixing some problem
			///< but is in such a high volumnes (multiple times per frame) that it is
			///< likely to cause a noticable slow down and potentially drown out
			///< other log messages
			/// \note Must be left at 0, things depend on this
			TRACE = 0,

			///< An event of some significance occured, but which isnt important enough
			///< to be recorded as info. Such an event occurs 0-1 times per frame
			DEBUG = 50,

			///< Some fairly significant but expected event occured, for example, the
			///< rendering api was initilized.
			///< Generally info is reserved for things which occur significantly less
			///< frequently than once per frame, for example once at startup, or occasionally
			///< in the normal operation of the program (eg, maybe some buffer is reallocated
			///< a few times per minute)
			INFO = 100,

			///< Something odd happened, maybe its by intention but its probably
			///< not what you want (eg, mesh with 0 verticies isnt an error, but its
			///< unlikely you actually want it)
			WARN = 150,

			///< An error occured which can be dealt with by the program, execution
			///< can continue but results may be unexpected
			ERROR = 200,

			///< A severe error occured, while under development this should immediately
			///< halt the program, alerting the developer that something needs to be fixed.
			///< While in production execution will continue
			FATAL_DEV = 250,

			///< A fatal error occured and the application cannot continue, the program will
			///< be terminated (regardless of whether the application is running in development
			///< or production mode)
			///< For example, the main asset file cant be found, so the game can't launch
			FATAL = 255,
		};

		struct Tag{
			const char* full_name;
			const char* name;

			// :TODO:COMP: common tree variables -> base class with CRTP?
			// if all tree type things have same fields can add macros like
			// foreach_child, foreach_ancestor, foreach_sibling
			// functions for add-ing/removing child, etc
			// (can we generalise even more for linked lists?)
			Tag* parent;
			Tag* _next_sibling;
			Tag* _first_child;

			Tag(){}
			Tag(const char* names) : full_name(names), name(names), parent(nullptr), _next_sibling(nullptr), _first_child(nullptr) {}
			
			bool operator==(const Tag& other) const { return this == &other; }
			bool operator!=(const Tag& other) const { return this != &other; }
		};

		/// \brief Retrieves the root tag for a log system
		Tag* getRootTag(SystemLog* system = _sys::log);

		/// Represents a message to be written to a log
		struct Message{
			/// \brief String containing file name where the log message was generated
			const char* file; 

			/// \brief The line number of the file where the log message was generated
			u32 line_number; 

			/// \brief The actual message to report
			const char* message;

			xen::DateTime time;

			/// \brief array of leaf tags, eg, if message has the tag "xen.render.gl" then
			/// this will contain only "xen.render.gl", the message implicitly also has the
			/// tags "xen" and "xen.render" but these are not stored in the array
			xen::Array<Tag*> _leaf_tags;
		};
		
		/// Gets a decendent of a tag, eg, get_decendent(TAG{xen.render}, "gl.mesh") returns xen.render.gl.mesh
		/// Creates a new tag if it does not exist, two tags with same name always have the same address
		/// \param system The system to create the tag in, if it doesn't exist
		/// \todo :TODO: system doesn't default to anything atm, since if tag is from system A and system is system B
		/// then tag will be created within space of B, but added to hierachy of A, could store system* in each tag
		/// but would rather not -> I'm guessing this wont be called very much by users of the system, if your
		/// reading this then maybe you are, in which case the interface may need to be changed
		const Tag* getDecendent(Tag* ancestor, const char* child_path, SystemLog* system);

		/// Gets a tag by its full name, equivalent to getDecendent with ancestor set to root tag
		const Tag* getTag(const char* name, SystemLog* system = _sys::log) { return getDecendent(getRootTag(system), name, system); }
 
		/// \brief Determines if a Message has the specified Tag
		/// \public \memberof Message
		bool hasTag(const Message& msg, const Tag* tag);
		
		/// \overload
		/// \public \memberof Message
		bool hasTag(const Message& msg, const char* tag_name) { return hasTag(msg, getTag(tag_name)); }

		/// \brief Rounds level down to the nearest defined LogLevel enumeration and returns string
		/// containing its name
		/// \param fixed_width If set all returns will have the same number of characters, shorter
		/// names will be padded with leading spaces
		const char* getLevelText(Level level, bool fixed_width = true);

		/// \brief Writes a log message
		/// \param file       The name of the file where the log message is being generated
		/// \param line_num   The line number of the file where the log message is being generated
		/// \param level      The level of the log message to generate
		/// \param msg_format Format string used to generate the log message, syntax like printf
		/// \param ... Arguments formatted by msg_format, if more arguments are provided than
		/// are needed by msg_format subsquent arguments are considered to be format strings
		/// for tags to attach to the message
		/// eg: write(..., "Error rendering group %i", group.id, "xen.render.group", "xen.render.%", renderer.api_name)
		/// - msg_format -> format string for message, contains single % so next param used as arg
		/// - group.id   -> used as arg to format string
		/// - "xen.render.group" -> extra param, interpreted as tag format string, contains 0 %s so takes 0 of subsquent parameters
		/// - "xen.render.%"     -> extra param, since "xen.render.group" takes none, used as format string for another tag
		/// - renderer.api_name  -> used as parameter for previous renderer
		void write(SystemLog* system, const char* file, u32 line_num, Level level, const char* msg_format, ...);

		/// \brief Overload of standard log write which is defined to be an inline no-op, as long as macros
		/// are used to generate the parameters (which they should be, for line numbers + file names)
		/// then all log messages of cerain levels can be turned into no-ops at compile time
		inline void write(){}
	} //end of namespace log

	/// \brief Creates a new logging system, allocates space using the specfied
	/// allocator
	/// \param num_bytes The number of bytes the system is allowed to use in total
	SystemLog* createSystemLog(Allocator& alloc, size_t num_bytes);

	/// \breif Creates a new logging system and installs it as the global default logger
	/// \see createSystemLog
	/// \see xen::_sys::log
	inline bool initializeSystemLog(Allocator& alloc, size_t num_bytes){
		XenAssert(_sys::log == nullptr, "Attempted to initialize global logging system multiple times");
		_sys::log = createSystemLog(alloc, num_bytes);
		return _sys::log != nullptr;
	}

	/// \brief Destroys a SystemLog instance, cleaning up all memory it allocated
	///
	/// \todo :TODO: when we register output devices should this do cleanup (relase file handles etc)
	/// or leave that to person who registered them? (would be easy enough, usually registed in main,
	/// can just clean up at end of main) -> otherwise outputs cannot just be a function pointer, would
	/// need virtuals or a struct of functions with one being cleanup
	void destroySystemLog(SystemLog* system);

	/// \breif Destroys the default logging system
	inline void shutdownSystemLog(){
		XenAssert(_sys::log != nullptr, "Attempted to shutdown global SystemLog before it was initialized");
		destroySystemLog(_sys::log);
		_sys::log = nullptr;
	}
}

#endif

