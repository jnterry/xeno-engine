////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file file.unix.cpp
/// \author Jamie Terry
/// \date 2016/07/23
/// \brief Contains unix specific implementation of types and functions in file.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <cstring>
#include <ctime>

static_assert(sizeof(DIR*) <= sizeof(xen::FileIterator::_data1), "Platform type needs to fit in xen::FileIterator::_data");
static_assert(sizeof(const char*) <= sizeof(xen::FileIterator::_data2), "Platform type needs to fit in xen::FileIterator::_data");

//this code uses FileIterator::_data1 to store the DIR* being iterated over and _data2 to point to the path being iterated

namespace xen{
	namespace impl{
		const char file_bad_platform_str[] = "file.hpp Not implemented on this platform";
		void invalidateFileIterator(FileIterator& it){
			it.attributes = 0;
			it.name[0]    = '\0';
			it._data1     = 0;
			it._data2     = 0;
		}

		void closeFileIterator(FileIterator& it){
			DIR* dir = (DIR*)it._data1;
			if(dir){
				closedir(dir);
			}
			invalidateFileIterator(it);
		}
	}

	bool getCwd(Path& path, bool allow_fail){
		path.buffer[0] = '.';
		path.buffer[1] = '/';
		path.buffer[2] = '\0';
		path.path_length = 2;
		return true;
	}

	FileIterator iterateDirectory(const char* directory){
		DIR* dir;
		FileIterator it;

		if((dir = opendir(directory)) == NULL){
			xen::log::write(XenWarn("Failed to open directory '%s' for file iteration",
			                        directory, "xen.file", "xen.unix"));
			impl::invalidateFileIterator(it);
			return it;
		}

		it.attributes = FileIterator::VALID;
		it._data1 = (u64)dir;
		it._data2 = (u64)directory;
		++it; //advance to the first file/dir in the directory

		return it;
	}

	FileIterator& FileIterator::operator++(){
		struct dirent* ent;
		const char* directory = (const char*)this->_data2;

		//get the next file's dirent
		if((ent = readdir((DIR*)this->_data1)) == NULL){
			impl::closeFileIterator(*this);
			return *this;
		}

		if(impl::isThisDirOrUpDirName(ent->d_name)){ //skip entires . and ..
			return this->operator++();
		}

		//copy file/dir's name
		u32 ent_name_length = 0;
		while(ent_name_length < XenArrayLength(this->name) && ent->d_name[ent_name_length] != '\0'){
			this->name[ent_name_length] = ent->d_name[ent_name_length];
			++ent_name_length;
		}
		this->name[ent_name_length] = '\0';

		//fill in the other details
		//:TODO: this is nasty, to get directory stats need to copy the directory name AND
		//ent->d_name into single buffer, then get stat() on the full path. It also requires
		//the user doesnt change the directory char* passed to iterateDirectory, which they may
		//well do (eg, recurisvly iterating, they push stuff to the path)
		char buffer[256]; //:TODO: max path length const that is public?
		u32 dir_name_length = strlen(directory);
		if(dir_name_length + ent_name_length > 256){
			xen::log::write(XenError("Failed to iterate over directory '%s' "
			                        "since a file name exceeded the path length limit,"
			                        "full path: '%s%s'", directory, directory, this->name,
			                        "xen.file", "xen.unix"));
			impl::closeFileIterator(*this);
			return *this;
		}
		strcpy(buffer, directory);
		char* file_name_start = (char*)ptrAdvance(buffer, sizeof(char) * dir_name_length);
		if(dir_name_length > 1 && file_name_start[-1] != '/'){ //ensure there's a trailing slash on directory
			file_name_start[0] = '/';
			++file_name_start;
		}
		strcpy(file_name_start, this->name);

		struct stat stat_raw;

		int status = stat(buffer, &stat_raw);
		if(status != 0){
			//:TODO: have a way to skip bad files rather than closing the iterator completely
			//do internally? or make public?
			xen::log::write(XenWarn("While iterating over directory contents failed to get file stats for '%s'",
			                        buffer, "xen.file", "xen.unix"));
			impl::closeFileIterator(*this);
			return *this;
		}
		this->attributes = VALID;

		if ( S_ISDIR(stat_raw.st_mode) ){
			this->attributes |= TYPE_DIRECTORY;
		} else if ( S_ISLNK(stat_raw.st_mode) ){
			this->attributes |= TYPE_LINK;
		} else if( S_ISREG(stat_raw.st_mode) ){
			this->attributes |= TYPE_FILE;
		} else {
			this->attributes |= TYPE_UNKNOWN;
		}


		return *this;
	}

	FileIterator::~FileIterator(){
		impl::closeFileIterator(*this);
	}

	bool createDirectory(char* path, bool create_ancestors){
		//:TODO: this currently sets all permisions (ie: chmod a+wrx), is that what we want?
		auto permissions = S_IRWXU | S_IRWXG | S_IRWXO;

		auto status = mkdir(path, permissions);
		if(status == 0)    { return true; }
		if(errno == EEXIST) { return true; }

		if(errno == ENOENT) {
			//need to create parent first
			for(char* cur = xen::findFirst('/', path); cur != nullptr; cur = xen::findFirst('/', ++cur)){
			   *cur = '\0';
			   status = mkdir(path, permissions);
			   *cur = '/';
			   if(status != 0 && errno != EEXIST){ return false; }
			}
			return true;
		}
	}
}
