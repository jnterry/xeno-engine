////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file file.cpp
/// \author Jamie Terry
/// \date 2016/07/23
/// \brief Contains platform agnostic implementation for functions in file.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_FILE_CPP
#define XEN_CORE_FILE_CPP

#include "intrinsics.hpp"
#include "file.hpp"
#include "debug.hpp"
#include "../config.hpp"
#include <cstdio> //:TODO: reading file into mem using c std lib atm
#include <cerrno>
#include <cstring>


//this is before include platform specific file so those files can use this
namespace xen{
	namespace impl{
		/// \brief finds if string is the special name for "this directory" (.)
		/// or "up a directory" (..)
		bool isThisDirOrUpDirName(const char* str){
			if(str[0] == '.'){
				if(str[1] == '\0'){
					return true;
				} else if(str[1] == '.' && str[2] == '\0'){
					return true;
				}
			}
			return false;
		}
	}
}

#if XEN_OS_WINDOWS
    #include "file.win.cpp"
#elif XEN_OS_UNIX
    #include "file.unix.cpp"
#else
	#warning "File.hpp Not implemented on this platform, using dummy implementation"
	namespace xen{
		namespace impl{
			const char file_bad_platform_str[] = "file.hpp Not implemented on this platform";
		}

		FileIterator iterateDirectory(const char* directory){
			FileIterator it;
			it.attributes = FileIterator::VALID;
			xen::copyBytes(impl::file_bad_platform_str, it.name, sizeof(impl::file_bad_platform_str));
			return it;
		}

		FileIterator& FileIterator::operator++(){
			this->attributes = 0;
			return *this;
		}

		FileIterator::~FileIterator(){}

		bool createDirectory(char* path, bool create_ancestors){ return false; }
	}
#endif

namespace xen{
	//note: these rely on _data1 and _data2 being clearable to 0 to represent an invalid OS
	//dependent type
	//also FileIterator needs to be clearable to 0 such that it's operator(bool)
	//evaluates to false

	FileIterator::FileIterator(FileIterator&& other)
		: attributes(other.attributes), _data1(other._data1){
		xen::copyBytes(other.name, this->name, sizeof(this->name));
		xen::clearToZero(&other);
	}

   FileIterator& FileIterator::operator=(FileIterator&& other){
		this->attributes = other.attributes;
		this->_data1 = other._data1;
	   xen::copyBytes(other.name, this->name, sizeof(this->name));
		xen::clearToZero(&other);
		return *this;
	}

	void clearPath(Path& path){
		path.buffer[0]   = '\0';
		path.path_length = 0;
	}

	Path makePath(char* buffer, size_t buffer_length, const char* value, bool allow_fail){
		Path result;
		result.buffer.values = buffer;
		result.buffer.length = buffer_length;

		u32 value_length = strlen(value);
		if(value_length + 1 >= buffer_length){
			xen::log::write(XenFatalMaybe("Attempted to make path with value '%s' "
			                              "but destination buffer was too short", value, "xen.file"));
			*buffer = '\0';
			return result;
		}

		for(; *value; ++value, ++buffer){
			*buffer = *value == '\\' ? '/' : *value;
		}
		*buffer = '\0';
		result.path_length = value_length;

		return result;
	}

	bool push(Path& base, const char* end, bool allow_fail){
		while (*end == '/') { ++end; }
		char* dest = &base.buffer[base.path_length];
		u32 string_length = strlen(end);

		if(base.path_length > 0 && dest[-1] != '/'){
			dest[0] = '/';
			++dest;
			++base.path_length;
		}

		if(!xen::copyString(end, dest, base.buffer.length - base.path_length)){
			//:TODO: test off by 1 error?
			base.buffer[base.path_length] = '\0';
			xen::log::write(XenFatalMaybe("Attempted to push string '%s' onto path '%s', but not enough room in buffer",
			                              end, base.buffer.values, "xen.file"));
			return false;
		}
		base.path_length += string_length;
		base.buffer[base.path_length] = '\0';

		return true;
	}

	bool pop(Path& p){
		u32 cur = p.path_length;
		if(cur == 0) { return false; }
		if(cur == 1) { return false; }
		--cur;
		while(cur && p.buffer[cur] != '/'){ --cur; }
		p.path_length = cur + 1;
		p.buffer[p.path_length] = '\0';
		return true;
	}

	char* loadFile(const char* path, ArenaLinear& arena, bool allow_fail){
		//:TODO: fseek end file size deermination is broken when reading non-binary since
		//file size can be different to chars read, as on windows /r/n -> /n when read
		//also if file contains the byte 0 it will be interpreted as end of the returned
		//string when it shouldnt be, this probably only happen when actually loading binary
		//files -> use os specific functions?
		FILE* file = fopen(path, "rb");
		if(!file){
			xen::log::write(XenFatalMaybe("Attempted to load file into memory but it could not be opened, file: '%s'", path, "xen.file"));
			return nullptr;
		}

		size_t arena_space = bytesRemaining(arena);

		fseek(file, 0, SEEK_END);
		long file_size = ftell(file);
		if(file_size < 0){
			xen::log::write(XenFatalMaybe("Attempted to load file into memory but failed to determine file size, "
			                              "file: '%s', errno: %i, strerror: %s", path, errno, strerror(errno)));
		}
		fseek(file, 0, SEEK_SET);

		if(arena_space < (u64)file_size + 1){ //safe cast since already checked if file_size < 0
			xen::log::write(XenFatalMaybe("Attempted to load file into memory but target arena was too small, "
			                         "file: '%s', file size: %i, arena size: %i", path, file_size, arena_space, "xen.file"));
			return nullptr;
		}

		char* result = (char*)xen::reserveBytes(arena, (u64)file_size + 1);
		fread(result, (u64)file_size, 1, file);
		result[file_size] = '\0';
		fclose(file);

		return result;
	}
}

#endif
