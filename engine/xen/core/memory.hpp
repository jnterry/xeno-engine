////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file memory.hpp
/// \author Jamie Terry
/// \date 2016/07/17
/// \brief Contains a number of helper functions for dealing with pointers as well
/// as the types and functions required to interact with Xeno Engine's memory system.
///
/// Memory System
/// -------------
/// This file defines the interface to the system xen::SystemMemory. This is the first
/// system that must be initialized when starting the engine, since all other systems
/// allocate thieir memory through this, hence it has no dependencies.
///
/// ALL memory managed by Xenogin is allocated through the xen::allocateXXX functions,
/// these take a xen::Allocator as a parameter. These xen::allocateXXX functions should
/// be used sparingly; they are relativley slow as they request memory from the OS.
/// Instead various memory arenas should be used to manage raw blocks of memory
/// requested from the OS (this is what all the other Xeno Engine systems do)
///
/// Debugging
/// ---------
/// Xeno Engine provides helful features for debugging the memory usage of both the
/// engine and application.
///
/// The debugging system is enabled when XEN_DEBUG_MEMORY is defined when compiling
/// the engine. Note that the system is intended to have low enough overhead that it
/// can be left enabled in production.
///
/// The system organises the Allocators and Arenas into a tree, this tree can be
/// examinded using xen::MemoryDebugNodeg
///
/// \see xen::SystemMemory
/// \see xen::Allocator
/// \see xen::ArenaLinear
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_MEMORY_HPP
#define XEN_CORE_MEMORY_HPP

#include "intrinsics.hpp"
#include "debug.hpp"
#include "../config.hpp"

#include <cstddef>
#include <new>

namespace xen{
	/// \struct SystemMemory
	/// \brief Opaque type which represents all the memory that Xeno Engine's
	/// memory system requies to operate
	struct SystemMemory;

	
	struct Allocator;

	namespace _sys{
		extern SystemMemory* memory;
	}

	/// \brief Creates a new SystemMemory instance, this is the first system that needs to
	/// initialized, since all other systems require memory
	/// \public \memberof SystemMemory
	SystemMemory* createSystemMemory();

	/// \breif Destroys a SystemMemory instance
	/// \public \memberof SystemMemory
	void destroySystemMemory(SystemMemory* system);

	/// \brief Creates a new SystemMemory instance and installs it as the default global
	/// memory system
	/// \public \memberof SystemMemory
	inline bool initializeSystemMemory(){
		_sys::memory = createSystemMemory();
		return _sys::memory != nullptr;
	}

	/// \breif Destroys the SystemMemory instance that is installed as the defaul global
	/// memory system
	/// \public \memberof SystemMemory
	inline void shutdownSystemMemory(){
		destroySystemMemory(_sys::memory);
		_sys::memory = nullptr;
	}

	/// \brief Retrieves the root allocator for Xeno Engine, all memory that is allocated
	/// is done through this or one of its children
	/// \public \memberof SystemMemory
	Allocator& getRootAllocator(SystemMemory* system = _sys::memory);

	/// \name Misc Helper Functions
	/// @{
	
	/// \brief Returns a number of kilobytes as a number of bytes
	inline constexpr size_t kilobytes(const size_t kb) { return kb            * 1024; }

	/// \brief Returns a number of megabytes as a number of bytes
	inline constexpr size_t megabytes(const size_t mb) { return kilobytes(mb) * 1024; }

	/// \brief Returns a number of gigabytes as a number of bytes
	inline constexpr size_t gigabytes(const size_t gb) { return megabytes(gb) * 1024; }

	inline void* copyBytes(const void* const from, void* const to, size_t num_bytes){
		u8* source = (u8*)from;
		u8* dest   = (u8*)to;
		while(num_bytes--) { *dest++ = *source++; }
		return to;
	}

	/// @}


	/// \name Pointer Manipulation Helpers
	/// @{
	
	/// \brief Advances the pointer ptr such that its address mod align is 0,
	/// \return Result of the alignment
	inline void* ptrGetAlignedForward(const void* const ptr, size_t align){
		uintptr_t ptr_int = uintptr_t(ptr);
		const uintptr_t mod = ptr_int % align;
		if (mod){ ptr_int += (align - mod); }
		return (void *)ptr_int;
	}

	/// \brief Advances a pointer (if nessaccery) such that its address mod align is 0
	/// \param ptr Pointer to the pointer to align, function modifies this!
	/// \return Number of bytes the pointer was advanced
	inline u32 ptrAlignForward(void** ptr, u32 align){
		uintptr_t ptr_int = uintptr_t(*ptr);
		u32 delta = ptr_int % align;
		if(delta) { delta = align - delta; }
		*ptr = (void*)(ptr_int + delta);
		return delta;
	}

	/// \brief Increases the value of a pointer by some number of bytes, useful to
	/// ensure it is moved by the number of bytes rather than by a multiple of the
	/// size of the pointed at object
	inline void* ptrAdvance(void* ptr, size_t bytes){
		return (void*)(((uint8_t*)ptr) + bytes);
	}
	/// \overload
	inline const void* ptrAdvance(const void* ptr, size_t bytes){
		return (const void*)(((const uint8_t*)ptr) + bytes);
	}

	/// \brief Decreases the value of a pointer by some number of bytes, useful to
	/// ensure it is moved by the number of bytes rather than by a multiple of the
	/// size of the pointed at object
	inline void* ptrRetreat(void* ptr, size_t bytes){
		return (void*)(((uint8_t*)ptr) - bytes);
	}
	/// \overload
	inline const void* ptrRetreat(const void* ptr, size_t bytes){
		return (const void*)(((const uint8_t*)ptr) - bytes);
	}

	/// \brief Returns the distance in bytes between two pointers
	/// \return The number of bytes that ptrA needs to be advanced by in order for
	/// it to be equal to ptrB, return type depends on platform but is always a
	/// signed type
	inline ptrdiff_t ptrDiff(const void* ptrA, const void* ptrB){
		return ((const uint8_t*)ptrB) - ((const uint8_t*)ptrA);
	}
	/// @}
	
	struct MemoryBlock{
		void*  start; ///< Pointer to the first byte in the block
		void*  end;   ///< Pointer to the last byte in the block
		//:TODO: would it be easier if end was byte past the end (eg, std::vector.end())?
		//also maths for getting block size means we need to + 1 (see blockSize)
	};

	/// \brief Determines the number of bytes in a MemoryBlock
	/// \public \memberof xen::MemoryBlock
	inline size_t blockSize(const MemoryBlock& block){ return (size_t)ptrDiff(block.start, block.end) + 1; }

	/// \brief A block of memory which keeps track of the next byte in the block to be used,
	/// provides memory sequentially from the start to the end of the block
	struct ArenaLinear : public MemoryBlock{
		void* _next_byte; ///< The next byte to write into, ie, lowest unused address

		//private:
		//ArenaLinear(ArenaLinear& other){}
	};

	struct MemoryDebugNode;

	struct Allocator : NonCopyable{
		friend struct SystemMemory;
		const char* name;   ///< Debug name of thie Allocator
		u32    allocations; ///< Number of un-freed allocations made by THIS allocator
		
		/// \name Debug Members
		/// \note These members are only updated if XEN_DEBUG_MEMORY is defined at compile time, otherwise
		/// they will contain garbage data
		/// @{

		size_t _allocated;   ///< Total number of bytes in un-freed allocations made by THIS allocator

		/// \brief The MemoryDebugNode for this allocator
		MemoryDebugNode* _debug_node;
		
		/// \brief Number of un-freed allocations made by this and the decendents of this allocator
		size_t _decendent_allocations;

		///< \brief Total number of bytes in un-freed allocations made by this and the decendents of this allocator
		size_t _decendent_allocated;

		SystemMemory* _system;

		/// @}
		~Allocator();
	private:
		/// \breif Private constructor, you must initialize the memory system with
		/// xen::initialzeSystemMemory and then use xen::getRootAllocator
		Allocator();
	};

	/// \brief Represents a node in the tree of Allocators and Arena and contains debug
	/// infomation about the node in question
	///
	/// \note Memory debugging only occurs if the engine is compiled with XEN_DEBUG_MEMORY
	/// defined
	struct MemoryDebugNode{
		enum Types{
			ALLOCATOR,
			ARENA_LINEAR,
		};
		const char* name;

		union{
			void*        object;
			Allocator*   allocator;
			ArenaLinear* arena_linear;
		};

		Types type;

		//:TODO:COMP: helper functions for tree traversal with CRTP base
		MemoryDebugNode* first_child;
		MemoryDebugNode* next_sibling;
		MemoryDebugNode* parent;
	};

	/// \brief Gets the root MemoryDebugNode, useful for examining the memory usage behaviour
	/// of both Xeno Engine and the program
	///
	/// \param system The SystemMemory whose root node you wish to get, defaults to the
	/// global SystemMemory
	///
	/// \return The root node, or nullptr if memory debuging disabled in this build (define
	/// XEN_DEBUG_MEMORY when building the engine to enable memory debuging)
	///
	/// \public \memberof SystemMemory
	const MemoryDebugNode* getRootMemoryDebugNode(const SystemMemory* system = _sys::memory);

	/// \brief Allocates a new MemoryBlock with the specified number of bytes
	/// \note MemoryBlock can be implictly cast to void*, hence this can be used in same way as malloc
	/// \public \memberof Allocator
	//MemoryBlock allocate(Allocator& alloc, size_t num_bytes, bool allow_fail);

	/// \brief Allocates a new ArenaLinear with a size of the specified number of bytes
	/// \param allow_fail If set to true then a failure to allocate memory will not be treated
	/// as an error, nullptr will be returned. If false (as is the default) a failure to allocate
	/// member will result in a log fatal
	/// \public \memberof Allocator
	ArenaLinear* allocateArenaLinear(Allocator& alloc, size_t num_bytes, const char* name = "<unnamed>", bool allow_fail = false);

	/// \brief Deallocates memory previously allocated with one of the xen::allocateXXX functions
	/// \public \memberof Allocator
	void deallocate(Allocator& alloc, void* address);

	/// \brief Determines the number of bytes that can still be used in the specified ArenaLinear
	/// after aligning the next available byte forward so that `align` is obeyed
	/// \public \memberof ArenaLinear
	inline size_t bytesRemaining(const ArenaLinear& arena, u32 align = 1) {
		return (size_t)ptrDiff(ptrGetAlignedForward(arena._next_byte, align), arena.end);
	}
	
	/// \name clearToZero
	/// \brief Clears a region of memory such that all bits in the region are set to 0
	///@{
	inline void* clearToZero(void* first_byte, size_t num_bytes){
		u8* cur = (u8*)first_byte;
		while(num_bytes--) { *cur++ = 0; }
		return first_byte;
	}
	inline void* clearToZero(MemoryBlock& block){ return clearToZero(block.start, blockSize(block)); }
	template<typename T>
	inline void* clearToZero(Array<T>& array){ return clearToZero(array.values, sizeof(T) * array.length); }
	template<typename T>
	inline void* clearToZero(T* obj) { return clearToZero(obj, sizeof(T)); }
	///@}
		
	/// \brief Resets an ArenaLinear such that all the space can be used again, existing allocations
	/// will be overwritten by the new ones, does not call any destructors
	/// \public \memberof xen::ArenaLinear
	inline ArenaLinear& resetArena(ArenaLinear& arena){
		arena._next_byte = arena.start;
		return arena;
	}
	  
	/// \brief Reserves some number of bytes in an Arena and returns pointer to the first, does not initialize the bytes
	/// \public \memberof xen::ArenaLinear
	inline void* reserveBytes(ArenaLinear& arena, size_t num_bytes, u32 align = alignof(int)){
		void* result = arena._next_byte;
		ptrAlignForward(&result, align);
		arena._next_byte = ptrAdvance(result, num_bytes);
		XenAssert(arena._next_byte <= arena.end, "ArenaLinear too full to reserve bytes");
		return result;
	}

	/// \brief Reserves space for some type in an ArenaLinear, does not initialize the reserved space
	/// \param count The number of instances of the type to reserve space for, puts them in continuous array
	/// \public \memberof xen::ArenaLinear
	template<typename T>
	inline T* reserve(ArenaLinear& arena, u32 count = 1, u32 align = alignof(T)){
		return (T*)reserveBytes(arena, sizeof(T) * count, align);
	}

	/// \brief Reserves space for an array of elements of type T of length \c element_count, returns result
	/// as a xen::Array<T>, if you want a raw C array just use xen::Array<T>::values
	/// \note Does not call any constructors or initialize the elements of the array
	/// \public \memberof xen::ArenaLinear
	template<typename T>
	inline Array<T> reserveArray(ArenaLinear& arena, const size_t element_count, u32 align = alignof(T)){
		Array<T> result;
		result.length = element_count;
		result.values = (T*)reserveBytes(arena, sizeof(T) * element_count, align);
		return result;
	}

	/// \brief Copies some number of bytes onto end of an ArenaLinear, returns pointer to first byte
	/// \public \memberof xen::ArenaLinear
	inline void* pushBytes(ArenaLinear& arena, const void* const data, size_t num_bytes, u32 align = alignof(int)){
		void* result = reserveBytes(arena, num_bytes, align);
		return copyBytes(data, result, num_bytes);
	}

	/// \brief copies some type onto end of arena, returns pointer to the created copy
	/// \public \memberof xen::ArenaLinear
	template<typename T>
	inline T* push(ArenaLinear& arena, const T* const data, u32 align = alignof(T)){
		return (T*)(pushBytes(arena, data, sizeof(T), align));
	}

	/// \brief copies some type onto end of arena, returns pointer to the created copy
	/// \public \memberof xen::ArenaLinear
	template<typename T>
	inline T* push(ArenaLinear& arena, const T& data, u32 align = alignof(T)){
		return (T*)(pushBytes(arena, &data, sizeof(T), align));
	}

	/// \brief Copies the elements of some array onto the end of an area, returns a pointer
	/// to the created copy
	/// \public \memberof xen::ArenaLinear
	template<typename T>
	inline T* pushArrayData(ArenaLinear& arena, const xen::Array<T>& array, u32 align = alignof(T)){
		return (T*)pushBytes(arena, array.values, sizeof(T) * array.length, align);
	}

	/// \brief Copies a string and its null terminator, returns pointer to the first char
	/// \public \memberof xen::ArenaLinear
	inline const char* pushString(ArenaLinear& arena, const char* str, u32 align = alignof(char)){
		return (char*)pushBytes(arena, str, strlen(str) + 1, align);
	}		

	/// \breif Copies a string WITHOUT its null terminator, returns pointer to the first char
	/// \public \memberof xen::ArenaLinear
	inline const char* pushStringNoTerminate(ArenaLinear& arena, const char* str, u32 align = alignof(char)){
		return (char*)pushBytes(arena, str, strlen(str), align);
	}

	/// \brief Copies the first \c num_chars of a string, or the whole string if
	/// \c strlen(str) < \c num_chars, then appends a null terminator
	/// \public \memberof xen::ArenaLinear
	inline const char* pushStringPrefix(ArenaLinear& arena, const char* str
	                                    , u32 num_chars, u32 align = alignof(char)) {
		u32 length = strlen(str);
		if (length < num_chars) { num_chars = length; }
		char* result = (char*)pushBytes(arena, str, num_chars, align);
		push<char>(arena, '\0', 1);
		return result;
	}

	/// \brief pushes a formatted string to the buffer
	/// \param arena         The arena to push to
	/// \param format_string The format string (syntax same as printf)
	/// \param ...           The parameters to be formatted
	///
	/// The resulting string will always be null terminated and the arena's bounds will never be
	/// exceeded
	///
	/// \return Pointer to first character of the string, or nullptr if not even the null terminator
	/// could be written (since there was no space after aligning the pointer)
	///
	/// \public \memberof xen::ArenaLinear
	const char* pushFormatted(ArenaLinear& arena, const char* format_string, ...);
		
	/// \brief Reserves space for some type in specified arena, then initializes the
	/// bytes with placement new and the specified args
	/// \tparam T The type to emplace
	/// \tparam T_ARGS The arguements to pass to the constuctor of T
	/// \public \memberof xen::ArenaLinear
	template<typename T, typename... T_ARGS>
	inline T* emplace(ArenaLinear& arena, T_ARGS... args){
		return new (reserveBytes(arena, sizeof(T), alignof(T))) (T)(args...);
	}

	/// \brief Rolls an ArenaLinear back to a previous point, think of this as pop-ing
	/// an allocation off of the arena
	void rollback(ArenaLinear& arena, void* prev_alloc){
		XenAssert(arena.start <= prev_alloc && prev_alloc <= arena.end, "Attempted to rollback ArenaLinear to point outside its buffer");
		arena._next_byte = prev_alloc;
	}

	/// \breif Dumps the debug memory hierachy (starting at the specified root node) to a
	/// human readable string, pushes result into specified arena
	///
	/// \todo :TODO: when make the diagnostics.hpp module (with other debugging stuff) this
	/// should be put in there, since atm it uses public interface of memory system, doesnt
	/// really need to be in here
	const char* debugMemoryDumpHierachy(ArenaLinear* arena, const MemoryDebugNode* root = getRootMemoryDebugNode(), u32 indent_level = 0);
}

// use xen::Allocator, xen::allocate, xen::deallocate
//#define malloc  #error BAD CALL use xen::allocate
//#define new     #error BAD CALL use xen::allocate

#endif
