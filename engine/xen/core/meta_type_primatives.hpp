////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file meta_type_primatives.hpp
/// \author Jamie Terry
/// \date 2016/08/02
/// \brief Contains the MetaType definitions for various primative and intrinsic
/// types
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_META_TYPE_PRIMATIVES_HPP
#define XEN_CORE_META_TYPE_PRIMATIVES_HPP

namespace xen{
	namespace impl{
		///////////////////////////////////////////////////////////////////////////////////
		// Primatives
		template<> const MetaType TypeOf<u08   >::type = { 0, "u08",    {0}, sizeof(u08)};
		template<> const MetaType TypeOf<u16   >::type = { 1, "u16",    {0}, sizeof(u16)};
		template<> const MetaType TypeOf<u32   >::type = { 2, "u32",    {0}, sizeof(u32)};
		template<> const MetaType TypeOf<u64   >::type = { 3, "u64",    {0}, sizeof(u64)};
		template<> const MetaType TypeOf<s08   >::type = { 4, "s08",    {0}, sizeof(s08)};
		template<> const MetaType TypeOf<s16   >::type = { 5, "s16",    {0}, sizeof(s16)};
		template<> const MetaType TypeOf<s32   >::type = { 6, "s32",    {0}, sizeof(s32)};
		template<> const MetaType TypeOf<s64   >::type = { 7, "s64",    {0}, sizeof(s64)};
		template<> const MetaType TypeOf<real32>::type = { 8, "real32", {0}, sizeof(real32)};
		template<> const MetaType TypeOf<real64>::type = { 9, "real64", {0}, sizeof(real64)};
		template<> const MetaType TypeOf<bool  >::type = {10, "bool",   {0}, sizeof(bool)};
		//template<> const MetaType TypeOf<long  >::type = {11, "long",   {0}, sizeof(long)};
		//template<> const MetaType TypeOf<unsigned long>::type = {12, "unsigned long", {0}, sizeof(unsigned long) };

		///////////////////////////////////////////////////////////////////////////////////
		// xen::Array
		// Technically the C standard doesn't guarrenty size of all pointers are the same
		// or the memory layout of a struct, but in practice every modern desktop compiler
		// works as expected. static_asserts are just to check our assumptions are true (ie:
		// data layout for xen::Array<T> is same for all T - ->length at start, the ->values
		// at same point for all T
		static_assert(sizeof(xen::Array<u32>) == sizeof(xen::Array<xen::Array<real>>),
		              "This relies on sizeof(xen::Array<T>) being the same for all T");

		static_assert(sizeof(xen::Array<u32>) == sizeof(xen::Array<u32*>),
		              "This relies on sizeof(xen::Array<T>) being the same for all T");

		static_assert(sizeof(xen::Array<u32>) == sizeof(xen::Array<const u32*>),
		              "This relies on sizeof(xen::Array<T>) being the same for all T");

		static_assert(sizeof(void*) == sizeof(int*),
		              "This relies on sizeof(T*) being the same for all T");

		static_assert(sizeof(void*) == sizeof(const int*),
		              "This relies on sizeof(T*) being the same for all T");

		static_assert(sizeof(void*) == sizeof(xen::MetaType*),
		              "This relies on sizeof(T*) being the same for all T");

		static_assert(sizeof(void*) == sizeof(xen::MetaType**),
		              "This relies on sizeof(T*) being the same for all T");

		static_assert(sizeof(void*) == sizeof(&xen::MetaTypeField::OFFSET_UNKNOWN),
		              "This relies on sizeof(T*) being the same for all T");

		static_assert(offsetof(xen::Array<u32>, length) == 0,
		              "This relies on ->length being at the very start of xen::Array<T> for all T");

		static_assert(offsetof(xen::Array<xen::Array<float>*>, length) == 0,
		              "This relies on ->length being at the very start of xen::Array<T> for all T");

		static_assert(offsetof(xen::Array<xen::Array<float>*>, length) == 0,
		              "This relies on ->length being at the very start of xen::Array<T> for all T");

		static_assert(offsetof(xen::Array<u32>, values) == offsetof(xen::Array<real>, values),
		              "This relies on xen::Array<T>::values");

		static_assert(offsetof(xen::Array<u32>, values) == offsetof(xen::Array<void*>, values),
		              "This relies on xen::Array<T>::values");

		static_assert(offsetof(xen::Array<u32>, values) == offsetof(xen::Array<xen::Array<s32>>, values),
		              "This relies on xen::Array<T>::values");


		static_assert(std::is_same<decltype(xen::Array<u32>::length), size_t>::value,
		              "Expected length field to be a size_t, this needs to be updated if xen::Array is modifed");
		static MetaTypeField type_xen_Array_fields[] = {
			{"length", &TypeOf< decltype(xen::Array<_DummyInstantiator>::length) >::type, 0, 0, 0},
			{"values", &xen::MetaTParam[0], 0, offsetof(xen::Array<int>, length), MetaTypeField::KIND_DYNAMIC_ARRAY},
		};

		template<typename T>
		struct TypeOf< xen::Array<T> >{
			static MetaTypeField fields[];
			const static MetaType type;
		};
		template<typename T>
		MetaTypeField TypeOf< xen::Array<T> >::fields[] = {
			{"length", &TypeOf< decltype(xen::Array<T>::length) >::type, 0, 0, 0},
			{"values", &TypeOf<T>::type, 0, offsetof(xen::Array<T>, values), MetaTypeField::KIND_DYNAMIC_ARRAY},
		};
		template<typename T>
		const MetaType TypeOf< xen::Array<T> >::type = {
			14,
			"xen::Array<T>",
			{2, TypeOf< xen::Array<T> >::fields},
			sizeof(xen::Array<T>)
		};

		template<>
		const MetaType TypeOf<xen::Array<_DummyInstantiator>>::type = {
			13, "xen::Array",
			xen::makeArray(type_xen_Array_fields),
			sizeof(xen::Array<_DummyInstantiator>)
		};
	}
}

#endif
