////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file log.cpp
/// \author Jamie Terry
/// \date 2016/07/18
/// \brief Contains defintion of types and functions declared in log.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#include "intrinsics.hpp"
#include "debug.hpp"
#include "memory.hpp"

#include <cstdarg>
#include <cstdio>

// :TOOD: add a arena that buffers log messages, then writing is done on seperate thread?

// :TODO: make log writing thread safe (if have backing store that buffers messages then just need to
// make sure only one thread writes to each, then the worker thread that outputs messages
// just reads from all those buffers)

// :TODO: make tag creation thread safe (mutex when creating tags? -> should be rare enough it doesn't cause issues)
// also need to ensure that if another thread is already searching for a tag that it doesn't create a duplicte

// :TODO: make some nice outputters (html with js filters for a start) + have a way to register them

#include "../config.hpp"
#ifdef XEN_OS_WINDOWS
//:TODO: This is for custom colors when printing to stdout, log output tempory only, move out
//into own file when implementing proper log outputs (+provide calls to change stdout colour?)
#include "../windows_header.hpp"

namespace xen{
	namespace impl{
		WORD getTextStyleForLogLevel(xen::log::Level level){
			switch(level){
			case xen::log::FATAL_DEV:
			case xen::log::FATAL:
				return BACKGROUND_RED;
			case xen::log::ERROR:
				return FOREGROUND_RED;
			case xen::log::WARN:
				return FOREGROUND_RED | FOREGROUND_GREEN;
			case xen::log::INFO:
				return FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			default:
				return FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;

			}
		}
	}
}

#endif


namespace xen{
	namespace log{
		constexpr const char* _root_tag_name = "";
	}
}

namespace xen{
	struct SystemLog{
		Allocator* allocator;
		
		/// \brief Arena for storing tag hierachy
		ArenaLinear* tag_store;

		/// \brief The root of the tag hierachy
		log::Tag root_tag;
	};
	
	namespace _sys{
		SystemLog* log = nullptr;
	}

	SystemLog* createSystemLog(Allocator& alloc, size_t num_bytes){
		ArenaLinear* arena = allocateArenaLinear(alloc, num_bytes, "Tag Store");
		SystemLog* result  = reserve<SystemLog>(*arena);
		result->tag_store = arena;
		result->allocator  = &alloc;

		result->root_tag = xen::log::Tag(xen::log::_root_tag_name);


	   xen::log::write(result, __FILE__, __LINE__, xen::log::INFO, "Initialized logging system", "xen.systems", nullptr);
		return result;
	}
	
	void destroySystemLog(SystemLog* system){
		//:TODO: when implemented message buffering flush the log here
		
		xen::log::write(system, __FILE__, __LINE__, xen::log::INFO, "Shutting down logging system", "xen.systems", nullptr);
		
		deallocate(*system->allocator, system->tag_store);
	}
	
	namespace log{
		Tag* getRootTag(SystemLog* system){
			return &system->root_tag;
		}
		
		const Tag* getDecendent(Tag* ancestor, const char* child_path, SystemLog* system){
			const char* remaining_path = child_path;
			Tag* cur_tag = ancestor;
			bool found_child = false;

			while(remaining_path[0] != '\0'){
				//skip all the dots, this makes xen...render equivalent to xen.render
				if(*remaining_path == '.') { ++remaining_path; continue; }

				s32 dot_index = indexOfFirst('.', remaining_path);
				if (dot_index < 0) { 
					dot_index = indexOfFirst('\0', remaining_path); 
				}
				found_child = false;

				for(Tag* cur_child = cur_tag->_first_child; cur_child != nullptr; cur_child = cur_child->_next_sibling){
					if(stringEquals(remaining_path, cur_child->name, (u32)dot_index)){
						//then we've found the next child
						cur_tag = cur_child;
						found_child = true;
						break;
					}
				}

				if(!found_child){//then create the child
					// create child
					Tag* new_tag = xen::reserve<Tag>(*system->tag_store);
					new_tag->parent = cur_tag;
					new_tag->_next_sibling = cur_tag->_first_child;
					new_tag->_first_child  = nullptr;

					if(cur_tag != &system->root_tag){
						new_tag->full_name = xen::pushStringNoTerminate(*system->tag_store, cur_tag->full_name);
						xen::pushStringNoTerminate(*system->tag_store, ".", 1);
						new_tag->name = xen::pushStringPrefix(*system->tag_store, remaining_path, (u32)dot_index, 1);
					} else {
						new_tag->full_name = xen::pushStringPrefix(*system->tag_store, remaining_path, (u32)dot_index, 1);
						new_tag->name = new_tag->full_name;
					}

					// add to parent
					cur_tag->_first_child = new_tag;

					// set new tag as current, so remaining_path is added as child of it
					cur_tag = new_tag;
				}

				remaining_path = &remaining_path[dot_index];
			} //end while

			return cur_tag;
		}

		bool hasTag(const Message& msg, const Tag* tag){
			for(auto it = xen::begin(msg._leaf_tags); it; ++it){
				// :TODO:OPT: -> if all tags are in the tag_store (they should be) and
				// are added in order such that parents are always before children (they should be) then
				// if address of tag > cur it is not possible for tag to be one of the ancestors of cur
				for(const Tag* cur = *it; cur->parent != nullptr; cur = cur->parent){
					if(cur == tag){ return true; }
				}
			}
			return false;
		}

		const char* getLevelText(Level level, bool fixed_width){
			//:TODO:COMP: macro which defines an enum with string names already defined in array
			// -> might also help with meta type system since can display name of enum values
			if(level >= FATAL)    { return               "FATAL";               }
			if(level >= FATAL_DEV){ return fixed_width ? "FTLDV" : "FATAL_DEV"; }
			if(level >= ERROR)    { return               "ERROR";               }
			if(level >= WARN)     { return fixed_width ? " WARN" : "WARN";      }
			if(level >= INFO)     { return fixed_width ? " INFO" : "INFO";      }
			if(level >= DEBUG)    { return               "DEBUG";               }
			if(level >= TRACE)    { return               "TRACE";               }
			InvalidCodePath; return "";
		}

		void write(SystemLog* system, const char* file, u32 line_num, Level level, const char* msg_format, ...){
			#ifdef XEN_LOG_NO_TRACE
			if(level == xen::log::TRACE){ return; }
			#endif
			//:TODO: if system == nullptr then just output to stdout (basically the code below, keep it when we actually implement the log)
			//:TODO: write data to a buffer, then output to registed outputs (on another thread?)
			//:TODO: generate a tag for the thread we're on, eg thread.[THREAD_ID] or thread.[THREAD_NAME]

			#ifdef XEN_OS_WINDOWS
			HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			CONSOLE_SCREEN_BUFFER_INFO original_console_settings;
			GetConsoleScreenBufferInfo( hStdout, &original_console_settings );
			SetConsoleTextAttribute( hStdout, impl::getTextStyleForLogLevel(level) );
			#endif

			xen::DateTime dt = xen::getLocalTime();
			char time_str[10];
			xen::formatDateTime(dt, time_str, XenArrayLength(time_str), "%H:%M:%S"); //:TODO: add support to format to get milliseconds
			printf("[%s]%s|%s:%i  ", time_str, getLevelText(level), file, line_num);

			///////////////////////////////////////////////////////
			// message
			va_list args;
			va_start(args, msg_format);
			vprintf(msg_format, args);
			va_end(args);

			///////////////////////////////////////////////////////
			// tags
			const char* tag_format = nullptr;

			// :TODO: when actually dealing with tags fix this -> %% is escaped, so shouldn't be counted
			u32 discard_count = xen::countXIn('%', msg_format);
			
			va_start(args, msg_format);
			
			//:TODO: the tag extractions works ish, skipping just u32 only works if everything
			//is 4 bytes, anything larger (double, ptr on 64 bits) wont work correctly
			//need to examine the format string to work it out - note that c varags (...) promotes
			//types, eg, u8 -> u32, float -> double, etc
			for(u32 i = 0; i < discard_count; ++i){ va_arg(args, u32); }
			tag_format = va_arg(args, const char *);

			if(tag_format != nullptr){
				printf(" (TAGS: ");
				
				do{
					vprintf(tag_format, args);
					printf(", ");
					discard_count += xen::countXIn('%', tag_format) + 1; //+1 to also discard the tag format string
					va_end(args);
					va_start(args, msg_format);
					for(u32 i = 0; i < discard_count; ++i){ va_arg(args, u32); }
					tag_format = va_arg(args, const char *);
				} while(tag_format != nullptr);

				printf(")");
			}
			
			va_end(args);


			#ifdef XEN_OS_WINDOWS
			SetConsoleTextAttribute( hStdout, original_console_settings.wAttributes );
			#endif
			
			printf("\n");
		}
	}
}

