////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file time.cpp
/// \author Jamie Terry
/// \date 2016/07/22
/// \brief Contains platform agnostic implementation for functions in time.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_TIME_CPP
#define XEN_CORE_TIME_CPP

#include <ctime>

#include "time.hpp"
#include "../config.hpp"

#if XEN_OS_WINDOWS
    #include "time.win.cpp"
#elif XEN_OS_UNIX
    #include "time.unix.cpp"
#else
    #error Time not implemented on this OS
#endif

//:TODO: haven't done rigorous testing to check the platform specific time representations
//are being converted safely - ie, dont wrap, esp for getTimeStamp

namespace xen{
	namespace impl{ std::tm asCTime(const DateTime& dt); } //impl in the platform files
	
	char* pushDateTimeString(ArenaLinear& arena, DateTime dt, const char* format, u32 align){
		char* start = (char*)ptrGetAlignedForward(arena._next_byte, align);
		ptrdiff_t buffer_length = ptrDiff(start, arena.end);
		if(buffer_length > 0){
			size_t bytes_written = formatDateTime(dt, start, bytesRemaining(arena), format);
			arena._next_byte = ptrAdvance(arena._next_byte, bytes_written);
			return start;
		} else {
			//:TODO: allow fail flag (combine with align?) - for all push functions
			xen::log::write(XenFatal("Attempted to push formatted DateTime string to arena, but there was no space"));
			return nullptr;
		}
	}

	size_t formatDateTime(DateTime dt, char* buffer, size_t buffer_length, const char* format){
		std::tm dt_tm = impl::asCTime(dt);

	   auto result = strftime(buffer, buffer_length, format, &dt_tm);
		if(result == 0){
			buffer[0] = '\0';
			return 1;
		} else {
			return result + 1; //+1 for null terminator
		}
	}
}

#endif

