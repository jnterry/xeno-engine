////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file meta_types.cpp
/// \author Jamie Terry
/// \date 2016/07/20
/// \brief Contains definitions of functions and types declared in meta_types.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_META_TYPES_CPP
#define XEN_CORE_META_TYPES_CPP

#include "memory.hpp"
#include "debug.hpp"
#include "meta_types.hpp"
#include "time.hpp"
#include "intrinsics.hpp"

namespace xen{
	namespace _sys{
		SystemMetaType* meta_type = nullptr;
	}

	MetaType MetaTParam[8] = {
		{ 0, "TParam0", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam1", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam2", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam3", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam4", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam5", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam6", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
		{ 0, "TParam7", {0}, xen::MetaType::STATIC_SIZE_UNKNOWN},
	};
	

	SystemMetaType* createSystemMetaType(Allocator& alloc, size_t bytes){
		if(_sys::log != nullptr){
			xen::log::write(XenInfo("Initializing Xeno Engine MetaType System, arena size: %i kb", bytes/1024, "xen.systems", "xen.meta_types"));
		}
		xen::Stopwatch timer;

		//:TODO: split more sensibly than just /2, also allocate as one block and carve seperate arenas out of it
		//dont forget to update destroySystemMetaType as well!
		ArenaLinear* type_arena  = xen::allocateArenaLinear(alloc, bytes/2, "MetaType Store");
		ArenaLinear* alias_arena = xen::allocateArenaLinear(alloc, bytes/2, "MetaType Aliases"); 
		SystemMetaType* result   = xen::reserve<SystemMetaType>(*type_arena); //:TODO: put this at start of block rather than in arena?

		result->allocator         = &alloc;
		result->meta_type_store   = type_arena;
		result->alias_store       = alias_arena;
		result->meta_type_next_id = 0;
		for(u32 i = 0; i < XenArrayLength(result->aliases); ++i){ result->aliases[i] = nullptr; }

		registerTypeAlias("char",               typeOf<char              >(), result);
		registerTypeAlias("unsigned char",      typeOf<unsigned char     >(), result);
		registerTypeAlias("short",              typeOf<short             >(), result);
		registerTypeAlias("unsigned short",     typeOf<unsigned short    >(), result);
		registerTypeAlias("int",                typeOf<int               >(), result);
		registerTypeAlias("unsigned int",       typeOf<unsigned int      >(), result);
		registerTypeAlias("long",               typeOf<long              >(), result);
		registerTypeAlias("unsigned long",      typeOf<unsigned long     >(), result);
		registerTypeAlias("long long",          typeOf<long long         >(), result);
		registerTypeAlias("unsigned long long", typeOf<unsigned long long>(), result);
		registerTypeAlias("float",              typeOf<float             >(), result);
		registerTypeAlias("double",             typeOf<double            >(), result);
		
		registerTypeAlias("u08",                typeOf<u08               >(), result);
		registerTypeAlias("u16",                typeOf<u16               >(), result);
		registerTypeAlias("u32",                typeOf<u32               >(), result);
		registerTypeAlias("u64",                typeOf<u64               >(), result);
		registerTypeAlias("s08",                typeOf<s08               >(), result);
		registerTypeAlias("s16",                typeOf<s16               >(), result);
		registerTypeAlias("s32",                typeOf<s32               >(), result);
		registerTypeAlias("s64",                typeOf<s64               >(), result);
		registerTypeAlias("bool",               typeOf<bool              >(), result);
		registerTypeAlias("real32",             typeOf<real32            >(), result);
		registerTypeAlias("real64",             typeOf<real64            >(), result);
		registerTypeAlias("real",               typeOf<real              >(), result);
		
		registerTypeAlias("b8",                 typeOf<b8>                (), result);
		registerTypeAlias("b32",                typeOf<b32>               (), result);
		registerTypeAlias("u8",                 typeOf<u8>                (), result);
		registerTypeAlias("s8",                 typeOf<s8>                (), result);
		registerTypeAlias("int8",               typeOf<int8>              (), result);
		registerTypeAlias("uint8",              typeOf<uint8>             (), result);
		registerTypeAlias("int16",              typeOf<int16>             (), result);
		registerTypeAlias("uint16",             typeOf<uint16>            (), result);
		registerTypeAlias("int32",              typeOf<int32>             (), result);
		registerTypeAlias("uint32",             typeOf<uint32>            (), result);
		registerTypeAlias("int64",              typeOf<int64>             (), result);

		float time_taken = xen::asSeconds<float>(timer.getElapsedTime());
		//:TODO: put this back into float when we've fixed the log writting (see ::write func)
		xen::log::write(XenInfo("Initialized Xeno Engine MetaType System in %i seconds", (int)time_taken, "xen.systems", "xen.meta_types"));

		return result;

	}

	void destroySystemMetaType(SystemMetaType* system){
		xen::deallocate(*(system->allocator), system->alias_store);
		xen::deallocate(*(system->allocator), system->meta_type_store);
	}

	/// \brief Creates new MetaType in specified system, automattically registers alias for the type
	const MetaType* _emplaceMetaType(const char* name, const xen::Array<MetaTypeField>& fields, u32 static_size, SystemMetaType* system){
		//:TODO: thread safety requird? -> getting next id and pushing data to the meta_type_store
		/*MetaType* result       = (MetaType*)xen::reserve<MetaType>(*(system->meta_type_store));
		result->id             = system->meta_type_next_id++; 
		result->name           = xen::pushString(*(system->meta_type_store), name);
		result->fields.length  = fields.length;
		if(result->fields.length > 0){
			result->fields.values = xen::pushArrayData(*(system->meta_type_store), fields);
			for(u32 i = 0; i < fields.length; ++i){
				result->fields[i].name = xen::pushString(*(system->meta_type_store), fields[i].name);
			}
		} else {
			result->fields.values = nullptr;
		}
		result->static_size   = static_size;

		registerTypeAlias(name, result, system);
		return result;*/
		return nullptr;
	}

	
	bool isInteger(const MetaType* type, SystemMetaType* system){
		return type->fields.length == 0 //bail out early if this is a composite type
		       &&(    type == &xen::impl::TypeOf<u08>::type
		           || type == &xen::impl::TypeOf<u16>::type
		           || type == &xen::impl::TypeOf<u32>::type
		           || type == &xen::impl::TypeOf<u64>::type
		           || type == &xen::impl::TypeOf<s08>::type
		           || type == &xen::impl::TypeOf<s16>::type
		           || type == &xen::impl::TypeOf<s32>::type
		           || type == &xen::impl::TypeOf<s64>::type
		           || type == &xen::impl::TypeOf<long>::type
		           || type == &xen::impl::TypeOf<unsigned long>::type
		         );
	}

	bool isTemplate(const MetaTypeField* field){
		return &xen::MetaTParam[0] <= field->type && field->type <= &xen::MetaTParam[XenArrayLength(xen::MetaTParam) - 1];
	}
	
	/*MetaTypeField MetaTypeField::staticArray(const char* name_, const MetaType* type_, u16 offset_,
	                                         size_t length, u8 additional_qualifiers){
		
		if(length < 2){
			xen::log::write(XenWarn("Creating MetaTypeField KIND_STATIC_ARRAY with element count of %i", length, "xen.meta_type"));
		}
		if((additional_qualifiers & KIND_MASK) != 0 && (additional_qualifiers & KIND_MASK) != KIND_STATIC_ARRAY){
			xen::log::write(XenFatalDev("Creating MetaTypeField KIND_STATIC_ARRAY, but another KIND flag was set"
			                            "(flags: '%x'), ignoring extra flag\n", additional_qualifiers, "xen.meta_type"));
		}
		
		MetaTypeField result(name_, type_, offset_, (u8)((additional_qualifiers & ~KIND_MASK) | KIND_STATIC_ARRAY));
		result.static_array_length = length;
		return result;
	}
	
	
	MetaTypeField MetaTypeField::dynamicArray(const char* name_, const MetaType* type_, u16 offset_,
	                                          const MetaTypeField* length_field, u8 additional_qualifiers,
	                                          SystemMetaType* system){
		
		if(length_field != nullptr && !isTemplate(length_field) && !isInteger(length_field->type, system)){
			xen::log::write(XenFatal("Creating MetaTypeField KIND_DYNAMIC_ARRAY with non-integer "
			                         "length_field, type: %s", length_field->type->name, "xen.meta_type"));
		}
		if((additional_qualifiers & KIND_MASK) != 0 && (additional_qualifiers & KIND_MASK) != KIND_DYNAMIC_ARRAY){
			xen::log::write(XenFatalDev("Creating MetaTypeField KIND_DYNAMIC_ARRAY, but another KIND flag was set"
			                            "(flags: '%x'), ignoring extra flag\n", additional_qualifiers, "xen.meta_type"));
		}
		
		MetaTypeField result(name_, type_, offset_, (u8)((additional_qualifiers & ~KIND_MASK) | KIND_DYNAMIC_ARRAY));
		result.dynamic_array_length = length_field;
		return result;
		}*/

	void registerTypeAlias(const char* name, const MetaType* type, SystemMetaType* system){
		XenAssert(name[0] > 'A' && name[0] < 'z', "Alias array expects first char to have ascii value of between A and z");

		#ifdef XEN_SLOW
		const MetaType* old_type = getType(name, system);
		if(old_type){
			if(type == old_type) {
				xen::log::write(XenFatal("Attempted to register MetaType alias '%s' multiple times for "
				                         "different types, old type: '%s', new type: '%s'", name,
				                         old_type->name, type->name, "xen.meta_type"));
			} else {
					xen::log::write(XenWarn("Attempted to register MetaType alias '%s' multiple times for "
					                         "same types, type: '%s'", name, type->name, "xen.meta_type"));
			}
			return;
		}
		#endif

		SystemMetaType::Alias* alias = xen::reserve<SystemMetaType::Alias>(*system->alias_store);
		alias->name = xen::pushString(*system->alias_store, name);
		alias->next = system->aliases[name[0]-'A'];
		alias->type = type;
		system->aliases[name[0]-'A'] = alias;
	}

	const MetaType* getType(const char* name, const SystemMetaType* system){
		//:TODO:COMP: linked list traversal macro (expands to for loop) / helper?
		for(SystemMetaType::Alias* alias = system->aliases[name[0]-'A']; alias != nullptr; alias = alias->next){
			if(xen::stringEquals(alias->name, name)){ return alias->type; }
		}
		return nullptr;
	}
}

#endif

