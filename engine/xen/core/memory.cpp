////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file memory.cpp
/// \author Jamie Terry
/// \date 2016/07/18
/// \brief Contains defintions for functions and types declared in memory.hpp
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_MEMORY_CPP
#define XEN_CORE_MEMORY_CPP

#include "memory.hpp"
#include "debug.hpp"
#include "../config.hpp"

#include <cstdio>
#include <cstdarg>
#include <cstdlib>

//#undef malloc

namespace xen{
	namespace _sys{
		SystemMemory* memory = nullptr;
	}

	struct SystemMemory{
		Allocator root_allocator;
		
		xen::Array<Allocator> allocators;
		
		ArenaLinear arena;
	};

	Allocator::Allocator()
		: name(nullptr), allocations(0), _allocated(0),_debug_node(nullptr),
		  _decendent_allocations(0), _decendent_allocated(0){}

	Allocator::~Allocator(){
		XenAssert(allocations == 0 && _allocated == 0,
		       "Allocator destructed before all allocations were freed");
	}

	/// \brief Helper function to create a new MemoryDebugNode and insert it into the tree
	/// \param instance Pointer to the allocator/arena that the node is for
	/// \private \memberof SystemMemory
	MemoryDebugNode* _createMemoryDebugNode(SystemMemory* system, MemoryDebugNode* parent,
	                                  const char* name, MemoryDebugNode::Types type, void* instance){
#ifdef XEN_DEBUG_MEMORY

		MemoryDebugNode* result = reserve<MemoryDebugNode>(system->arena);
		result->name = pushString(system->arena, name);

		//:TODO:COMP: linked list tree add child, CRTP base?
		result->first_child = nullptr;
		if(parent == nullptr){
			result->next_sibling = nullptr;
			result->parent       = nullptr;
		} else {
			result->parent       = parent;
			result->next_sibling = parent->first_child;
			parent->first_child  = result;
		}

		result->type   = type;
		result->object = instance;
		                                        
		return result;
#else
		return nullptr;
#endif
	}


	SystemMemory* createSystemMemory(){
		//manually allocate
		const size_t block_size = megabytes(1);
		void* mem = malloc(block_size);
		SystemMemory* result = (SystemMemory*)mem;
		
		//set up the ->arena
		result->arena.start = ptrAdvance(mem, sizeof(SystemMemory));
		result->arena.end   = ptrAdvance(mem, block_size);
		resetArena(result->arena);

		// setup root allocator + pretend it allocated "mem"
		result->root_allocator.name        = "root";
		result->root_allocator.allocations = 1;

		//set up the allocators
		//:TODO: configurable allocator count
		result->allocators = reserveArray<Allocator>(result->arena, 64);
		clearToZero(result->allocators);

		//set up the debug data
#ifdef XEN_DEBUG_MEMORY
		MemoryDebugNode* root_node = _createMemoryDebugNode(
		    result, nullptr, "root", MemoryDebugNode::ALLOCATOR, (&result->root_allocator)
		);

		result->root_allocator._allocated             = blockSize(result->arena); //block_size;
		result->root_allocator._decendent_allocated   = blockSize(result->arena); //block_size;
		result->root_allocator._decendent_allocations = 1;
		result->root_allocator._debug_node            = root_node;
		result->root_allocator._system                = result;

		_createMemoryDebugNode(result, root_node, "Memory Debug Data", MemoryDebugNode::ARENA_LINEAR, &result->arena);
#else
		result->root_allocator._allocated  = 0;
		result->root_allocator._debug_node = nullptr;
#endif

		return result;
	}

	void destroySystemMemory(SystemMemory* system){
		for(auto it = xen::begin(system->allocators); it; ++it){
			it->~Allocator(); //this checks nothing is left allocated
		}
		XenAssert(system->root_allocator.allocations == 1, "Root allocator destructed before all allocations were freed");
		free(system);
	}

	/// \brief Retrieves the root allocator for Xeno Engine, all memory that is allocated
	/// is done through this or one of its children
	Allocator& getRootAllocator(SystemMemory* system){
		return system->root_allocator;
	}

	const MemoryDebugNode* getRootMemoryDebugNode(const SystemMemory* system){
		return system->root_allocator._debug_node;
	}

	MemoryBlock allocate(Allocator& alloc, size_t num_bytes, bool allow_fail){
		MemoryBlock block;
		block.start = malloc(num_bytes);
		if(block.start == nullptr){
			xen::log::write(XenFatalMaybe("Allocator %s failed to allocate memory, requested size: %i bytes",
			                              alloc.name, num_bytes, "xen.memory.allocate"));
			block.end = nullptr;
		} else {
			block.end = ptrAdvance(block.start, num_bytes - 1);
			++alloc.allocations;
#ifdef XEN_DEBUG_MEMORY
			alloc._allocated += num_bytes;
			for(auto node = alloc._debug_node; node != nullptr; node = node->parent){
				XenAssert(node->type == MemoryDebugNode::ALLOCATOR, "All ancestors of an Allocator node should also be Allocators");
				node->allocator->_decendent_allocated += num_bytes;
				++node->allocator->_decendent_allocations;
			}
#endif
		}
		return block;
	}

	ArenaLinear* allocateArenaLinear(Allocator& alloc, size_t num_bytes, const char* name, bool allow_fail){
		MemoryBlock block = allocate(alloc, num_bytes + sizeof(ArenaLinear), allow_fail);
		if(allow_fail && block.start == nullptr) { return nullptr; }
		ArenaLinear* arena = (ArenaLinear*)block.start;
		arena->start = ptrAdvance(block.start, sizeof(ArenaLinear));
		arena->end   = block.end;
		resetArena(*arena);

		_createMemoryDebugNode(alloc._system, alloc._debug_node, name, MemoryDebugNode::ARENA_LINEAR, arena);
		
		return arena;
	}

	void deallocate(Allocator& alloc, void* address){
		XenAssert(alloc.allocations > 0, "Tried to free memory with an allocator that has no outstanding allocations");
		--alloc.allocations;
		free(address);
	}
	
	const char* pushFormatted(ArenaLinear& arena, const char* format_string, ...){
		ptrAlignForward(&arena._next_byte, alignof(char));
		size_t space = bytesRemaining(arena);
		if(space == 0){
			//:TODO: named arenas
			xen::log::write(XenFatal("Tried to push formatted string to an ArenaLinear but arena was full", "xen.memory.push"));
			return nullptr;
		}
		va_list args;
		va_start(args, format_string);
		char* result = (char*)arena._next_byte; //:TODO: aliasing bug possible
		int written = vsnprintf(result, space, format_string, args);
		va_end(args);

		if(written < 0){
			xen::log::write(XenFatalDev("Encoding error occured when pushing formatted string to ArenaLinear", "xen.memory.push"));
			return nullptr;
		}

		if((size_t)written > space){
			//:TODO: named arenas
			xen::log::write(XenFatalDev("Ran out of space while pushing a formatted string to an arena", "xen.memory.push"));
			written = (int)space;
		}

		arena._next_byte = ptrAdvance(arena._next_byte, (size_t)written+1); //+1 for '\0'
		return result;
	}

	const char* debugMemoryDumpHierachy(ArenaLinear* arena, const MemoryDebugNode* root, u32 indent_level){
#ifdef XEN_DEBUG_MEMORY
		char* result = (char*) arena->_next_byte;

		for(u32 i = 0; i < indent_level; ++i){ pushStringNoTerminate(*arena, "  "); }

		switch(root->type){
		case MemoryDebugNode::ALLOCATOR:
			pushFormatted(*arena, "ALLOCATOR %s - Own: %i Allocs, %i KB, Decendents: %i Allocs, %i KB", root->name,
			              root->allocator->allocations, root->allocator->_allocated/1024,
			              root->allocator->_decendent_allocations, root->allocator->_decendent_allocated/1024);
			break;
		case MemoryDebugNode::ARENA_LINEAR:{
			size_t total_bytes = blockSize(*root->arena_linear);
			size_t used_bytes  = total_bytes - bytesRemaining(*root->arena_linear, 1);
			pushFormatted(*arena, "ARENA_LINEAR: %22s - Used %5.2f%% (%6i/%6i KB)", root->name,
			              (100.0f*(float(used_bytes))/((float)total_bytes)),
			              used_bytes/1024, total_bytes/1024);
			break;
		}
		default:
			InvalidCodePath;
		}

		((char*)(arena->_next_byte))[-1] = '\n'; //replace null terminator with new line
		
		++indent_level;
		for(MemoryDebugNode* node = root->first_child; node != nullptr; node = node->next_sibling){
			debugMemoryDumpHierachy(arena, node, indent_level);

			//chop of the null terminator introduced by pushing "" below...
			arena->_next_byte = ptrRetreat(arena->_next_byte, 1);
		}

		//...push null terminator, this will only stay if we're returning to some caller rather
		//than this being a recursive call
		pushString(*arena, "");
		return result;
#else
		return pushString(*arena, "Memory Debugging Disabled\n");
#endif
	  
	}

}

//#define malloc #error BAD CALL use xen::allocate

#endif

