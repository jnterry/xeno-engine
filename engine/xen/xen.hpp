////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file xen.hpp
/// \author Jamie Terry
/// \date 2016/07/18
/// \brief Convienience header which includes all the module headers, thus including
/// every public interface hpp file in Xeno Engine
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_XEN_HPP
#define XEN_XEN_HPP

#include "config.hpp"
#include "core.hpp"
#include "math.hpp"

#endif

