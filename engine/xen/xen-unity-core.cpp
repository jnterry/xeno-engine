////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file 
/// \author Jamie Terry
/// \date 2016/07/21
/// \brief Convinience file for building Xeno Engine via the unity build, includes
/// all other files in the core module
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#include "core.hpp"

#include "core/debug.cpp"
#include "core/memory.cpp"
#include "core/meta_types.cpp"
#include "core/time.cpp"
#include "core/file.cpp"

