////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file core.hpp
/// \author Jamie Terry
/// \date 2016/07/18
/// \brief Convienience header which includes all public headers in the core module
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CORE_CORE_HPP
#define XEN_CORE_CORE_HPP

#include "core/intrinsics.hpp"
#include "core/memory.hpp"
#include "core/debug.hpp"
#include "core/meta_types.hpp"
#include "core/time.hpp"
#include "core/file.hpp"

#endif
