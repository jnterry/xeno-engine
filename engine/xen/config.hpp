////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file config.hpp
/// \author Jamie Terry
/// \date 2016/07/22
/// \brief Contains various configuration settings for Xeno Engine
///
/// \ingroup core
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_CONFIG_HPP
#define XEN_CONFIG_HPP


/// \name Config Settings
///
/// \brief Rather than having a single XEN_DEBUG flag or similar which just enables
/// all debugging different debug systems have different macros, this means which
/// systems are active can be more finely controled as a tradeoff between debug infomation
/// and performance hit
///
/// @{


/// \brief Whether the memory system should store debugging data about allocations
/// and arenas, this will increase memory usage and decrease allocation performance
/// but enable looking at how much memory each system is using
#define XEN_DEBUG_MEMORY

/// \brief Misc extra error checking code is allowed to run, decreases performance but
/// catches errors
#define XEN_SLOW


/// @}



/// \name Platform Macros
///
/// \brief These macros are defined depending on the platform Xeno Engine is being
/// compiled on. Generally they should be automattically detected, but if this fails
/// you will need to compile with one of the these set
///
/// @{

//determine OS
#if !defined(XEN_OS_WINDOWS) && !defined(XEN_OS_UNIX)
	#if defined(_WIN32) || defined(__WIN32__) || defined _MSC_VER
      /// \brief Defined whenever 
		#define XEN_OS_WINDOWS 1
	#elif defined(linux) || defined(__linux) || defined(__unix) || defined(__unix__) || defined(unix)
		#define XEN_OS_UNIX 1
	#else
		//Unsupported system
		//note, it could be that the os is supported but the compiler doesn't #define
		//one of the expected values, so we cant automattically determine the OS
		//try defining the one of the XEN_OS_*** #defines manually
		#error This operating system is not supported by Xeno Engine
	#endif
#endif

/// @}


namespace xen{

}

#endif

