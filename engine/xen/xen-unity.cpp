////////////////////////////////////////////////////////////////////////////////
//                        Part of Xeno Engine                                 //
////////////////////////////////////////////////////////////////////////////////
/// \file xen-unity.hpp
/// \author Jamie Terry
/// \date 2016/07/18
/// \brief Convienience header which includes all other hpp AND cpp files that make
/// up Xeno Engine, this allows for a so called "unity" build, which simulates having
/// the entire code base in a single file and building it all as a single translation
/// unit. This can speed up the compilation process and also allow the compiler to
/// make more optimisations
////////////////////////////////////////////////////////////////////////////////

#ifndef XEN_UNITY_HPP
#define XEN_UNITY_HPP

#include "xen-unity-core.cpp"
#include "xen-unity-math.cpp"


#endif

