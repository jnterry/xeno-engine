# Xeno Engine - Xenogin
Back to C style game engine

## Design Justification
Xeno Engine is a back to C style game engine written from the ground up with performance and simplicity in mind. It is written from the premise that data and the transformations that must be applied to the data are the most important aspects of a system. Simply applying OOP ideas everywhere due to some sense of "best practice" leads to code with layers upon layers of abstractions between the programmer and the data being manipulated. When it comes time to optimize a program in a world where reading and writing data from RAM is orders of magnitude slower than performing simple operations these layers only serve to add additional complexity and additional code that must be understood.

It is a commonly held belief that computers today are "fast enough" that any "minor" inefficiencies introduced by such abstractions are insignificant, however it is such claims that lead to google chrome performing 25,000 memory allocations per keystoke in Omnibox. (https://groups.google.com/a/chromium.org/forum/#!msg/chromium-dev/EUqoIz2iFU4/kPZ5ZK0K3gEJ). While it is true that the Omnibox does more than typing into a 1980's text editor, it is little wonder that the input lag appears to have worsened rather than improved.

There seems to be some truth to wirth's "law"  that states "software is getting slower more rapidly than hardware becomes faster".

If you wish to write performant code the underlying data MUST be understood, and this is far simpler when it is not hidden under layers and layers of abstraction. This does not mean we should discard all advances since the times of writing assembly code and directly controlling the registers of the machine, but it does mean that every additional layer that comes between the developer and the hardware should be carefully considered, and the benefits of the more abstract interface must be weighed up against the problems that are introduced - no abstraction is free.

For example, making some abstraction layer which means we don't need to care about what operating system we're running on is an abstraction worth having. Being able to write the rest of the game once with no differences between platforms is an enormouse advantage, one which outweighs the cost of hiding what is going on under the hood.

### Resources

The design philosophy of Xeno Engine has been heavily shaped by a number of sources, certainly the idea of "Data Oriented Design" is not at all novel (in fact, one could argue it precedes more modern OOP ideas, since before such ideas the data was often more at the heart of the design anyway).

Handmade Hero - Series showing that a complete professional quality game can be made by a single person "by hand", that is without layers of tools and APIs between the developer and the hardware.
https://hero.handmade.network/

Compression Oriented Programming
https://mollyrocket.com/casey/stream_0019.html

Mike Acton - Data Oriented Design

Jonathon Blow on Software Quality
https://www.youtube.com/watch?v=k56wra39lwA

## Code Style and Design

### Types - Structures and Classes

Xeno Engine categories its types into three categories:

- Open structs (declared as struct type {};)
- Classes      (declared as class type {};)
- Opaque types (declared as struct type;)

(clearly there is no difference between structs and classes to the compiler, but the keyword used distingishes between these 3 types and hints to developers the indended usage of the type.)

#### Open Structs

This type is the most common in Xeno Engine. These are data only structs with NO private members. The only member functions are those which cannot be implemented as free standing functions, eg, constructors (although ideally constructors shouldn't be needed, clearing to 0 should produce a valid instance where possible).

Members which would normally be private are instead  prefixed eith an underscore.

eg:

```
template<typename T>
struct DynamicArray{
  T* data;              // pointer to the data
  uint32_t size;        // current number of elements in collection
  uint32_t _capacity;   // actual length of the data array
};
```

Here "data" and "size" are being guaranteed by the interface to be stable and for anyone to use (within whatever constraints are documented). `_capacity` on the other hand should only be used by people who know what they are doing and have an understanding of the internal behavior of the type. Using this definition of "know what they are doing" is of course open to interpretation, and you could argue that marking such fields as private so that it is enforced by the compiler rather than a programmers conscience is better, however:

Advantages of open structs:

- Operations may be grouped by the functionality they provide, rather than by the types they operate on. For example a file which provided serialisation routines, one for iterating over containers, another for generating debug views in game, etc.

- The interface the type provides can be expanded without modifying the type. This is particular helpful if the type is in a header that is included in many places - modifing the header would require recompiling much of the project, where as adding a new file with the new functions that is only inclued where required means a full recompile of everything using the type is not requried.

- Having in game debugging tools is FAR easier, since no hacks are needed to expose the private members (eg, run time reflection data visualizers: http://joshparnell.com/blog/runtime-reflection-ii/)

#### Opaque types

These are types with a forward declaration visible to user code, but not the actual definition. Hence user code can only hold a pointer or reference to such a type.

These are usually reserved for types where the definition of the type varies on different platforms, eg a Window type.

#### Classes

Classes are rarley used as you must give up the advantages described concering open structs. They are generally used to represent high level systems that do no vary on different platforms and hence do not need to be opaque pointers. They are also used where virtual methods are appropriate, such methods are used sparingly and should be avoided in tight loops as they cannot be inlined when called through a reference to a base class and have other performance impacts.

These are the full blown OOP classes, with "proper" encapsulation (private members, etc).

### File organization

Rather than is traditional in OOP design having a header and source file for each class, code should be organized by function. This is facilitated by the use of open structs as described above. If a file grows to be large there is no need to split it just for the sake of best practice, single responsiblity etc.

Frequently used types should have a minimal definition in a header, with functions that operate on the types found seperately. The data layout of such types rarely changes (eg, a 3 dimensional floating point vector is always just 3 floats), but the functions operating on them are changed frequently - especially true if the function's body is in the header as well as the defintion, eg so it can be inlined/is a template function. Seperating the type definitons from the functions mean that other type headers can include only the type definitions and do not need to be recompiled when the functions change, but only when the data layout is changed.

The type only files should be suffixed with \_types.hpp, eg math_types.hpp, vector.hpp, matrix.hpp

More infomation about this idea can be found at the bitsquid blog article:
http://bitsquid.blogspot.co.uk/2012/09/a-new-way-of-organizing-header-files.html

### Error Handling

The short explanation of error handling is to prefer (controlled) crashing to error checking.

Ideas for error handling in this engine are taken from a number of sources, most notably the bitsquid series of error handling.
http://bitsquid.blogspot.co.uk/2012/01/sensible-error-handling-part-1.html

Traditional methods for error handling include some return code to indicate an error has occurred or exceptions.

Return code checking is tedious and makes code messy, exceptions make the code cleaner but obscure the code flow and can decrease performance even when the exception is not thrown (possibly not such a concern on modern compilers and platforms, but something to bear in mind). When generating an exception we often don't have all the information required to generate a descriptive error message, for example if an error occurred passing some part of a file we don't necessaraly know the name of the file being parsed. We could catch and re-throw the exception but this is cumbersome.

Ideally we would not have errors and not force calling code to check for errors, clearly we cannot just get rid of errors but we can make it so the caller doesn't need to check for errors by - just crashing.
This may seem undesirable but for critical errors which are caused by programming errors (eg, trying to free memory with an allocator which didn't allocate the memory) there is nothing the calling code can do. We could just print a log messsage but this relies on logs being checked, and when errors are found them being fixed, neither of which is guarrentied. Instead, if we just crash (after printing a log message and/or displaying an error alert) then the error cannot be ignored, and the calling code doesn't need to check for errors - either the call succseeds or we crash.

This also means that an error wont go un-noticed, corrupt some state and then cause a much harder to debug crash at some later point.

In different situations the same error may have different levels of severity.
For example, if we can't find the games main data file then the game cant start, pretty much all we can do is display and error and crash. On the other hand if a settings file cant be found then we could use default settings and then make a new settings file. Rather than always have the burden of error checking we simply say that trying to open a file that doesn't exist causes a crash, then if you want to handle a file not existing you call a function that checks for its existence before trying to open it. Hence, where error checking is desired we can have it, without forcing places where errors are unexpected (and cant be handled in a sensible way) to have the runtime overhead and mental programming overhead of error checking.
In other words, we can check for potential foreseen errors before performing some action, and then  handle these sensibly, however, if an error is unforeseen we cannot do anything sensible about it, so simply crashing is a better option to ensure the error doesn't propagate and cause problems elsewhere.

### Unity Build

The unity build is the simplest way to build Xeno Engine, it simply involves #including every hpp, cpp and hxx file that makes up the engine in a single file, xen/xen-unity.cpp This file may then be compiled standalone to create the Xeno Engine library, compiling the entire engine in a single translation unit.

This can decrease compile times and present more opertunites to the optimiser (not such a concern since many modern toolchains can do cross translation unit optimisations anyway).

xen/xen-unity.cpp can also be included by the program (ie, by main.cpp or equivalent) in order to prevent the need to link to the shared/static library.

### File Extensions

Public headers which define the public interface to the engine have the extension .hpp

Implementation source files have the extension .cpp. Internal headers have the extension .hxx


### Comments + Documentation
Documentation for Xenogin is provided by doxygen style comments, the style used for these comments is using three slashes, for example:

```C++
//////////////////////////////////////////////////////////////////////////////
/// \brief This is a function which does some interesting things
/// \param pName If you read this you'll know what this parameter is for
/// \param pName2 Knowing what parameters are for is helpful
/// \return It ensures the return is what you expect
/// \warn Assuming you pay attention to potential pitfalls with the function
//////////////////////////////////////////////////////////////////////////////
```

If a single line is comment is required for member variables then the `///<` style should be used, these may appear on the line before a variable declaration or at the end of the line containing the declaration, for example:

```C++
///< Good variable names mean comments like these aren't necessary
int thing;

int stuff; ///< But they're good to add some extra detail for the documentation
```

For a well named function where it is clear what the parameters are doing it is acceptable to have no comment or, to have a single line ///< style comment without details on the parmeters.

Eg:

`
void* allocate(size_t num_bytes);
`

Adding comments to a function like this would just be a waste of time - don't feel compeled to. In fact, prefer functions that are so well named, and whose parameters are so obvious that documentation is not needed, since when reading code using the functions the documentation cannot be seen anyway (at least not without having it open elsewhere and jumping around).


Descriptive comments inside function bodies should ALWAYS use a double slash style, ie, //, even if they span multiple lines

By using these styles large blocks of code may be commented out (for testing/debugging purposes) using /\*...\*/
If block comments are used elsewhere then it is harder to do this, as block comments can't be nested!

### Comment Tags

In order to facilitate seaching for certain types of comment they can be tagged as follows:

// :TODO: -> general todo

// :OPT:  -> description of possible optimisation, should be tested (ideally write unit tests for exisitng implementation if don't already exist, then do optimisation and check they pass)

// :COMP: -> possible compression, this is put when developer has a feeling that something could be extracted into general helper function, class, etc, but until another example is found not going to bother

Can combine tags like :TODO:OPT:

## The Name "Xeno Engine"

Xeno means different, alien or foreign. This matches the design philosophy of the engine in that it differs from the standard object-oriented design.

It also has the advantage of having a 3 letter pronounceable shortening which is convenient for use as the namespace containing the public interface for the engine. The only other alternative I thought of that also possessed this property was "Data Oriented Engine" with the shortening doe, which is not the most creative name.

It also has the nice portmanteau Xenogin
